<?php
/******************************************
US English
Admin Order Language
******************************************/

$lang['message_orders_deleted']		= 'The selected order/s have been deleted';
$lang['error_no_orders_selected']	= 'You did not select any order to delete';
$lang['message_customers_deleted']		= 'The selected customer/s have been deactivated';
$lang['error_no_customers_selected']	= 'You did not select any customer to deactivate';