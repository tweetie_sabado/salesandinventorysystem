<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/
			$config['protocol']    = 'smtp';

            $config['smtp_host']    = 'ssl://smtp.gmail.com';

            $config['smtp_port']    = '465';

            $config['smtp_user']    = 'pcfast.ph@gmail.com';

            $config['smtp_pass']    = 'pcfast123';

            $config['charset']    = 'utf-8';

            $config['newline']    = "\r\n";

            $config['mailtype'] = 'html';

            $config['validation'] = TRUE;  
			
			$config['wordwrap'] = TRUE;
			
/* End of file email.php */
/* Location: ./application/config/email.php */