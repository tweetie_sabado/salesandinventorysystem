<div class="container">
  <h1 class="text-center page-header">Add Product</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if(!empty($this->upload->display_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo $this->upload->display_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('success');?>
  		</div>
  <?php }?>
<div class="alert alert-success" role="alert">
	<span class="glyphicon glyphicon-ok-sign"></span>
	<?php echo 'Your image has been successfully uploaded.';?>
	</div>
<div class="container-fluid">
  	<div class="row">		 
		  <!-- Tab panes -->
		    	<div class="col-md-9 col-md-offset-4">
		    	
		    		
				  	<img class="img-thumbnail" src="<?php echo $image_url;?>" width="300" height="300">
				  	
				   
    				<!--<h1>Upload File</h1>
				    <form method="post" action="" id="upload_file">
				        <label for="title">Title</label>
				        <input type="text" name="title" id="title" value="" />
				 
				        <label for="userfile">File</label>
				        <input type="file" name="userfile" id="userfile" size="20" />
				 
				        <input type="submit" name="submit" id="submit" />
				    </form>
				    <h2>Files</h2>
				    <div id="files"></div>-->
				  	<a class="btn btn-primary" href="<?php echo base_url();?>admin/view_product">View Product<span class="glyphicon glyphicon-chevron-right"></span></a>	  		
		    	</div>

		    		<!--<hr>
		    		<div class="form-group">
				    	<input type="text" class="form-control" name="prodName" placeholder="Product Name">
				    </div>
				    <div class="form-group">
				    	<textarea class="form-control" rows="3" name="prodDesc" placeholder="Description"></textarea>
				  	</div>
				  	<div class="form-group">
					    <div class="input-group">
					      <div class="input-group-addon">₱ </div>
					      <input type="text" class="form-control" name="regPrice" placeholder="Regular Price">
					    </div>
					</div>
					<div class="form-group">
						<label class="label label-danger" for="salePrice">Leave blank if product is NOT on sale.</label>
					    <div class="input-group">
					      <div class="input-group-addon">₱</div>
					      <input type="text" class="form-control" name="salePrice" id="salePrice" placeholder="Sale Price">
					    </div>
					</div>
				  	<div class="form-group">
				  		<select name="status" class="form-control">
						  <option>Disabled</option>
						  <option>Enabled</option>
						</select>
				  	</div>
				  	<div class="form-group">
				    	<input type="number" class="form-control" name="quantity" placeholder="Quantity">
				    </div>
				    <div class="form-group">
				    	<label for="category">Category</label>
				    	<select name="category" id="category" class="form-control">
				    		<?php if(!empty($categories)){
			                        if (is_array($categories)){                      
			                          foreach ($categories as $row) {?>?>
			                          	<option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name']?></option>
			                <?php } } }else{?>
		                      			<option value=""></option>
		            		<?php }?>
				    	</select>
				    </div>
				     <div class="form-group">
				    	<label for="supplier">Supplier</label>
				    	<select name="supplier" id="supplier" class="form-control">
				    		<?php if(!empty($suppliers)){
			                        if (is_array($suppliers)){                      
			                          foreach ($suppliers as $row) {?>?>
			                          	<option value="<?php echo $row['supplier_id'];?>"><?php echo $row['company_name']?></option>
			                <?php } } }else{?>
		                      			<option value=""></option>
		            		<?php }?>
				    	</select>
				    </div>-->
		    		

	</div>
	<!--<div class="row">
		<div class="col-md-9" align="right">
			<button name="upload" type="submit" role="button" class="btn btn-success">Save</button>
			<a class="btn btn-danger" href="<?php echo base_url();?>admin/products">Cancel</a>
		</div>
	</div>-->
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

