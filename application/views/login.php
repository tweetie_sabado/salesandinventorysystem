<!--Pulling Awesome Font 
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
    	 <form class="form-signin" method="post" accept-charset="utf-8" action="<?php echo base_url();?>admin_login/validate_account">
            <div class="form-login">
            <h4>Welcome back.</h4>
            <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="username" />
            </br>
            <input type="text" id="userPassword" class="form-control input-sm chat-input" placeholder="password" />
            </br>
            <div class="wrapper">
            <span class="group-btn">     
                <a href="#" class="btn btn-primary btn-md">login <i class="fa fa-sign-in"></i></a>
            </span>
            </div>
            </div>
       </form>
        </div>
    </div>
</div>
-->

<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
            <div class="container">
                    <div class="col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-7 col-lg-offset-3 col-lg-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">                                
                                <div class="row-fluid user-row">
                                    <img style="margin-left: 10px;" class="img-thumbnail" height="150" width="300" src="<?php echo base_url();?>img/logo.jpg" />
                                </div>
                            </div>
                            <div class="panel-body">
                                <form class="form-signin" method="post" accept-charset="utf-8" action="<?php echo base_url();?>admin_login/validate_account">
                                    <fieldset>
                                        
                                            	 <?php 
												  	if(validation_errors() == FALSE){
												  		echo validation_errors();
												  	}
												  	else{
												  	?>	
													  	<div class="alert alert-danger" role="alert"><?php echo validation_errors();?></div>
												  	<?php 
												  	}
												  	?>
                                     
                                        <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required>
                                      
                                        <div class="password">
                                        	<input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
                                        	 <span class="glyphicon glyphicon-eye-open"></span>
                                        </div>
                                          
                                        <style>
                                        	
										.password{
										    position: relative;
										}
										
										.password input[type="password"]{
										    padding-right: 30px;
										}
										
										.password .glyphicon,#password2 .glyphicon {
										    display:none;
										    right: 15px;
										    position: absolute;
										    top: 12px;
										    cursor:pointer;
										}

                                        </style>
                                        <br></br>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login »">
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                
            </div>
      <script type="text/javascript">
      	$("#inputPassword").on("change",function(){
		    if($(this).val())
		        $(".glyphicon-eye-open").show();
		    else
		        $(".glyphicon-eye-open").hide();
		    });
		$(".glyphicon-eye-open").mousedown(function(){
                $("#inputPassword").attr('type','text');
            }).mouseup(function(){
            	$("#inputPassword").attr('type','password');
            }).mouseout(function(){
            	$("#inputPassword").attr('type','password');
            });
      </script>
