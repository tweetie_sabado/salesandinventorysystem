<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-cog"></span> Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account"><span class="fa fa-user"></span> My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages"><span class="fa fa-envelope-o"></span> My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>account/info"><i class="fa fa-info-circle"></i> About Us</a></li>
             <li class="divider"></li>
             <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li class="divider"></li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout"><span class="fa fa-power-off"></span> Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



<div class="container">
	 <?php if(!empty(validation_errors())){?>
      <div class="alert alert-danger" role="alert">
      <?php echo validation_errors();?>
      </div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('bday')){?>
      <div class="alert alert-success" role="alert">
        <a href="<?php echo base_url()?>account/bday_promo"><?php echo $this->session->flashdata('bday');?></a>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('warning')){?>
      <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('warning');?>
      </div>
  <?php }?>
<div class="col-md-5">
  <div class="panel panel-primary">
    <div class="panel-heading">Account Information</div>
      <div class="panel-body">
        <div class="wrap">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>account/edit_client">
          
                        <div class="row">
                          <div class="col-md-6">
                          <label><small>First Name</small></label>
                          <input type="text" name="fname" class="form-control" id="fname" value="<?php echo $row->fname;?>">
                          </div>

                          <div class="col-md-6">
                          <label><small>Last Name</small></label>
                          <input type="text" name="lname" class="form-control" id="lname" value="<?php echo $row->lname;?>"/>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                          <label><small>Email</small></label>
                          <input type="text" name="email" class="form-control" id="email" value="<?php echo $row->email;?>" disabled/>
                          </div>

                          <div class="col-md-6">
                          <label><small>City</small></label>
                          <input type="text" name="cnum" class="form-control" id="cnum" value="<?php echo $row->cnum;?>"/>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-6">
                          <label><small>City</small></label>
                          <select id="city" name="city" class="form-control">
			                      <option><?php echo $row->city;?></option>
			                      <option>-- Select a City --</option>
			                      <option>Caloocan City</option>
			                      <option>City of Malabon</option>
			                      <option>City of Manila</option>
			                      <option>City of Navotas</option>
			                      <option>Las Pinas City</option>
			                      <option>Makati City</option>
			                      <option>Mandaluyong City</option>
			                      <option>Marikina City</option>
			                      <option>Muntinlupa City</option>
			                      <option>Paranaque City</option>
			                      <option>Pasay City</option>
			                      <option>Pasig City</option>
			                      <option>Pateros</option>
			                      <option>Quezon City</option>
			                      <option>San Juan City</option>
			                      <option>Taguig City</option>
			                      <option>Valenzuela City</option> 
		                      </select>
                          </div>

                          <div class="col-md-6">
                          <label><small>Street</small></label>
                          <input type="text" name="address" class="form-control" id="address" value="<?php echo $row->address;?>"/>
                          </div>
                        </div>

                        <div class="col-md-10 col-md-offset-2">
                          <label><small>Barangay</small></label>
                          <input type="text" name="brgy" class="form-control" id="brgy" value="<?php echo $row->brgy;?>"/>
                          </div>

                        <div class="row" align="right">
                            <div class="col-md-12" style="margin-top:10px;">
                              <input type="submit" class="btn btn-primary" value="Submit"/>
                            </div>
                        </div>
              </form>
             
      </div>
    </div>
  </div>
</div>

<div class="col-md-5">
  <div class="panel panel-primary">
    <div class="panel-heading">Change Password</div>
      <div class="panel-body">
        <div class="wrap">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>account/edit_password">
          				<input type="hidden" value="<?php echo $this->session->userdata('pass');?>" name="oldpass" id="oldpass" />
                        <div class="row">
                          <div class="col-md-6">
                          <label><small>Password</small></label>
                          <input type="password" name="recpass" class="form-control" id="recpass">
                          </div>	
                        	
                          <div class="col-md-6">
                          <label><small>New Password</small></label>
                          <input type="password" name="password" class="form-control" id="password">
                          </div>

                          <div class="col-md-6">
                          <label><small>Confirm Password</small></label>
                          <input type="password" name="conpass" class="form-control" id="conpass"/>
                          </div>
                        </div>

                        <div class="row" align="right">
                            <div class="col-md-12" style="margin-top:10px;">
                              <input type="submit" class="btn btn-primary" value="Change"/>
                            </div>
                        </div>
              </form>
              <a href="#" id="button" onclick="showhide()">Change Secret Question?</a>
             
      </div>
    </div>
  </div>
</div>

<div class="col-md-5" id="secret" hidden>
  <div class="panel panel-primary">
    <div class="panel-heading">Change Secret Question</div>
      <div class="panel-body">
        <div class="wrap">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>account/edit_secret">
          
                    <div class="form-group col-md-12">
                    <label>Choose a Secret Question</label>
                    <select name="quest" class="form-control" id="quest">
                    <option>What is your maternal grandmother's maiden name?</option>
                    <option>Name of the street where you grew up?</option>
                    <option>Name of your first pet?</option>
                    <option>Name of your favorite teacher in high school?</option>
                    <option>What is your favorite song?</option>
                    </select>
                    </div>

                    <div class="form-group col-md-12">
                    <label>Answer</label>
                    <input type="text" name="ans" class="form-control" id="ans"/>
                    </div>

                        <div class="row" align="right">
                            <div class="col-md-12" style="margin-top:10px;">
                              <input type="submit" class="btn btn-primary" value="Change"/>
                            </div>
                        </div>
              </form>
      </div>
    </div>
  </div>
</div>
</div>

<div class="container">

  <div class="col-md-12">
    <div class="page-header">
      <h2>Order History</h2>
    </div>
        <table class="table table-bordered table-striped">
      
      <thead>
        <tr>
          <th>Ordered On</th>
          <th>Order Number</th>
          <th>Status</th>
        </tr>
      </thead>

      <tbody>
        <tr>
      <?php if(isset($order)): foreach($order as $row):?>  
          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->order_date));?></td>
          <td width="100" style="text-align:center;"><a href="<?php echo base_url();?>account/view_order/<?php echo $row->order_number;?>"><?php echo $row->order_number;?></a></td>
          <td width="100" style="text-align:center;"> <?php echo $row->status;?></td>

        </tr><?php endforeach?>        
      <?php else: ?>
      <th><div>No records</div></th>
      <?php endif;?>
      </tbody>
    </table>
  
</div>
</div>

<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Cart</h2>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Product Name</th>
                  <th width="100"style="text-align:center;">Price</th>
                  <th width="100"style="text-align:center;">Quantity</th>
                </tr>
              </thead>
              <tbody>
              
     
                <tr>
                <?php if(isset($cart)) : foreach($cart as $row):?> 
                  <tr>
                  <td width="10" style="text-align:center;"> <?php echo $row->product_name;?></td>
                  <td width="100" style="text-align:center;">₱ <?php echo $this->cart->format_number($row->reg_price);?></td>
                  <td width="100" style="text-align:center;"> <?php echo $row->quantity;?></td>
                  <td width="100" style="text-align:center;"><a href="<?php echo base_url();?>account/cart_del/<?php echo $row->cart_id;?>" class="btn btn-danger">Delete</a></td>    
                  </tr><?php endforeach;?>
                  <?php else: ?>
                    <th><div></div></th>
                    <?php endif;?>
                </tr>

              </tbody>
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url();?>order" type="button" class="btn btn-success">Check out</a>
      </div>
    </div>
  </div>
</div>

<script>
function showhide()
     {
           var div = document.getElementById("secret");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";
    }
     }
</script>