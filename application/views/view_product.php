<div class="container">
  <h1 class="text-center page-header">View Product</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if(!empty($this->upload->display_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo $this->upload->display_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('success');?>
  		</div>
  <?php }?>

<div class="container-fluid">
  	<div class="row">		 
		  
	</div>
</div>
