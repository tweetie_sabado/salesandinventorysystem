<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-cog"></span> Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account"><span class="fa fa-user"></span> My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages"><span class="fa fa-envelope-o"></span> My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>account/info"><i class="fa fa-info-circle"></i> About Us</a></li>
             <li class="divider"></li>
             <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li class="divider"></li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout"><span class="fa fa-power-off"></span> Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="row" style=" margin-top:20px;">
    <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
      </div>
      <div class="panel-body">
        <div class="row">
     
     <?php
      foreach($results as $row)
      {
        echo '<a href="'.base_url().'account/product/'.$row->product_id.'/'.$row->current_count.'">
          <div class="col-sm-4 col-md-4">         
          <div class="thumbnail">           
            <img src="'.$row->image_url.'" alt="..." style="width: 160px; height: 160px;">
           <div class="caption center">';
            echo'<h3>'.$row->product_name.'</h3>';
            echo'<p>₱ '.$this->cart->format_number($row->reg_price).'</p>';            
            echo'</div>
            </div>
          </div>
         </a>';
         }
    ?>
        </div>   
      </div>
    </div>


<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Cart</h2>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Product Name</th>
                  <th width="100"style="text-align:center;">Price</th>
                  <th width="100"style="text-align:center;">Quantity</th>
                </tr>
              </thead>
              <tbody>
              
     
                <tr>
                <?php if(isset($cart)) : foreach($cart as $row):?> 
                  <tr>
                  <td width="10" style="text-align:center;"> <?php echo $row->product_name;?></td>
                  <td width="100" style="text-align:center;">₱ <?php echo $this->cart->format_number($row->reg_price);?></td>
                  <td width="100" style="text-align:center;"> <?php echo $row->quantity;?></td>
                  <td width="100" style="text-align:center;"><a href="<?php echo base_url();?>account/cart_del/<?php echo $row->cart_id;?>" class="btn btn-danger">Delete</a></td>    
                  </tr><?php endforeach;?>
                  <?php else: ?>
                    <th><div></div></th>
                    <?php endif;?>
                </tr>

              </tbody>
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url();?>order" type="button" class="btn btn-success">Check out</a>
      </div>
    </div>
  </div>
</div>