<div class="container">
  <h1 class="text-center page-header">Upload Product Image/s</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
<!--<?php echo base_url();?>admin/product_image_upload-->
  <div class="container-fluid">
    	<div class="row">
        <div id="upload">
          <div class="col-md-6">
            <?php 
              echo form_open_multipart('admin/upload');
              $data = array(
              'name'            => 'userfile',
              'id'              => 'files',
              'multiple'          => 'multiple',
              'onchange'          => 'images_preview()',
              );

              echo form_upload($data);
              echo form_submit('upload', 'Upload');
            ?>
              <output id="result" />
          </div>
        </div>
        <!--<form id="upload_file" action="<?php echo base_url();?>admin/do_upload" method="POST" enctype="multipart/form-data">
          <div class="col-md-6 col-md-offset-5">
            <div id="upload">
              <input type="file" >
            </div>
                      </div>
          <h2>Files</h2>
          <div id="files"></div>
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <button type="submit" role="button" class="btn btn-success">Done</button>
              <a class="btn btn-danger" href="<?php echo base_url();?>admin/products">Cancel</a>
            </div>
          </div>
        </form>
        -->
      </div>
  </div>
</div>
<script type="text/javascript">
function handleFileSelect() {
    //Check File API support
    if (window.File && window.FileList && window.FileReader) {

        var files = event.target.files; //FileList object
        var output = document.getElementById("result");

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            //Only pics
            if (!file.type.match('image')) continue;

            var picReader = new FileReader();
            picReader.addEventListener("load", function (event) {
                var picFile = event.target;
                var div = document.createElement("div");
                div.innerHTML = "<div class='col-md-6'><img class='img-thumbnail image-responsive' src='" + picFile.result + "'" + "title='" + picFile.name + "'/></div>";
                output.insertBefore(div, null);
            });
            //Read the image
            picReader.readAsDataURL(file);
        }
    } else {
        console.log("Your browser does not support File API");
    }
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>
