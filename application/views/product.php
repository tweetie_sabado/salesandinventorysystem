<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
              echo '<li><a href="'.base_url().'category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>';	
            }
            ?>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>home/info"><i class="fa fa-info-circle"></i> About Us</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
     <!-- <form class="form-inline navbar-left" role="search" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form> -->
        <li><a href="<?php echo base_url();?>account_login"><i class="fa fa-user"></i> Login</a></li>
        <li><a href="<?php echo base_url();?>home/signup"><i class="fa fa-pencil"></i> Sign Up</a></li>
          <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>	
<div class="container">
          
           
            <div class="page-header">
              <?php    
              foreach ($products as $row) {
                 echo '<h2 style="font-weight:normal">'.$row->product_name.'';
              }
              ?></h2>
            
            </div>

            <div class="col-md-4">
             <div>
                <img class="img-responsive" height="100" width="200" src="<?php echo $row->image_url?>" alt=""/>
            </div>


        </div>
     <div class="col-md-8 container-fluid">
     <div class="row">
         <div class="col-md-8 col-sm-8 col-xs-8" style="margin-top:10px;">
          
            <?php foreach ($products as $row) ?>
                    <br>
                    <br>
                    <h4 style="font-weight:normal">                                            
                    <span class="pull-left">
                    <medium>Price:</medium>
                    <span>₱ <?php echo $this->cart->format_number($row->reg_price);?></span>
                    
                  </span>
                    </h4>
                </div>
              </div>
       
      <div class="row">
        <div class="col-md-5 col-sm-5">
     
 		<br>
        </div>
      </div>
    </div>    
</div>

<div class="container">
	<br>
<ul class="nav nav-pills">
  <li role="presentation" class="active"><a href="#desc" data-toggle="tab"><i class="fa fa-align-left"></i> Description</a></li>
  <li role="presentation"><a href="#reviews" data-toggle="tab"><i class="fa fa-eye"></i> Reviews</a></li>
  <li role="presentation"><a href="#howto" data-toggle="tab"><i class="fa fa-pencil-square-o"></i> How to Order</a></li>
</ul>

	<div class="tab-content">
		
		<div role="tabpanel" class="tab-pane active" id="desc">
			<h2>Description:</h2>
			<p><?php echo $row->product_description;?></p>
			
		</div>
		<div role="tabpanel" class="tab-pane" id="reviews">
			
			<h2>Reviews:</h2>
			
			<?php foreach($review as $row){;?>
			
			<h4><?php echo $row->fname.' '.$row->lname;?></h4>
			<input id="input-id" type="number" name="rating" glyphicon ="true" class="rating form-control" min="0" max="5" step="1" data-size="xs" readonly="true" value="<?php echo $row->rating;?>">
			<p><?php echo $row->review;?></p>
			</br>
			<?php };?>
		</div>
		<div role="tabpanel" class="tab-pane" id="howto">
			<h2>How to Order:</h2>
			<p>1. Create an account</p>
			<p>2. Confirm your account through your registered email.</p>
			<p>3. Login your account</p>
			<p>4. Pick a product category from the list.</p>
			<p>5. Choose your desired product from the list.</p>
			<p>6. Input desired quantity and click <span class="glyphicon glyphicon-shopping-cart"></span> Add to cart button.</p>
			<p>7. Click the cart icon <span class="glyphicon glyphicon-shopping-cart"></span> at the upper right of your dashboard when you are done placing your items.</p>
			<p>8. Fill up the necessary information needed.</p>
			<p>9. Finally submit your order.</p>
		</div>
	</div>
	
</div>
     