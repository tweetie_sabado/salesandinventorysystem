<div class="container">
  <h1 class="text-center page-header">Add Administrator</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>

  
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
		  <form role="form" data-toggle="validator" action="<?php echo base_url();?>admin/add_administrator" method="POST" accept-charset="utf-8">
		    <div class="form-inline form-group">
		    	<input type="text" class="form-control" name="fname" placeholder="First Name" required>
		    	<input type="text" class="form-control" name="lname" placeholder="Last Name" required>
		    	<div class="help-block with-errors"></div>    	
		    </div>	 
	        <div class="input-group form-group" id="sandbox-container">
	        	<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
	            <input type="text" type="text" class="form-control" name="bday" placeholder="Birthdate" required>
	        </div>
	        <div class="help-block with-errors"></div> 
			   
	        <script type="text/javascript">
				$('#sandbox-container input').datepicker({
				});
	        </script>

		    <div class="form-inline form-group">
		    	<input type="text" data-minlength="11" data-error="Please enter a valid contact number." class="form-control" name="cnum" placeholder="Contact Number" required>
		    	<input type="email" class="form-control" name="email" placeholder="Email Address" required>
		    	<div class="help-block with-errors"></div> 
		    </div>
		    <div class="form-group">
		    	<textarea class="form-control" rows="3" name="add" placeholder="Address" required></textarea>
		    	<div class="help-block with-errors"></div> 
		  	</div>
		  	<div class="form-inline form-group">
			    <input type="password" data-minlength="6" class="form-control" name="pass1" id="pass1" placeholder="Password" required>
			    <input type="password" data-match="#pass1" data-match-error="Whoops, passwords don't match" class="form-control" name="pass2" id="pass2" placeholder="Confirm Password" required>
		  		<div class="help-block with-errors"></div> 
		  	</div>
		  
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/administrators" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>

		  <script>
			$(document).ready(function() {
			    $('#adminForm').bootstrapValidator({
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			            adminFname: {
		                    validators: {
		                        notEmpty: {
		                            message: 'The First Name is required'
		                        }
		                    }
		                }
			        }
			    });
			});
			</script>
	  </div>
  </div>
</div>

