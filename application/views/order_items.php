
<div class="container">
	<div class="page-header">
		<h1 align="center"><span class="label label-info">Ordered Items</span></h1>
	</div>
	<div class="table-responsive">
      <table class="table table-striped table-hover">
      	<thead>
      		<tr>
					<th><a href="#">Order Date</a></th>
    				<th><a href="#">Order Nubmer</a></th>
    				<th><a href="#">Product Name</a></th>
    				<th><a href="#">Quantity</a></th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($items)){
                      if (is_array($items)){                      
                        foreach ($items as $row) {?>
            <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['timestamp']));?></td>
            <td><?php echo $row['cart_number'];?></td>
            <td><?php echo $row['product_name'];?></td>
            <td><?php echo $row['quantity'];?></td>
          </tr>
          <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no customers';?></span></p></tr>
                 <?php }?>
               
        </tbody>
      </table>
   </div>
</div>