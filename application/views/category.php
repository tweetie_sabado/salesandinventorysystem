<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
              echo '<li><a href="'.base_url().'category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>';	
            }
            ?>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>home/info"><i class="fa fa-info-circle"></i> About Us</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
     <!-- <form class="form-inline navbar-left" role="search" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form> -->
        <li><a href="<?php echo base_url();?>account_login"><i class="fa fa-user"></i> Login</a></li>
        <li><a href="<?php echo base_url();?>home/signup"><i class="fa fa-pencil"></i> Sign Up</a></li>
      <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>	   

<div class="row" style=" margin-top:20px;">
    <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
     <?php
     foreach ( $cn as $rw){
      echo'<h3 class="panel-heading">'.$rw->category_name.':</h3>';
       }
    ?>
      </div>
      <div class="panel-body">
        <div class="row">
     
     <?php
      foreach($category as $row)
      {
        echo '<a href="'.base_url().'products/'.$row->product_id.'/'.$row->current_count.'">
          <div class="col-sm-4 col-md-4">         
          <div class="thumbnail">           
            <img src="'.$row->image_url.'" alt="..." style="width: 160px; height: 160px;">
           <div class="caption center">';
            echo'<h3>'.$row->product_name.'</h3>';
            echo'<p>₱ '.$this->cart->format_number($row->reg_price).'</p>';            
            echo'</div>
            </div>
          </div>
         </a>';
         }
    ?>
        
      </div>
    </div>