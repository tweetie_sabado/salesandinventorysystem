<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/cat_logged_in/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account">My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages">My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout">Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="container">
	
	<div class="page-header">
		<h2 align="center"><label class="label label-primary">Messages</label></h2>
	</div>
	
	<div class="col-md-5">
		<?php foreach($message as $r);?>

        <input type="hidden" name="message_id"/>
        
        <div class="form-group">
        	<label>From:</label>
        	<input class="form-control" id="usender" value="<?php echo $r->sender_id;?>" name="sender" type="text" disabled/>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" id="title" name="title" value="<?php echo $r->title;?>" type="text" disabled/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" id="message" name="message" rows="5" cols="250" disabled><?php echo $r->body;?></textarea>
        </div>
        
     
        <a href="<?php echo base_url('account/trash/'.$r->message_id);?>" class="btn btn-danger">Trash</a>
        <a href="<?php echo base_url('account/read/'.$r->message_id);?>" class="btn btn-success">Done</a>
		<a href="#" data-toggle="modal" data-target="#reply" class="btn btn-primary">Reply</a>
	
	
	
	
</div>

<div class="modal fade" id="reply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">New Message</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('account/reply_message');?>
        
        <div class="form-group">
        	<label>To:</label>
        	<input class="form-control" type="text" value="<?php echo $r->sender_id;?>" name="receiver"/>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" type="text" value="RE: <?php echo $r->title;?>" name="title"/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" name="message" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>

