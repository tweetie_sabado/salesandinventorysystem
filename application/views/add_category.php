<div class="container">
  <h1 class="text-center page-header">Add Category</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
		  <form role="form" data-toggle="validator" action="<?php echo base_url();?>admin/add_cat" method="post" accept-charset="utf-8">
		  	<div class="form-group">
		    	<input type="text" class="form-control" name="catName" placeholder="Category Name" required>
		    	<div class="help-block with-errors"></div>  
		    </div>
		    <div class="form-group">
		    	<textarea class="form-control" rows="3" name="catDesc" placeholder="Description"></textarea>
		  	</div>
		  	<div class="form-group">
		  		<select name="status" class="form-control">
				  <option>Disabled</option>
				  <option>Enabled</option>
				</select>
		  	</div>
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/categories" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>