
 <div class="container">
  <h1 class="text-center page-header">Purchase Breakdown</h1>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?php foreach($purchases as $row){?>


      <?php echo form_open('admin/edit_pur/'.$row->purchase_id.'/'.$row->purchase_id);?>
       
          <input type="text" name="purchase_id" class="hidden" id="purchase_id" value="<?php echo $row->purchase_id;?>" />
		  <input name="category_id" class="hidden" value="<?php echo $row->category_id;?>" />
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><h3 class="text-center">Purchase Number: <?php echo $row->purchase_id ;?></h3></h3>
              </div>
              <div class="panel-body">
              	  <div class="row">
              	  	 <div class="col-md-9">
                    <strong>Supplier Name:</strong><p><?php echo $row->company_name;?></p>
                    <input type="hidden" name="supplier_id" value="<?php echo $row->supplier_id;?>"/>
                    <strong>Order Date:</strong><p><?php echo date('m/d/y h:i a', strtotime($row->timestamp));?></p>
                    <input type="hidden" name="purchase_date" value="<?php echo $row->timestamp;?>" />
                  </div>
                  <strong>Status:</strong><p><?php echo $row->status;?></p>
              	  </div>
                 
                  
                  <hr>
           			<div class="row">
           				<div class="col-md-12">
           					<div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                      <!--<?php echo '<pre>';print_r($cart_contents);?>-->
                        <?php foreach ($purchases as $row1) {?>
                          <tr>
                             <td><?php echo $row1->product_name;?></td>
                             <input type="hidden" name="product_name" value="<?php echo $row1->product_name;?>" />
                             <td><?php echo $row1->count;?></td>
                             <input type="hidden" name="prod_count" value="<?php echo $row1->count;?>" />
                             <td>₱ <?php $price = $row1->reg_price; $formattedPrice = number_format($price, 2); echo $formattedPrice;?></td>
                             <input type="hidden" name="reg_price" value="<?php echo $row1->reg_price;?>" />
                             <td class="text-left">₱ <?php $qty = $row1->count; $price = $row1->reg_price; $mul = $qty*$price; $formattedAmount = number_format($mul, 2); echo $formattedAmount;?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td>Grand Total</td>
                          <td></td>
                          <td></td>
                          <td class="text-left">₱ <?php $amount = $row->total; $formattedAmount = number_format($amount, 2); echo $formattedAmount;?></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
           				</div>
           				
           				
           			</div>
                  
                  
              </div>
            </div>

            
                  <div class="form-group">
                  <label for="order_status">Edit Status</label>
                     <select class="form-control" name="purchase_status">
                      <option>Pending</option>
                      <option>Delivered</option>
                    </select>
                  </div>
                  <input type="submit" name="Submit" value="Update"  class="btn btn-success">
                  <a href="<?php echo base_url();?>admin/orders" class="btn btn-danger" style="align">Cancel</a>
        <?php } ?>
        <?php form_close();?>
        </div>
        </div>
    </div>