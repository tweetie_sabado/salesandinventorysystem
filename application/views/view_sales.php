<div class="container">
	<div class="row">
		<!--<?php print_r($order_details);?>-->
		<?php if(!empty($order_details)){                
                  foreach ($order_details as $row) {?>
		<div class="col-md-12">
			<div class="col-md-2">
				<a style="margin-top: 4em;" class="btn btn-primary" data-toggle="collapse" href="#download" aria-expanded="false" aria-controls="download">Download</a>
				<div class="collapse" id="download">
					<div class="well" style="margin-top: 5px; width: 90px;">
						<button onclick="myFunction()" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-save"></span></button>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<div class="letter">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<img width="250" height="150" src="<?php echo base_url();?>img/logo.jpg" alt="logo" class="img-rounded">									
								</div>
								<div class="col-md-8 text-right" style="margin-top: 2em;">
									<p>No. 1781 Unit A Espana Blvd.St.cor.R. Cristobal St.,Sampaloc Manila,1008</p>						
									<p>741-8012/559-6475</p>				
									<p>pcfastcomputer@gmail.com</p>	
									<a>www.pcfast.com.ph</a>				
								</div>
							</div>					
							
		 					<h1 style="margin-top: -3px;" class="text-right page-header">Sales Invoice</h1>
						</div>
					</div>

		 			<div class="row">
		 				<div class="col-md-8">
	 						<div class="form-group">
					  		 <strong>Customer Name:</strong>
					  		 <?php 
					  		 $fname = $row->fname;
							 $lname = $row->lname;
							 $cust_name = $fname.' '.$lname;
					  		 echo $cust_name;
					  		 ?>
						  	</div>
						   	<div class="form-group">
						  		  <strong>Address:</strong>
						  		  <?php echo $row->address;?>
						  	</div>
					  		<div class="form-group">
						  		 <strong>Contact Number:</strong>
						  		 <?php echo $row->cnum;?>
						  	</div>	
						  	<div class="form-group">
						  		 <strong>Notes:</strong>
						  		 <?php 
						  		 $notes = $row->notes;
								 if(!empty($notes)){
								 	echo $notes;
								 }else{
								 	echo 'No notes.';
								 }
						  		 ?>
						  	</div>					
		 				</div>
		 				<div class="col-md-4">
		 					<div class="form-group">
		 						<strong>Date :</strong>
		 						<?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?>
		 					</div>
		 					<div class="form-group">
		 						<strong>Invoice # :</strong>
		 						<?php echo $row->order_number;?>
		 					</div>
		 					<div class="form-group">
		 						<strong>Customer # :</strong>
		 						<?php echo $row->customer_id;?>
		 					</div>	
		 					<div class="form-group">
		 						<strong>Status :</strong>
		 						<?php echo $row->status;?>
		 					</div>		 			 					
		 				</div>
		 			</div>
		 			
		 			<div class="row">
		 				<div class="col-md-6">
		 					<div class="panel panel-success">
		 						<div class="panel-heading">
		 							Bill To
		 						</div>
								  <div class="panel-body">
								  	<address>
									  <strong><?php echo $row->bill_to;?></strong><br>
									  <?php 
									  $street = $row->bill_street;
									  $brgy = $row->bill_brgy;
									  $address_one = $street.' '.$brgy;
									  echo $address_one;
									  ?><br>
									  <?php echo $row->bill_city;?><br>
									  <abbr title="Phone">Contact:</abbr> <?php echo $row->bill_cnum;?><br>
									  <abbr title="Email">Email:</abbr> <?php echo $row->bill_email;?>
									</address>		   						    
								  </div>
							</div>
		 				</div>
		 				<div class="col-md-6">
		 					<div class="panel panel-success">
		 						<div class="panel-heading">
		 							Ship To
		 						</div>
							  	 <div class="panel-body">
								  	<address>
									  <strong><?php echo $row->ship_to;?></strong><br>
									  <?php 
									  $street = $row->ship_street;
									  $brgy = $row->ship_brgy;
									  $address_one = $street.' '.$brgy;
									  echo $address_one;
									  ?><br>
									  <?php echo $row->ship_city;?><br>
									  <abbr title="Phone">Contact:</abbr> <?php echo $row->ship_cnum;?><br>
									  <abbr title="Email">Email:</abbr> <?php echo $row->ship_email;?>
									</address>		   						    
								  </div>
							</div>
		 				</div>
		 				
		 			</div>
		 			
		 				
		 			<div class="row">
		 				<table class="table table-striped">
	 						<thead>
	 							<tr class="info">
	 								<th>Product Name</th>
	 								<th>Product Description</th>
	 								<th>Quantity</th>
	 								<th>Regular Price</th>
	 								<th>Sale Price</th>
	 								<th>Total</th>
	 							</tr>
	 						</thead>
	 						<tbody>
	 							 <?php if(!empty($cart_contents)){
		 	 	       				if(is_array($cart_contents)){        
                 	 					foreach ($cart_contents as $row1) {?>
	 							<td><?php echo $row1['product_name'];?></td>
	 							<td><?php echo $row1['product_description'];?></td>
	 							<td><?php echo $row1['quantity'];?></td>
	 							<td>₱ <?php 
	 							$reg_price = $row1['reg_price'];
								$formattedPrice = number_format($reg_price, 2);
	 							echo $formattedPrice;
	 							?></td>
	 							<td>₱ <?php 
	 							$sale_price = $row1['sale_price'];
								$formattedPrice = number_format($sale_price, 2);
	 							echo $formattedPrice;
	 							?></td>
	 							<td>₱ <?php 
								$qty = $row1['quantity']; 
								$price = $row1['reg_price']; 
								$mul = $qty*$price;
								$formattedAmount = number_format($mul, 2); 
								echo $formattedAmount;
								?></td>
	 						</tbody>
	 						<?php } } }?>
	 						<tfoot>
	 							<td></td>
	 							<td></td>
	 							<td></td>
	 							<td></td>
	 							<td class="success">Grand Total</td>
	 							<td>₱ <?php 
								$total = $row->total;
								$formattedAmount = number_format($total,2);
								echo $formattedAmount;
								?></td>
	 						</tfoot>
	 					</table>
		 			</div>
		 			<?php } } ?>
		 			
		 			
		 		<!--<div class="row">
	 				<div class="panel panel-warning">
	 					<div class="panel-heading">Terms and Conditions</div>
	 					<ul>
	 						<li></li>
	 						<li></li>
	 						<li></li>
	 					</ul>
	 				</div>
		 		</div>-->
		 		<div class="row" style="margin-left: 4em;">
		 			<!--<div class="checkbox">
		 				<label>
		 					<input type="checkbox" />
		 				 I have read the terms and conditions and agree that I have received the products stated above in EXCELLENT condition.</label>
		 				
		 			</div>-->
		 			<div class="form-group" style="margin-top: 5em; ">
		 				  <div class="col-md-6">
	 			  		  <strong>Received By:</strong>
				  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
	 			  </div>
	 			  <div class="col-md-6">
	 			  			 <strong>Received From:</strong>
				  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
	 			  </div>
		 			</div>
	 			
		
			
				 </div>
		 		<div class="row text-center" style="margin-top: 1em;">
		 			<div class="panel panel-default">
					  <div class="panel-body">
					    Thank you for your business! This will serve as your official receipt. 
					  </div>
					</div>
		 		</div>
		 		</div>
			</div>
		</div>	
	</div>
	<script>
function myFunction() {
    window.print();
}
</script>
</div>