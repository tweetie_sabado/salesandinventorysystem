<div class="container">
 <div class="bs-callout bs-cat-four">
	  <h1><span class="glyphicon glyphicon-barcode"> Products</span></h1>
	</div>
  <?php if(!empty(validation_errors())){?>
      <div class="alert alert-danger" role="alert">
      	<span class="glyphicon glyphicon-remove-sign"></span>
      <?php echo validation_errors();?>
      </div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
      	<span class="glyphicon glyphicon-ok-sign"></span>
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-success" role="alert">
      	<span class="glyphicon glyphicon-ok-sign"></span>
        <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('warning')){?>
      <div class="alert alert-danger" role="alert">
      	<span class="glyphicon glyphicon-remove-sign"></span>
        <?php echo $this->session->flashdata('warning');?>
      </div>
  <?php }?>
  <div class="row">
    <div class="col-md-6 col-md-offset-6">
      <div class="panel panel-default pull-right">
            <div class="panel-heading">
              <h3 class="panel-title">Stock Legend:</h3>
            </div>
            <div class="panel-body">
              <div>
                <h5><span class="label label-success"><span class="glyphicon glyphicon-thumbs-up"></span></span> = In stock</h5>
              </div>
              <div>
                <h5><span class="label label-warning"><span class="glyphicon glyphicon-hand-up"></span></span> = Stocks are halfway their count.</h5>
              </div>
              <div>
                <h5><span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down"></span></span> = Out of stock or less than half of the stocks.</h5>
              </div>
            </div>
        </div>
    </div>
  </div>
  <div class="table-responsive">
    <form action="<?php echo base_url();?>admin/bulk_delete_prod" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return prod_form();" class="form-inline">
      <div style="text-align:right">
      </div>
      
      <table class="table table-striped table-hover">
      	<thead>
      		<tr>
            <th><input type="checkbox" id="checkAll"/>&nbsp;<button type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
			<th class="col-md-1">Enabled</th>
			<th class="col-md-2">Product Name</th>
    		<th class="col-md-1">Category</th>
			<th>Supplier</th>
			<th>Date Received</th>
   	 		<th class="col-md-1">Current No. of Stocks</th>
   	 		<th class="col-md-1">Total No. of Stocks</th>
    		<th class="col-md-1">Price per Stock</th>
    		<th>Total based on Current No. of Stocks</th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($products)){
                        if (is_array($products)){                      
                          foreach ($products as $row) {?>
             <td><input name="product[]" value="<?php echo $row['product_id']?>" type="checkbox" class="gc_check"/></td>
             <td><?php $stat = $row['show'];
             		if($stat == 'Disabled'){?>
             			<h4><label class="label label-danger">No</label></h4>
             	 <?php	}elseif($stat == 'Enabled'){?>
             			<h4><label class="label label-success">Yes</label></h4>
             	 <?php	} ?>
             </td>
             <td><?php echo $row['product_name'];?></td>
             <td><?php echo $row['category_name'];?></td> 
             <td><?php echo $row['company_name'];?></td>
             <td><?php echo date('F d,Y (D)', strtotime($row['date_received']));?></td>
             <td>
              <?php $count = $row['count'];
                    $half_count = $count/2;
                    $current_count = $row['current_count'];
                    if($current_count == $half_count) { ?>
                      &nbsp;<span class="label label-warning"><span class="glyphicon glyphicon-hand-up"></span></span>                    
                    <?php } elseif ($current_count <= 0 || $current_count < $half_count) { ?>
                      &nbsp;<span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down"></span></span>
                    <?php } elseif ($count >= $current_count || $current_count > $half_count) { ?>
                      &nbsp;<span class="label label-success"><span class="glyphicon glyphicon-thumbs-up"></span></span>
                    <?php }?>
              <?php echo $row['current_count'];?>
             </td>
             <td> <?php echo $row['count'];?></td>
             <td>₱ <?php $price = $row['reg_price']; $formattedPrice = number_format($price, 2); echo $formattedPrice;?></td>
             <td>₱ <?php $count = $row['current_count']; $priceStock = $row['reg_price']; $total = $count*$priceStock; $formattedTotal = number_format($total, 2); echo $formattedTotal; ?></td>
             <td>
             <a href="<?php echo base_url().'admin/edit_product/'.$row['product_id']?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
             <!--<a style="margin-top: 5px;" href="<?php echo base_url().'admin/view_product/'.$row['product_id']?>" class="btn btn-primary btn-small"><span class="glyphicon glyphicon-search"></span></a>-->
             </td>  
          </tr> 
           <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no products';?></span></p></tr>
                 <?php }?> 
        </tbody>
      </table>
    </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
 });
</script>