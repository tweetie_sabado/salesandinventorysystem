<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>admin">PCFAST</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a class="cat1" href="<?php echo base_url();?>admin/dashboard"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
            <li><a class="cat7" href="<?php echo base_url();?>admin/statistics"><span class="glyphicon glyphicon-stats"></span> Statictics</a></li>
            <li class="dropdown">
              <a class="dropdown-toggle cat2" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-save"></span> Reports <span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
              	<li><a href="<?php echo base_url();?>admin/sales"><span class="glyphicon glyphicon-usd"></span> Sales</a></li>
                <!--<li><a href="<?php echo base_url();?>admin/orders"><span class="glyphicon glyphicon-list-alt"></span> Orders</a></li>-->
                <li><a href="<?php echo base_url();?>admin/purchases"><span class="glyphicon glyphicon-shopping-cart"></span> Purchases</a></li>
              </ul>
            </li>
             <li class="dropdown">
              <a class="dropdown-toggle cat2" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-list"></span> Orders<span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>admin/orders"><span class="glyphicon glyphicon-list-alt"></span> Orders - Customers</a></li>
                <li><a href="<?php echo base_url();?>admin/purchases"><span class="glyphicon glyphicon-link"></span> Orders - Suppliers</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle cat3" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-inbox"></span> Archives <span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
              	<li><a href="<?php echo base_url();?>admin/sales_archives"><span class="glyphicon glyphicon-usd"></span> Sales Archives</a></li>
                <li><a href="<?php echo base_url();?>admin/archives"><span class="glyphicon glyphicon-list-alt"></span> Orders Archives</a></li>
                <li><a href="<?php echo base_url();?>admin/products_archives"><span class="glyphicon glyphicon-barcode"></span> Products Archives</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle cat4" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;Catalog <span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
              	<li><a href="<?php echo base_url();?>admin/customers"><span class="glyphicon glyphicon-user"></span> Customers</a></li>
                <li><a href="<?php echo base_url();?>admin/categories"><span class="glyphicon glyphicon-list"></span> Categories</a></li>
                <li><a href="<?php echo base_url();?>admin/products"><span class="glyphicon glyphicon-barcode"></span> Products</a></li>
                <li><a href="<?php echo base_url();?>admin/suppliers"><span class="glyphicon glyphicon-link"></span> Suppliers</a></li>
              </ul>
            </li>
            <!--
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="#">Content<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Banners</a></li>
                <li><a href="#">Pages</a></li>
              </ul>
            </li>
            -->
            <li class="dropdown">
              <a class="dropdown-toggle cat5" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-cog"></span> Administrative <span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>admin/administrators"><span class="glyphicon glyphicon-user"></span> Administrators</a></li>
              </ul>
            </li>
         
            <li class="dropdown">
              <a style="" class="dropdown-toggle cat6" data-toggle="dropdown" role="button" aria-expanded="false" href="#"><span class="glyphicon glyphicon-wrench"></span> Actions <span class="glyphicon glyphicon-chevron-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-globe"></span> Customer Site</a></li>
                <li><a href="<?php echo base_url();?>admin/logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
              </ul>
            </li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
           	<li>
	 			<a href="<?php echo base_url();?>admin/profile"><?php $fname = $this->session->userdata('fname');
	         						$lname = $this->session->userdata('lname');
									$name = $fname .' '.$lname;
									echo $name;
	         				 ?>
	         	<span class="glyphicon glyphicon-user"></span></a>
         	</li>
         	
         	<li>
         		<a href="<?php echo base_url('admin/view_messages');?>"><span class="glyphicon glyphicon-envelope"></span> <span class="badge"></span></a>
         	</li>
           </ul>          	
        </div>
      </div>
    </nav>

    <!--<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Sales</a></li>
            <li><a href="#">Catalog</a></li>
            <li><a href="#">Content</a></li>
            <li><a href="#">Administrative</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <h2 class="sub-header">Recent Orders</h2>

          <h2 class="sub-header">Recent Customers</h2>

          
        </div>
      </div>
    </div>
    -->

 
