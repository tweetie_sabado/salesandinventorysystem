    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/bootstrapValidator.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/bootstrapValidator.min.js"></script>-->
 
 	<!-- jQuery -->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>js/parallax.js"></script>
 	<script  src="<?php echo base_url();?>js/star-rating.min.js"></script>
 	<script  src="<?php echo base_url();?>js/global.js"></script>

    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url();?>js/docs.min.js"></script> -->
      
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url();?>js/ie10-viewport-bug-workaround.js"></script>
    
   <script type="text/javascript" src="<?php echo base_url();?>js/plupload.full.min.js"></script>
	
	
    <!--
    <script src="<?php echo base_url();?>js/blueimp-gallery.min.js"></script>
    <script src="<?php echo base_url();?>js/blueimp-gallery.js"></script>
    <script src="<?php echo base_url();?>js/blueimp-gallery-indicator.js"></script>-->
    <!--custom js-->
    <script src="<?php echo base_url();?>js/custom.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url();?>js/upload.js?>"></script>-->
    
     <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url();?>js/plugins/raphael.min.js"></script>
    <script src="<?php echo base_url();?>js/plugins/morris.min.js"></script>
    <script src="<?php echo base_url();?>js/plugins/morris-data.js"></script>
    
    <script src="<?php echo base_url();?>js/load-image.js"></script>
	<script src="<?php echo base_url();?>js/load-image-ios.js"></script>
	<script src="<?php echo base_url();?>js/load-image-orientation.js"></script>
	<script src="<?php echo base_url();?>js/load-image-meta.js"></script>
	<script src="<?php echo base_url();?>js/load-image-exif.js"></script>
	<script src="<?php echo base_url();?>js/load-image-exif-map.js"></script>
	
 	
  </body>
</html>