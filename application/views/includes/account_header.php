<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/cat_logged_in/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account">My Account</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout">Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <?php echo $num;?></a></li>
        <li><a><span class="glyphicon glyphicon-envelope"></span> 0</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>