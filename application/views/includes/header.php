<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>PCFAST</title>
	
    <!-- Bootstrap core CSS -->
    
     <script type="text/javascript" src="<?php echo base_url(); ?>js/jqBarGraph.1.1.mi.js"></script>
    <link type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Custom styles for this template -->
    <link type="text/css" href="<?php echo base_url();?>css/custom.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/signin.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/star-rating.min.css" media="all" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/dashboard.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/jquery-ui.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?php echo base_url();?>js/custom.js"></script>
    <link href="<?php echo base_url();?>css/bootstrap-text-editor/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/bootstrap-text-editor/index.css" rel="stylesheet">
    <script src="<?php echo base_url();?>css/bootstrap-text-editor/external/google-code-prettify/prettify.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>css/bootstrap-text-editor/bootstrap-wysiwyg.js"></script>
    <!-- new admin header  -->
    <!-- Custom CSS 
    <link href="<?php echo base_url();?>css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS 
    <link href="<?php echo base_url();?>css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
  	
    
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui.css">
    
    <!--<script type="text/javascript" src="<?php echo base_url();?>js/upload.js?>"></script>-->

	<!-- Generic page styles -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui.css">


    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui-noscript.css"></noscript>
    
    <!--<script src="<?php echo base_url();?>js/moment.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datetimepicker.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>dist/validator.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>dist/validator.min.js"></script>
    <!-- BOOTSTRAP VALIDATOR 
    <link type="text/css" href="<?php echo base_url();?>dist/css/bootstrapValidator.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>dist/css/bootstrapValidator.min.css" rel="stylesheet">-->
    

   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
  </head>