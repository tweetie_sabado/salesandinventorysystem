<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>PCFAST</title>
	
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link type="text/css" href="<?php echo base_url();?>css/custom.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/signin.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/dashboard.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>css/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>js/custom.js"></script>
    
    <!-- new admin header  -->
    <!-- Custom CSS 
    <link href="<?php echo base_url();?>css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS 
    <link href="<?php echo base_url();?>css/morris.css" rel="stylesheet">

    <!-- Custom Fonts 
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
  	-->
    
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui.css">
    
    <!--<script type="text/javascript" src="<?php echo base_url();?>js/upload.js?>"></script>-->

	<!-- Generic page styles -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui.css">


    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url();?>css/jquery.fileupload-ui-noscript.css"></noscript>
    <!--<script src="<?php echo base_url();?>js/moment.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datetimepicker.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>dist/validator.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>dist/validator.min.js"></script>
    <!-- BOOTSTRAP VALIDATOR 
    <link type="text/css" href="<?php echo base_url();?>dist/css/bootstrapValidator.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>dist/css/bootstrapValidator.min.css" rel="stylesheet">-->
    

   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php echo $map['js']; ?>
	 <!-- HIGH CHARTS -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/highcharts/js/highcharts.js"></script>
    <script src="<?php echo base_url();?>js/highcharts/js/modules/exporting.js"></script>
  	<script type="text/javascript">
  	  var chart;
  	jQuery(document).ready(function(){
	    chart = new Highcharts.Chart({
	        chart: {
	            renderTo: 'container',
	             type: 'bar'
	        },
	        title: {
	            text: 'Top Selling Products, 2010'
	        },
	        xAxis: {
            categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hits (pieces)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Year 1800',
            data: [107, 31, 635, 203, 2]
        }, {
            name: 'Year 1900',
            data: [133, 156, 947, 408, 6]
        }, {
            name: 'Year 2008',
            data: [973, 914, 4054, 732, 34]
        }],
        exporting: {
            buttons: {
                contextButton: {
                    text: 'Export'
                }
            }
        }
    });
  	});
    </script>
  </head>