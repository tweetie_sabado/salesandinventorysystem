<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
              echo '<li><a href="'.base_url().'category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>';	
            }
            ?>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>home/info"><i class="fa fa-info-circle"></i> About Us</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
     <!-- <form class="form-inline navbar-left" role="search" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form> -->
        <li><a href="<?php echo base_url();?>account_login"><i class="fa fa-user"></i> Login</a></li>
        <li><a href="<?php echo base_url();?>home/signup"><i class="fa fa-pencil"></i> Sign Up</a></li>
     <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>						

<div class="container">
  <?php if($this->session->flashdata('feedback')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('feedback');?>
      </div>
  <?php }?>	
<ul class="nav nav-tabs">
  <li role="presentation"  class="active"><a data-toggle="tab" href="#about">About Us</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#faq">FAQ</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#terms">Terms and Conditions</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#order">Order Acceptance and Pricing</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#restricted">Restricted Access</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#warranty">Warranty Policy</a></li>
  <li role="presentation" ><a data-toggle="tab" href="#feedback">Feedback</a></li>
</ul>
</div>

<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="about">
		<h2>About Us:</h2>
			<p>
				PC FAST COMPUTER SALES AND REPAIR is recognized as one of the premier providers of computer. Established in 2008, witnessed our significant emergence into the computer industry focusing on delivering quality products at affordable prices to all classes of the society.
				PC FAST COMPUTER SALES AND REPAIR, decided to deliver personal approach in dealing with customers. Together with an array of trained and competent human resource personnel has grown rapidly through by satisfied client, because of this, we established PC FAST COMPUTER SALES AND REPAIR, equipped with extended knowledge, experience, skillful man power and commitment to maintain and provide the present and 
				
				PC FAST COMPUTER SALES AND REPAIR, professional approach and proven capabilities have reaped mutual and fruitful alliances with several manufacturers.
				Presently, PC FAST COMPUTER SALES AND REPAIR is aggressively expanding with thrust to cater once again to small and medium-sized enterprises. With the growing demand for quality products and after sales services, and the continuous developments in the field of Information Technology, PC FAST COMPUTER SALES AND REPAIR, has differentiated herself to be the number one choice of fastidious clients.

			</p>
		<br>
		<h2>Mission:</h2>
			<p>It is our mission to provide our clients with not just computer itself but also the satisfaction and pleasure that goes into buying it. It is our vision to be recognized as one of the top computer sales and service providers in the country- highly regarded with the quality of services we provides.</p>
		</div>
		
	<div role="tabpanel" class="tab-pane" id="faq">
			<h2>FAQ:</h2>
			
			<h4>What Products are offered?</h4>
			<p>Laptop, Desktop System (Pre-built & Clone), Servers, CCTV, Network Devices Computer Parts and Accessories</p><br>
			<h4>Where is PC Fast Computer Sales and Repair located?</h4>
			<p>1781 España Boulevard corner Cristobal Street, Metro Manila Cristobal St Sampaloc, Manila, Metro Manila‎</p><br>
			<h4>I forgot my password. How can I retrieve my password?</h4>
			<p>You can reset your password by clicking on the forgot password in the log-in page and you will be asked for your email and  fill up a security question in order to verify that you own this account. After the verification you can create your new password.</p><br>
			<h4>Where can I view my orders?</h4>
			<p>Go to Account and click “My Account”.  All of your transactions are listed in Order History.</p><br>
			<h4>How can I change my account information?</h4>
			<p>Login to your account and click “My Account”. You can edit your account information and your password</p><br>
			<h4>What are the accepted modes of payment?</h4>
			<p>Payment will be cash on delivery only or through our physical store. </p><br>
			<h4>How if I have concerns with my order? (ex. Cancellation of order, follow-up repair)</h4>
			<p>All registered customers may message us through our “My Messages”. Customers can also contact us through our Facebook page or Skype account. </p><br>
			

			
		</div>
		
	<div role="tabpanel" class="tab-pane" id="terms">
			<h2>Terms and Conditions:</h2>
			<p>These terms and conditions govern your use of pcfast.co (“website”); by using this website, you accept these terms and conditions, warranty policy, and Sales Return Policy in full. If you disagree with any these terms, conditions and policies or any part there of, you must not use this website.</p>
			<p>This website uses cookies. By using this website and agreeing to these terms and conditions, you consent to PC Fast’ use of cookies in accordance with the terms of PC Fast’ privacy policy.</p>
			
		</div>
		
	<div role="tabpanel" class="tab-pane" id="order">
			<h2>Order Acceptance and Pricing:</h2>
			<p>In the case where an order cannot be processed for any reasons, PC Fast reserves the right, at their sole discretion, to refuse or cancel any order at any given time. You may also be asked to provide additional verifications or information, before we accept the order.</p>
			<p>All Prices displayed on the website are subject to change without any prior notice and are available only via online payments or other mode of payments specified on the website. The prices may or may not be applicable for some or all physical branches of PC Fast and may or may not be applicable for some modes of payment. In the event that the price changed or have some error, we may, at our own discretion, either contact you for the new price of the product or cancel your order and notify you afterwards. PC Fast reserves the right to refuse or cancel any such orders whether or not the order has been confirmed.</p>
			
		</div>
	
	<div role="tabpanel" class="tab-pane" id="restricted">
			<h2>Restricted Access:</h2>
			<p>Access to certain areas of this website is restricted. PC Fast reserves the right to restrict access to other areas of this website, or indeed this entire website, at PC Fast’ discretion.</p>
			<p>If PC Fast provides you with an email address and password to enable you to access restricted areas of this website or other content or services, you must ensure that the email address and password are kept confidential. </p>
			<p>PC Fast may disable your email address and password in PC Fast’ sole discretion without notice or explanation.</p>

			
		</div>
		
	<div role="tabpanel" class="tab-pane" id="warranty">
			<h2>Warranty Policy:</h2>
			<p>1.	Each item released by PC Fast is identified with a warranty sticker which indicates the date it was sold (month, day, and year). Units assembled by PC Fast authorized technicians must also bear the assembly seal or sticker indicating the assembly date and identification of the technician</p>
			<p>2.	When claiming warranty, the following guidelines should be strictly followed:</p>
			<p>•	Present the warranty slip of the item concerned with the corresponding Job Order Form (J.O.). In case of lost warranty slip, original copy of the sales invoice should be brought and a certified copy of warranty slip will be issued (a fee of Php 100.00 will be charged for the certified true copy). In case of lost Job Order Form (JO), the following must be submitted:</p>
			<p>•	Affidavit of loss</p>
			<p>•	Any two (2) valid IDs</p>
			<p>•	Inter-branch claiming of warranty without the warranty slip and the corresponding Job Order form will not be allowed.</p>
			<p>•	Present the defective item with the corresponding serial number and warranty sticker.</p>
			
			<h4>Note: Failure in following the guidelines will void the eligibility for any warranty claims</h4>
			
		</div>
		
	<div role="tabpanel" class="tab-pane" id="feedback">
		<h2>Feedback:</h2>
			<?php echo form_open('home/feedback')?>
			<div class="col-md-4">
				<div class="form-group">
					<label>Name:</label>
					<input type="text" class="form-control" name="name" />
				</div>
				
				 <div class="form-group">
		        	<label>Message:</label>
		        	<textarea class="form-control" id="message" name="message" rows="5" cols="250"></textarea>
		        </div>
		        
		        <div class="form-group">
					<input type="submit" class="btn btn-primary" value="Submit" />
				</div>
				
				
			</div>
			
			<?php echo form_close()?>

			
		</div>
		
						
	
</div>