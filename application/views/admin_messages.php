<div class="container">
	 <?php if($this->session->flashdata('sent')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('sent');?>
      </div>
  <?php }?>
	<div class="page-header">
		<h2 align="center"><label class="label label-primary">Messages</label></h2>
	</div>

	<div class="row">
		<div class="col-md-2">
			<div>
			<button class="btn btn-primary" data-toggle="modal" data-target="#new_message">New Message <i class="fa fa-plus-square"></i></button>
			</div>
			</br>
			<div>
			<ul class="nav nav-pills nav-stacked">
			  <li role="presentation" class="active"><a href="#inbox" data-toggle="tab">Inbox <span class="badge"><i class="fa fa-envelope"></i> <?php echo $ct_inbox;?></span></a></li>
			  <li role="presentation"><a href="#outbox" data-toggle="tab">Outbox <span class="badge"><i class="fa fa-share"></i> <?php echo $ct_outbox;?></span></a></li>
			  <li role="presentation"><a href="#trash" data-toggle="tab">Trash <span class="badge"><i class="fa fa-trash"></i> <?php echo $ct_trash;?></span></a></li>
			  
			</ul>
			</div>
		</div>
		
		<div class="col-md-10">
			
			<div class="tab-content">
		
				<div role="tabpanel" class="tab-pane active" id="inbox">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">Message ID</th>	
				          <th style="width:200px;">From</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				          <th style="width:400px;">Status</th>
				          <th style="width:100px;">Action</th>
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($inbox)): foreach($inbox as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->message_id;?></td>	
				      	  <td width="100" style="text-align:center;"> <?php echo $row->sender_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->status;?></td>
				          <td width="100" style="text-align:center;"> <a href="<?php echo base_url('admin/inbox/'.$row->message_id);?>" class="btn btn-primary">View</a></td>
				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
				</div>
				
				<div role="tabpanel" class="tab-pane" id="outbox">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">To</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				  
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($outbox)): foreach($outbox as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->receiver_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>
				       
				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
					
				</div>
				<div role="tabpanel" class="tab-pane" id="trash">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">From</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($trash)): foreach($trash as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->sender_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>

				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
				</div>
				
		</div>

		</div>
	</div>
</div>

<div class="modal fade" id="new_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">New Message</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('admin/add_message');?>
        <div class="form-group">
        	<label>To:</label>
        	<select class="form-control" name="receiver">
        		  <?php foreach($em as $r){;?>
        		<option><?php echo $r->email;?></option><?php };?>
        	</select>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" type="text" name="title"/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" name="message" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>