
<div class="container">
	<div class="page-header">
		<h1 align="center"><span class="label label-info">Order History</span></h1>
	</div>
	<div class="table-responsive">
      <table class="table table-striped table-hover">
      	<thead>
      		<tr>
					<th><a href="#">Order Date</a></th>
    				<th><a href="#">Order Nubmer</a></th>
    				<th><a href="#">Status</a></th>
    				<th><a href="#">View Items</a></th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($order)){
                      if (is_array($order)){                      
                        foreach ($order as $row) {?>
            <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['order_date']));?></td>
            <td><?php echo $row['order_number'];?></td>
                     <td><?php 
              			$status = $row['status'];
              			if($status == 'Pending'){?>
              				<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Processing'){?>
              				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
              	    <?php }elseif($status == 'On Hold'){?>
				   			<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
			   		<?php }elseif($status == 'Cancelled'){?>
			   				<h4><label class="label label-danger"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Delivered'){?>
			   				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
			   		<?php } ?>
              </td>
            <td><a href="<?php echo base_url()?>admin/order_items/<?php echo $row['order_number'];?>" class="btn btn-primary"><i class="fa fa-caret-right"></i> View</a></td> 
          </tr>
          <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no customers';?></span></p></tr>
                 <?php }?>
               
        </tbody>
      </table>
   </div>
</div>