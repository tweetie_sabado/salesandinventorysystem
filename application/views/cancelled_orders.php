<div class="container">
   <div class="bs-callout bs-cat-two">
	  <h1><span class="glyphicon glyphicon-list-alt"> Cancelled Orders</span></h1>
	</div>

  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-warning" role="alert">
        <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
   <?php if($this->session->flashdata('warning')){?>
      <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('warning');?>
      </div>
  <?php }?>
   <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <div class="row">
    <div class="span12" style="border-bottom:1px solid #f5f5f5;">
      <div class="row">
        <div class="span4">
          <?php echo $this->pagination->create_links();?>&nbsp;
        </div>
        <!--onsubmit="return customer_form();" lagay nalang natin to sa onclick function ng delete button.-->
        <div class="span8">
         <form action="<?php echo base_url();?>admin/order_search" method="post" accept-charset="utf-8" id="delete_form" class="form-inline" style="float:right">
            <fieldset>
               <input name="sdate" id="sdate" data-provide="datepicker" class="form-control" data-date-format="yyyy/mm/dd" placeholder="Start Date" />
              <input id="start_top_alt" type="hidden" name="start_date" />
              <input name="edate" id="edate" data-provide="datepicker" class="form-control" data-date-format="yyyy/mm/dd" placeholder="End Date" />
              <input id="end_top_alt" type="hidden" name="end_date" />
              <button type="submit" class="btn btn-info" name="search" value="Search">Search</button>
              <button type="submit" class="btn btn-info" name="orders_p" value="export">Export</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="table-responsive">
      <form action="<?php echo base_url();?>admin/bulk_delete_order" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return submit_form();" class="form-inline">
        <table class="table table-striped  table-hover">
          <thead>
            <tr>
              <th><input type="checkbox" id="checkAll" />&nbsp;<button type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
              <th><a href="#">Order ID</a></th>
              <th><a href="#">Order Number</a></th>
              <th><a href="#">Customer Name</a></th>
              <th><a href="#">Order Date and Time</a></th>
              <th><a href="#">Status</a></th>
              <th><a href="#">Total</a></th>
              <th><a href="#">Edit</a></th>
            </tr>
          </thead>
          <tbody>
              <?php if(!empty($orders)){
                        if (is_array($orders)){                      
                          foreach ($orders as $row) {?>
            <tr>
              <td><input name="order[]" value="<?php echo $row['order_id']?>" type="checkbox" class="gc_check"/></td>
              <td><?php echo $row['order_id'];?></td>
              <td><?php echo $row['order_number'];?></td>
              <td><?php echo $row['bill_to'];?></td>
              <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['timestamp']));?></td>
              <td><?php 
              			$status = $row['status'];
              			if($status == 'Pending'){?>
              				<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Processing'){?>
              				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
              	    <?php }elseif($status == 'On Hold'){?>
				   			<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
			   		<?php }elseif($status == 'Cancelled'){?>
			   				<h4><label class="label label-danger"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Delivered'){?>
			   				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
			   		<?php } ?>
              </td>
              <td>₱ <?php $amount = $row['total']; $formattedAmount = number_format($amount, 2); echo $formattedAmount;?></td>
              <td>    
              <input type="hidden" value="<?php echo $row['order_number'];?>" name="order_number" id="order_number">
              <a href="<?php echo base_url();?>admin/view_stat/<?php echo $row['order_id'];?>/<?php echo $row['customer_id'];?>/<?php echo $row['order_number'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span></a>

              </td>
              <td></td>
            </tr>
            <?php } } }else{?>
                      <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no orders';?></span></p></tr>
            <?php }?>
          </tbody>
        </table>
      </form>
   </div>
</div>

<div class="modal fade" id="edit_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Status</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('');?>

                   <input type="text" name="order_id" class="form-control" id="order_id"/>
                  <input type="text" name="order_number" class="form-control" id="order_number"/>

                  <div class="form-group">
                      <label>Status</label>
                      <?php $att = 'id="order_status" class="form-control"';
                        $options = array(
                          ''              => ' ',
                          'Order Placed'  => 'Order Placed',
                          'Pending'    => 'Pending',
                          'Processing'   => 'Processing',
                          'Shipped' => 'Shipped',
                          'On Hold' => 'On Hold',
                          'Cancelled' => 'Cancelled',
                          'Delivered' => 'Delivered'
                        );
                        $value = $row['status'];
                        if(strcmp("Order Placed",$value) == 0){
                          $selected = array('Order Placed');
                        }else if(strcmp("Pending",$value) == 0){
                          $selected = array('Pending');
                        }else if(strcmp("Processing",$value) == 0){
                          $selected = array('Processing');
                        }
                        else if(strcmp("Shipped",$value) == 0){
                          $selected = array('Shipped');
                        }
                        else if(strcmp("On Hold",$value) == 0){
                          $selected = array('On Hold');
                        }
                        else if(strcmp("Cancelled",$value) == 0){
                          $selected = array('Cancelled');
                        }
                        else if(strcmp("Delivered",$value) == 0){
                          $selected = array('Delivered');
                        }
                        echo form_dropdown('status',$options,$selected,$att);
                  ?>
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Update"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
  });

function save_status(id)
{
  show_animation();
  $.post("<?php echo base_url().'admin/edit_order/'.$row['order_id']?>", { order_id: id, status: $('#status_form_'+id).val()}, function(data){
    setTimeout('hide_animation()', 500);
  });
}

function show_animation()
{
  $('#saving_container').css('display', 'block');
  $('#saving').css('opacity', '.8');
}

function hide_animation()
{
  $('#saving_container').fadeOut();
}
</script>

<div id="saving_container" style="display:none;">
  <div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
  <img id="saving_animation" src="<?php echo base_url('img/saving.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
  <div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001"><?php echo 'Saving...';?></div>
</div>