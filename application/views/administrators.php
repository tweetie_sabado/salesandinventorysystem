<div class="container">
  <div class="bs-callout bs-cat-five">
	  <h1><span class="glyphicon glyphicon-user"> 	Administrators</span></h1>
	</div>
   <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <div class="table-responsive">
    <form action="<?php echo base_url();?>admin/bulk_delete_administrators" method="post" accept-charset="utf-8" id="admin_form" onsubmit="return admin_form();" class="form-inline">
      <div style="text-align:right">
        <a class="btn btn-info" href="<?php echo base_url();?>admin/add_admin"><span class="glyphicon glyphicon-plus"></span> Add Administrator</a>
      </div>
      <table class="table table-striped">
      	<thead>
      		<tr>
      		<th><input type="checkbox" id="checkAll" />&nbsp;<button type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
			<th><a href="#">ID</a></th>
            <th><a href="#">First Name</a></th>
			<th><a href="#">Last Name</a></th>
			<th class="col-md-3"><a href="#">Address</a></th>
			<th><a href="#">Contact Number</a></th>
			<th class="col-md-1"><a href="#">Email</a></th>
            <th><a href="#">Status</a></th>
            <th><a href="#">Created By</a></th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($administrators)){
                      if (is_array($administrators)){                      
                        foreach ($administrators as $row) {?>
            <td><input name="admin[]" value="<?php echo $row['admin_id']?>" type="checkbox" class="gc_check"/></td>
            <td><?php echo $row['admin_id'];?></td>
            <td><?php echo $row['fname'];?></td>
            <td><?php echo $row['lname'];?></td>
            <td><?php echo $row['address'];?></td>
            <td><?php echo $row['cnum'];?></td>
            <td><?php echo $row['email'];?></td>
            <td><?php
                  $status = $row['status'];
                  if($status == 'active'){ ?>
                      <h4><span class="label label-success"><?php echo $row['status'];?></span></h4>
                  <?php }elseif ($status = 'inactive') { ?>
                      <h4><span class="label label-danger"><?php echo $row['status'];?></span></h4>
                  <?php } ?>
            </td>
            <td><?php echo $row['created_by'];?></td>
          </tr>
          <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no administrators';?></span></p></tr>
                 <?php }?>
        </tbody>
      </table>
    </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
 });
</script>
