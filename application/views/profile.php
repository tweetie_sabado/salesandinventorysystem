<?php if($this->session->flashdata('success')){?>
  <div class="alert alert-success" role="alert">
    <?php echo $this->session->flashdata('success');?>
  </div>
 <?php }?>
<?php 
if(validation_errors() == FALSE){
	echo validation_errors();
}
else{
?>	
  	<div class="alert alert-danger" role="alert"><?php echo validation_errors();?></div>
<?php 
}
?>
<div class="container">
	<div class="col-md-12">
		<div class="bs-callout bs-cat-six">
		  <h1><span class="glyphicon glyphicon-user"> My Administrator Profile</span></h1>
		</div>
		 <div class="row">
	 	 	<div class="col-md-2 col-md-offset-2 col-sm-6 col-sm-offset-6 col-xs-12 text-center">
			 	<img height="100" width="100" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgLTI3IDI0IDEwMCAxMDAiIGhlaWdodD0iMTAwcHgiIGlkPSJ1bmtub3duIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9Ii0yNyAyNCAxMDAgMTAwIiB3aWR0aD0iMTAwcHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48Zz48ZGVmcz48Y2lyY2xlIGN4PSIyMyIgY3k9Ijc0IiBpZD0iY2lyY2xlIiByPSI1MCIvPjwvZGVmcz48dXNlIGZpbGw9IiNGNUVFRTUiIG92ZXJmbG93PSJ2aXNpYmxlIiB4bGluazpocmVmPSIjY2lyY2xlIi8+PGNsaXBQYXRoIGlkPSJjaXJjbGVfMV8iPjx1c2Ugb3ZlcmZsb3c9InZpc2libGUiIHhsaW5rOmhyZWY9IiNjaXJjbGUiLz48L2NsaXBQYXRoPjxnIGNsaXAtcGF0aD0idXJsKCNjaXJjbGVfMV8pIj48ZGVmcz48cGF0aCBkPSJNMzYsOTUuOWMwLDQsNC43LDUuMiw3LjEsNS44YzcuNiwyLDIyLjgsNS45LDIyLjgsNS45YzMuMiwxLjEsNS43LDMuNSw3LjEsNi42djkuOEgtMjd2LTkuOCAgICAgICBjMS4zLTMuMSwzLjktNS41LDcuMS02LjZjMCwwLDE1LjItMy45LDIyLjgtNS45YzIuNC0wLjYsNy4xLTEuOCw3LjEtNS44YzAtNCwwLTEwLjksMC0xMC45aDI2QzM2LDg1LDM2LDkxLjksMzYsOTUuOXoiIGlkPSJzaG91bGRlcnMiLz48L2RlZnM+PHVzZSBmaWxsPSIjRTZDMTlDIiBvdmVyZmxvdz0idmlzaWJsZSIgeGxpbms6aHJlZj0iI3Nob3VsZGVycyIvPjxjbGlwUGF0aCBpZD0ic2hvdWxkZXJzXzFfIj48dXNlIG92ZXJmbG93PSJ2aXNpYmxlIiB4bGluazpocmVmPSIjc2hvdWxkZXJzIi8+PC9jbGlwUGF0aD48cGF0aCBjbGlwLXBhdGg9InVybCgjc2hvdWxkZXJzXzFfKSIgZD0iTTIzLjIsMzVjMC4xLDAsMC4xLDAsMC4yLDBjMCwwLDAsMCwwLDAgICAgICBjMy4zLDAsOC4yLDAuMiwxMS40LDJjMy4zLDEuOSw3LjMsNS42LDguNSwxMi4xYzIuNCwxMy43LTIuMSwzNS40LTYuMyw0Mi40Yy00LDYuNy05LjgsOS4yLTEzLjUsOS40YzAsMC0wLjEsMC0wLjEsMCAgICAgIGMtMC4xLDAtMC4xLDAtMC4yLDBjLTAuMSwwLTAuMSwwLTAuMiwwYzAsMC0wLjEsMC0wLjEsMGMtMy43LTAuMi05LjUtMi43LTEzLjUtOS40Yy00LjItNy04LjctMjguNy02LjMtNDIuNCAgICAgIGMxLjItNi41LDUuMi0xMC4yLDguNS0xMi4xYzMuMi0xLjgsOC4xLTIsMTEuNC0yYzAsMCwwLDAsMCwwQzIzLjEsMzUsMjMuMSwzNSwyMy4yLDM1TDIzLjIsMzV6IiBmaWxsPSIjRDRCMDhDIiBpZD0iaGVhZC1zaGFkb3ciLz48L2c+PC9nPjxwYXRoIGQ9Ik0yMi42LDQwYzE5LjEsMCwyMC43LDEzLjgsMjAuOCwxNS4xYzEuMSwxMS45LTMsMjguMS02LjgsMzMuN2MtNCw1LjktOS44LDguMS0xMy41LDguMyAgICBjLTAuMiwwLTAuMiwwLTAuMywwYy0wLjEsMC0wLjEsMC0wLjIsMEMxOC44LDk2LjgsMTMsOTQuNiw5LDg4LjdjLTMuOC01LjYtNy45LTIxLjgtNi44LTMzLjhDMi4zLDUzLjcsMy41LDQwLDIyLjYsNDB6IiBmaWxsPSIjRjJDRUE1IiBpZD0iaGVhZCIvPjwvZz48L3N2Zz4=" alt="admin_image" class="img-circle">
			 	<h3 class="sub-header"><?php $fname = $this->session->userdata('fname');
		         						$lname = $this->session->userdata('lname');
										$name = $fname .' '.$lname;
										echo $name;
	 				 ?>
	 		 	</h3>
		 	</div>
		 </div>
		 <div class="row">
		 	<div class="col-md-4"></div>
		 	<div class="col-md-4" style="text-align: center; margin-top: -15em;">
		 		<div class="panel panel-default">
				  <div class="panel-body">
				  	<?php if(!empty($admin_details)){                    
                      			foreach ($admin_details as $row) {?>
				    <strong>Email :</strong><p><?php echo $row->email;?></p>
				    <strong>Birthday :</strong><p><?php echo date('F d,Y', strtotime($row->bdate));?></p>
				    <strong>Cellphone Number :</strong><p><?php echo $row->cnum;?></p>
				    <strong>Address :</strong><p><?php echo $row->address;?></p>
				    <a class="btn btn-primary" data-toggle="collapse" href="#changePass" aria-expanded="false" aria-controls="changePass">Change Password</a>
			      	<div class="collapse" id="changePass" style="margin-top: 0.5em;">
					<form action="<?php echo base_url();?>admin/change_pass" method="POST" accept-charset="utf-8" data-toggle="validator"
					  <div class="well">
					  	<div class="form-group">
					  		
				  			<input class="form-control" type="password" name="curPass" id="curPass" placeholder="Current Password" required/>
					  		<div class="help-block with-errors"></div> 
					  	</div>
					  
					    <div class="form-group">
				    		<input class="form-control" type="password" data-minlength="6" name="newPass" id="newPass" placeholder="New Password" data-minlength-error="Your password should be not less than 6 characters." required/>
					    	 <div class="help-block with-errors"></div> 
					    </div>
					   
					    <div class="form-group">
					    	<input class="form-control" type="password" data-match="#newPass" data-minlength="6" name="confPass" id="curPass" placeholder="Confirm New Password" data-minlength-error="Your password should be not less than 6 characters." data-match-error="Your passwords don't match." required/>
					    	<div class="help-block with-errors"></div> 
					    </div>
					    
					    <button class="btn btn-success" type="submit">Done</button>
					  </div>
					</div>
			      	<?php } }else{?>
                      <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no orders';?></span></p></tr>
            		<?php }?>
				  </div>
				</div>
		 		
	 		</div>
		 </div>
	</div>
</div>