
<div class="container">
 <?php if(!empty(validation_errors())){?>
      <div class="alert alert-danger" role="alert">
      <?php echo validation_errors();?>
      </div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('password')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('password');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('warning')){?>
      <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('warning');?>
      </div>
  <?php }?>
</div>
  
<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
              echo '<li><a href="'.base_url().'category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>';	
            }
            ?>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>home/info"><i class="fa fa-info-circle"></i> About Us</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
     <!-- <form class="form-inline navbar-left" role="search" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form> -->
        <li><a href="<?php echo base_url();?>account_login"><i class="fa fa-user"></i> Login</a></li>
        <li><a href="<?php echo base_url();?>home/signup"><i class="fa fa-pencil"></i> Sign Up</a></li>
        <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>						

	   <div class="row">

		<div class="col-md-12">

		<!--Carousel -->
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img class="center-block carouselimg" src="img/nemo.jpg" style="height:400px;">
						<div class="carousel-caption">
							
						</div>
					</div>
					<div class="item">
						<img class="center-block carouselimg" src="img/C1-1.jpg" style="height:400px;"> 
						<div class="carousel-caption">
						
						</div>
					</div>
					<div class="item">
						<img class="center-block carouselimg" src="img/walle.jpg" style="height:400px;">
						<div class="carousel-caption">
						 
						</div>
					</div>
				</div>
			</div>

		</div>
		
			 
			</div>
			
			
	</div>	
			
		<div class="container">	
  		<div class="row" style=" margin-top:20px;">
    		<div class="col-sm-12">
      		<div class="panel panel-primary">
      		  <div class="panel-heading">
      			<h3 class="panel-heading">Latest Items:</h3>
      		  </div>
      		  <?php  ?>
      		  <div class="panel-body">
      		<div class="row">
      		<?php
      		foreach($q2 as $row){
      				echo '<a href="products/'.$row->product_id.'">
      				  <div class="col-sm-4 col-md-4">				  
      					<div class="thumbnail">';						
      					  echo '<img src="'.$row->image_url.'" alt="..." style="width: 160px; height: 160px;">
      					 <div class="caption center">';
      						echo'<h3>'.$row->product_name.'</h3>';
      						echo'<p>₱ '.$this->cart->format_number($row->reg_price).'</p>';						
      					  echo'</div>
      					  </div>
      				  </div>
      				 </a>';
      				 }
      	 	?>
      				  </div>
      				
      		  </div>
      		</div>
    		</div> 
  		</div>
    </div> 
    
    <div class="container">	
  		<div class="row" style=" margin-top:20px;">
    		<div class="col-sm-12">
      		<div class="panel panel-primary">
      		  <div class="panel-heading">
      			<h3 class="panel-heading">Top Selling Items:</h3>
      		  </div>
      		  <?php  ?>
      		  <div class="panel-body">
      		<div class="row">
      		<?php
      		foreach($q3 as $row){
      				echo '<a href="products/'.$row->product_id.'">
      				  <div class="col-sm-4 col-md-4">				  
      					<div class="thumbnail">';						
      					  echo '<img src="'.$row->image_url.'" alt="..." style="width: 160px; height: 160px;">
      					 <div class="caption center">';
      						echo'<h3>'.$row->product_name.'</h3>';
      						echo'<p>₱ '.$this->cart->format_number($row->reg_price).'</p>';						
      					  echo'</div>
      					  </div>
      				  </div>
      				 </a>';
      				 }
      	 	?>
      				  </div>
      				
      		  </div>
      		</div>
    		</div> 
  		</div>
    </div> 
				  
		</div>		<!--End of div row-->
</div>
</div><!--End of Wrap-->