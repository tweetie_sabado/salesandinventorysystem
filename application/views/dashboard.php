<div class="container">
	<div class="bs-callout bs-cat-one">
	  <h1><span class="glyphicon glyphicon-dashboard"> Dashboard</span></h1>
	</div>
	  
	  <hr />
	  <div class="row">
	  	
		<div class="panel panel-success">
		  <div class="panel-heading">Orders Overview</div>
		  <div class="panel-body">
		  	<div class="row">
		    	<div class="col-lg-2 col-sm-6 col-lg-offset-4">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/orders">
                    <div class="circle-tile-heading green">
                        <i class="fa fa-shopping-cart fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content green">
                    <div class="circle-tile-description text-faded">
                        Total Orders
                    </div>
                    <div class="circle-tile-number text-faded">
                        <?php echo $order_count; ?>
                        <span id="sparklineA"></span>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/archives">
                    <div class="circle-tile-heading blue">
                        <i class="fa fa-archive fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content blue">
                    <div class="circle-tile-description text-faded">
                        Archives
                    </div>
                    <div class="circle-tile-number text-faded">
                        <?php echo $archived_orders; ?>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/archives" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
      	<div class="col-lg-2 col-sm-6  col-lg-offset-1">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/pending_orders">
                    <div class="circle-tile-heading orange">
                        <i class="fa fa-exclamation-triangle fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content orange">
                    <div class="circle-tile-description text-faded">
                        Pending
                    </div>
                    <div class="circle-tile-number text-faded">
                        <?php echo $pending; ?>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/pending_orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/processing_orders">
                    <div class="circle-tile-heading dark-blue">
                        <i class="fa fa-spinner fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content dark-blue">
                    <div class="circle-tile-description text-faded">
                        Processing
                    </div>
                    <div class="circle-tile-number text-faded">
                          <?php echo $processing; ?>
                        <span id="sparklineB"></span>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/processing_orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/onhold_orders">
                    <div class="circle-tile-heading yellow">
                        <i class="fa fa-pause fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content yellow">
                    <div class="circle-tile-description text-faded">
                        On Hold
                    </div>
                    <div class="circle-tile-number text-faded">
                         <?php echo $on_hold; ?>
                        <span id="sparklineC"></span>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/onhold_orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/del_orders">
                    <div class="circle-tile-heading dark-gray">
                        <i class="fa fa-check fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content dark-gray">
                    <div class="circle-tile-description text-faded">
                        Delivered
                    </div>
                    <div class="circle-tile-number text-faded">
                        <?php echo $delivered; ?>
                        <span id="sparklineD"></span>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/del_orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
         <div class="col-lg-2 col-sm-6">
            <div class="circle-tile">
                <a href="<?php echo base_url(); ?>admin/can_orders">
                    <div class="circle-tile-heading red">
                        <i class="fa fa-times fa-fw fa-3x"></i>
                    </div>
                </a>
                <div class="circle-tile-content red">
                    <div class="circle-tile-description text-faded">
                        Cancelled
                    </div>
                    <div class="circle-tile-number text-faded">
                        <?php echo $cancelled; ?>
                        <span id="sparklineD"></span>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/can_orders" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
        </div>
		  </div>
		</div>
	  
	  	<div class="row">
	  	
	  		<div class="col-md-3">
		  		<div class="panel panel-success">
		  			 <div class="panel-heading">Clients Overview</div>
			  			<div class="panel-body">
			  				 <div class="circle-tile">
				                <a href="<?php echo base_url(); ?>admin/active_clients">
				                    <div class="circle-tile-heading green">
				                        <i class="fa fa-smile-o fa-fw fa-3x"></i>
				                    </div>
				                </a>
				                <div class="circle-tile-content green">
				                    <div class="circle-tile-description text-faded">
				                        Active
				                    </div>
				                    <div class="circle-tile-number text-faded">
				                        <?php echo $active_clients; ?>
				                        <span id="sparklineD"></span>
				                    </div>
				                    <a href="<?php echo base_url(); ?>admin/active_clients" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
				                </div>
				            </div>
				            <div class="circle-tile">
				                <a href="<?php echo base_url(); ?>admin/inactive_clients">
				                    <div class="circle-tile-heading red">
				                        <i class="fa fa-frown-o fa-fw fa-3x"></i>
				                    </div>
				                </a>
				                <div class="circle-tile-content red">
				                    <div class="circle-tile-description text-faded">
				                        Inactive
				                    </div>
				                    <div class="circle-tile-number text-faded">
				                        <?php echo $inactive_clients; ?>
				                        <span id="sparklineD"></span>
				                    </div>
				                    <a href="<?php echo base_url(); ?>admin/inactive_clients" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
				                </div>
				            </div>
			  								
			  			</div>
			  	</div>
	  		</div>
	  	
	  	<div class="col-md-9">
	  		<div class="panel panel-success">
	  	 <div class="panel-heading">Orders and Customers Overview</div>
		  <div class="panel-body">
		     <div class="panel panel-primary">
	    <div class="panel-heading"><h4 class="text-center">Recent Orders</h4></div>
	  </div>
	    <div class="table-responsive">
	      <table class="table table-striped">
	        <thead>
	            <tr>
	              <th>Order Number</th>
	              <th>Customer Name</th>
	              <th>Order Date</th>
	              <th>Status</th>
	              <th>Notes</th>
	            </tr>
	          </thead>
	          <tbody>
	                <?php if(!empty($recent_orders)){
	                        if(is_array($recent_orders)){                    
	                          foreach ($recent_orders as $row) {?>
	                <tr>
	                  <td><?php echo $row['order_number']; ?></td>
	                  <td><?php echo $row['bill_to']; ?></td>
	                  <td><?php echo $row['order_date']; ?></td>
	                  <td><?php 
              			$status = $row['status'];
              			if($status == 'Pending'){?>
              				<h4><label class="label label-warning"><?php echo $row['status']; ?></label></h4>
              		<?php }elseif($status == 'Processing'){ ?>
              				<h4><label class="label label-success"><?php echo $row['status']; ?></label></h4>
              	    <?php }elseif($status == 'On Hold'){ ?>
				   			<h4><label class="label label-warning"><?php echo $row['status']; ?></label></h4>
			   		<?php }elseif($status == 'Cancelled'){ ?>
			   				<h4><label class="label label-danger"><?php echo $row['status']; ?></label></h4>
              		<?php }elseif($status == 'Delivered'){ ?>
			   				<h4><label class="label label-success"><?php echo $row['status']; ?></label></h4>
			   		<?php } ?>
              		  </td>
	                  <td><?php echo $row['notes']; ?></td>
	                </tr>
	                <?php } } }else{ ?>
	                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no recent orders'; ?></span></p></tr>
	                 <?php } ?>
	            </tbody>
	      </table>
	      <div class="span12" style="text-align:center;">
	        <a href="<?php echo base_url(); ?>admin/orders" class="btn btn-success">
	          View all orders
	        </a>
	      </div>
	    </div>
	
	    <hr>
	
	    <div class="panel panel-primary">
	      <div class="panel-heading"><h4 class="text-center">Recent Customers</h4></div>
	    </div>
	    <div class="table-responsive">
	      <table class="table table-striped">
	        <thead>
	            <tr>
	              <th>First Name</th>
	              <th>Last Name</th>
	              <th>Emails</th>
	              <th>Address</th>
	            </tr>
	          </thead>
	          <tbody>
	                <?php if(!empty($recent_customers)){
	                        if (is_array($recent_customers)){                      
	                          foreach ($recent_customers as $row) {?>
	                <tr>
	                  <td><?php echo $row['fname']; ?></td>
	                  <td><?php echo $row['lname']; ?></td>
	                  <td><?php echo $row['email']; ?></td>
	                  <td><?php echo $row['address']; ?></td>
	                </tr>
	                <?php } } }else{ ?>
	                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no recent customers'; ?></span></p></tr>
	                 <?php } ?>
	          </tbody>
	      </table>
	      <div class="span12" style="text-align:center;">
	        <a href="<?php echo base_url(); ?>admin/customers" class="btn btn-success">
	          View all customers
	        </a>
	      </div>
	    </div>
		  </div>
	  </div>	
	  	</div>
	  	</div>
	  	
	  </div>
</div>
