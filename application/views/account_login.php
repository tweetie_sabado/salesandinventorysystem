<div class="container">

	    <?php 
	  	if(validation_errors() == FALSE){
	  		echo validation_errors();
	  	}
	  	else{
	  	?>	
		  	<div class="alert alert-danger" role="alert"><?php echo validation_errors();?></div>
	  	<?php 
	  	}
	  	?>
	 <div class="col-lg-4 col-lg-offset-4">
      <form class="form-signin" method="post" accept-charset="utf-8" action="<?php echo base_url();?>account_login/validate_account">
        <img src="<?php echo base_url('img/logo.jpg')?>"/>
        <h2 class="form-signin-heading" align="center">Please sign in</h2>
        <div class="form-group">
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        </div>
        <div class="form-group">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <a href="#" data-target="#forgot" data-toggle="modal">Forgot password?</a>
      </form>

       
</div> <!-- /container -->

<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Enter Email Address</h2>
      </div>
      <div class="modal-body">
        
        <form class="form-group" method="post" accept-charset="utf-8" action="<?php echo base_url();?>account_login/forgot_pass">
      	
		    <div class="form-group">
			    <input type="email" class="form-control" id="email" placeholder="Email Address">
			  </div>
    	
    
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Submit"/>
      </div>
      	</form>
    </div>
  </div>
</div>
