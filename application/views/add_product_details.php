<div class="container">
  <h1 class="text-center page-header">Add Product</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  			<span class="glyphicon glyphicon-remove-sign"></span>
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if(!empty($this->upload->display_errors())){?>
  		<div class="alert alert-danger" role="alert">
  			<span class="glyphicon glyphicon-remove-sign"></span>
  		<?php echo $this->upload->display_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		
  		<div class="alert alert-success" role="alert">
  			<span class="glyphicon glyphicon-ok-sign"></span>
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
  		
  		<div class="alert alert-success" role="alert">
  			<span class="glyphicon glyphicon-ok-sign"></span>
  			<?php echo $this->session->flashdata('success');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('error')){?>
  		<div class="alert alert-danger" role="alert">
  			<span class="glyphicon glyphicon-remove-sign"></span>
  			<?php echo $this->session->flashdata('error');?>
  		</div>
  <?php }?>

<div class="container-fluid">
	<div class="col-lg-12">
  	<div class="row">		 
		  <!-- Tab panes -->
		  <form role="form" data-toggle="validator" action="<?php echo base_url();?>admin/add_prod_details" method="post" accept-charset="utf-8" enctype="multipart/form-data">
		  		<div class="col-md-6">

<<<<<<< HEAD
		    	<img class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable" height="150px" width="150px" />
		    
		    		

=======

		    	<img id="image_1" class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable1" height="350px" width="450px" />
		    	<!--<img id="image_2" class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable2" height="150px" width="150px" style="margin-left: 5em;" />
		    	<img id="image_3" class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable3" height="150px" width="150px" />-->
		    		

		    	<img id="image_1" class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable" height="450px" width="450px" />

>>>>>>> 75463eb09b27db642c684db9fe0dcd1f39fb09e9
		    		<script type="text/javascript">
		    			function init() {
						    $("img[data-type=editable]").each(function (i, e) {
						        var _inputFile = $('<input/>')
						            .attr('type', 'file')
						            .attr('name', 'userfile')
						            .attr('id', 'userfile')
						            .attr('hidden', 'hidden')
						            .attr('onchange', 'readImage()')
						            .attr('data-image-placeholder', e.id);
						
						        $(e.parentElement).append(_inputFile);
						
						        $(e).on("click", _inputFile, triggerClick);
						    });
						}
						
						function triggerClick(e) {
						    e.data.click();
						}
						
						Element.prototype.readImage = function () {
						    var _inputFile = this;
						    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
						        var _fileReader = new FileReader();
						        _fileReader.onload = function (e) {
						            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
						            var _img = $("#" + _imagePlaceholder);
						            _img.attr("src", e.target.result);
						        };
						        _fileReader.readAsDataURL(_inputFile.files[0]);
						    }
						};
						
						// 
						// IIFE - Immediately Invoked Function Expression
						// https://stackoverflow.com/questions/18307078/jquery-best-practises-in-case-of-document-ready
						(
						
						function (yourcode) {
						    "use strict";
						    // The global jQuery object is passed as a parameter
						    yourcode(window.jQuery, window, document);
						}(
						
						function ($, window, document) {
						    "use strict";
						    // The $ is now locally scoped 
						    $(function () {
						        // The DOM is ready!
						        init();
						    });
						
						    // The rest of your code goes here!
						}));
		    		</script>

		    		<!--<div style="margin-top: 10px;">
		    			<span class="btn btn-success fileinput-button">
	                    <i class="glyphicon glyphicon-plus"></i>
	                    <span>Choose an image</span>
	                    <input type="file" name="userfile" />
                	</span>
               	 	</div>
               		 -->		    		    			 
		  		</div>
		  		
		  		
		    	<div class="col-md-6">
		    		
		    		<div class="form-group">
				    	<input type="text" class="form-control" name="prodName" placeholder="Product Name" />
				    </div>
				    <div class="form-group">
				    	<textarea class="form-control" rows="3" name="prodDesc" placeholder="Description"></textarea>
				  	</div>
				  	<div class="form-group">
					    <div class="input-group">
					      <div class="input-group-addon">₱ </div>
					      <input type="text" class="form-control" name="regPrice" placeholder="Regular Price""/>
					    </div>
					</div>
					<div class="form-group">
						<label class="label label-danger" for="salePrice">Leave blank if product is NOT on sale.</label>
					    <div class="input-group">
					      <div class="input-group-addon">₱</div>
					      <input type="text" class="form-control" name="salePrice" id="salePrice" placeholder="Sale Price" />
					    </div>
					</div>
				  	<div class="form-group">
				  		<select name="show" class="form-control">
						  <option value="Disabled">Disabled</option>
						  <option value="Enabled">Enabled</option>
						</select>
				  	</div>
				  	<div class="form-group">
				    	<input type="number" class="form-control" name="quantity" placeholder="Quantity" />
				    </div>
				    <div class="form-group">
				    	<label for="category">Category</label>
				    	<select name="category" id="category" class="form-control"">
				    		<?php if(!empty($categories)){
			                        if (is_array($categories)){                      
			                          foreach ($categories as $row) {?>?>
			                          	<option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name']?></option>
			                <?php } } }else{?>
		                      			<option disabled>No Categories</option>
		            		<?php }?>
				    	</select>
				    </div>
				     <div class="form-group">
				    	<label for="supplier">Supplier</label>
				    	<select name="supplier" id="supplier" class="form-control"">
				    		<?php if(!empty($suppliers)){
			                        if (is_array($suppliers)){                      
			                          foreach ($suppliers as $row) {?>?>
			                          	<option value="<?php echo $row['supplier_id'];?>"><?php echo $row['company_name']?></option>
			                <?php } } }else{?>
		                      			<option disabled>No Suppliers</option>
		            		<?php }?>
				    	</select>
				    </div>
				    <div class="form-group pull-right">
				    	<button type="submit" class="btn btn-success">Done</button>
			  			<a href="<?php echo base_url();?>admin/products" class="btn btn-danger" style="align">Cancel</a>		    
				    </div>	
				    </form> 
		    </div>   		
	</div>
	<!--<div class="row">
		<div class="col-md-9" align="right">
			<button name="upload" type="submit" role="button" class="btn btn-success">Save</button>
			<a class="btn btn-danger" href="<?php echo base_url();?>admin/products">Cancel</a>
		</div>
	</div>-->
	</div>
</div>
<<<<<<< HEAD

<script>
  $(function(){
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana'],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function (idx, fontName) {
          fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
      });
      $('a[title]').tooltip({container:'body'});
    	$('.dropdown-menu input').click(function() {return false;})
		    .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
        .keydown('esc', function () {this.value='';$(this).change();});

      $('[data-role=magic-overlay]').each(function () { 
        var overlay = $(this), target = $(overlay.data('target')); 
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });
      if ("onwebkitspeechchange"  in document.createElement("input")) {
        var editorOffset = $('#editor').offset();
        $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
      } else {
        $('#voiceBtn').hide();
      }
	};
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	};
    initToolbarBootstrapBindings();  
	$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
    window.prettyPrint && prettyPrint();
  });

<script type="text/javascript" charset="utf-8">
	function init() {
    $("img[data-type=editable]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);

        $(e.parentElement).append(_inputFile);

        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};

// 
// IIFE - Immediately Invoked Function Expression
// https://stackoverflow.com/questions/18307078/jquery-best-practises-in-case-of-document-ready
(

function (yourcode) {
    "use strict";
    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);
}(

function ($, window, document) {
    "use strict";
    // The $ is now locally scoped 
    $(function () {
        // The DOM is ready!
        init();
    });

    // The rest of your code goes here!
}));

</script>
=======
>>>>>>> 75463eb09b27db642c684db9fe0dcd1f39fb09e9

	
