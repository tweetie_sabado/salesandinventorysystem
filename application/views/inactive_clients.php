<div class="container">
  	<div class="bs-callout bs-cat-four">
	  <h1><span class="glyphicon glyphicon-user"> Inactive Customers</span></h1>
	</div>
   <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <div class="table-responsive">
    <form action="<?php echo base_url();?>admin/bulk_delete_customers" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return customer_form();" class="form-inline">
      <table class="table table-striped table-hover">
      	<thead>
      		<tr>
	      		<th><input type="checkbox" id="checkAll" />&nbsp;<button type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
    				<th><a href="#">First Name</a></th>
    				<th><a href="#">Last Name</a></th>
    				<th><a href="#">Birthdate</a></th>
    				<th><a href="#">Address</a></th>
    				<th><a href="#">Contact Number</a></th>
    				<th><a href="#">Email</a></th>
    				<th><a href="#">Status</a></th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($customers)){
                      if (is_array($customers)){                      
                        foreach ($customers as $row) {?>
            <td><input name="customer[]" value="<?php echo $row['client_id']?>" type="checkbox" class="gc_check"/></td>
            <td><?php echo $row['fname'];?></td>
            <td><?php echo $row['lname'];?></td>
            <td><?php echo $row['bdate'];?></td>
            <td><?php echo $row['address'];?></td>
            <td><?php echo $row['cnum'];?></td>
            <td><?php echo $row['email'];?></td>
             <td><?php
                  $status = $row['status'];
                  if($status == 'active'){ ?>
                      <h4><span class="label label-success"><?php echo $row['status'];?></span></h4>
                  <?php }elseif ($status = 'inactive') { ?>
                      <h4><span class="label label-danger"><?php echo $row['status'];?></span></h4>
                  <?php } ?>
            </td>
          </tr>
          <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no customers';?></span></p></tr>
                 <?php }?>
        </tbody>
      </table>
    </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
 });
</script>
