<div class="container">
  <div class="bs-callout bs-cat-two">
	  <h1><span class="glyphicon glyphicon-shopping-cart"> Purchases</span></h1>
	</div>
  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
    <div class="row">
    <div class="span12" style="border-bottom:1px solid #f5f5f5;">
      <div class="row">
        <div class="span4">
          <?php echo $this->pagination->create_links();?>&nbsp;
        </div>
        <!--onsubmit="return customer_form();" lagay nalang natin to sa onclick function ng delete button.-->
        <div class="span8">
         <form action="<?php echo base_url();?>admin/purchases_report" method="post" accept-charset="utf-8" id="delete_form" class="form-inline" style="float:right">
            <fieldset>
               <input name="sdate" id="sdate" data-provide="datepicker" class="form-control" data-date-format="yyyy/mm/dd" placeholder="Start Date" />
              <input id="start_top_alt" type="hidden" name="start_date" />
              <input name="edate" id="edate" data-provide="datepicker" class="form-control" data-date-format="yyyy/mm/dd" placeholder="End Date" />
              <input id="end_top_alt" type="hidden" name="end_date" />
              <button type="submit" class="btn btn-info" name="search" value="Search">Search</button>
              <button type="submit" class="btn btn-info" name="export" value="export">Export</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div> 
<div class="table-responsive">
      <form action="<?php echo base_url();?>admin/bulk_delete_purchases" method="post" accept-charset="utf-8" id="purchases_form" onsubmit="return purchase_form();" class="form-inline">
        <div style="pull-left">
          <a class="btn btn-info" href="<?php echo base_url();?>admin/add_purchase"><span class="glyphicon glyphicon-plus"></span> Add New Purchase</a>
        </div>
        <table class="table table-striped  table-hover">
        	<thead>
        		<tr>
          		<th><input type="checkbox" id="checkAll"/><button style="margin-left:1.5em;" type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
      				<th><a href="#">Purchase ID</a></th>
      				<th class="col-md-2"><a href="#">Product Name</a></th>
      				<th><a href="#">Category</a></th>
      				<th><a href="#">Supplier</a></th>
              		<th class="col-md-2"><a href="#">Purchase Date</a></th>
      				<th><a href="#">Requested Stocks</a></th>
      				<th class="col-md-2"><a href="#">Price per Stock</a></th>
      				<th><a href="#">Total</a></th>
      				<th class="col-md-2"><a href="#">Status</a></th>
      				<th><a href="#">Action</a></th>
              <th></th>
        		</tr>
        	</thead>
          <tbody>
             <!-- <?php echo '<pre>';print_r($purchases);?>-->
              <?php if(!empty($purchases)){
                        if (is_array($purchases)){                      
                          foreach ($purchases as $row) {?>
            <tr>
              <td><input name="purchase[]" value="<?php echo $row['purchase_id']?>" type="checkbox" class="gc_check"/></td>
              <td><?php echo $row['purchase_id'];?></td>
              <td><?php echo $row['product_name'];?></td> 
              <td><?php echo $row['category_name']?></td> 
              <td><?php echo $row['company_name'];?></td>
              <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['timestamp']));?></td>
              <td><?php echo $row['count'];?></td>   
              <td>₱ <?php $price = $row['reg_price']; $priceFormatted = number_format($price,2); echo $priceFormatted;?></td> 
              <td class="col-md-2">₱ <?php $count = $row['count']; $priceStock = $row['reg_price']; $total = $count*$priceStock; $formattedTotal = number_format($total, 2); echo $formattedTotal; ?></td>
               <td> <?php 
              			$status = $row['status'];
              			if($status == 'Pending'){?>
              				<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Processing'){?>
              				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
              	    <?php }elseif($status == 'On Hold'){?>
				   			<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
			   		<?php }elseif($status == 'Cancelled'){?>
			   				<h4><label class="label label-danger"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Delivered'){?>
			   				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
			   		<?php } ?> 
              </td>
              <td>
                <a href="<?php echo base_url().'admin/edit_purchase/'.$row['purchase_id']?>" class="btn btn-success btn-small"><span class="glyphicon glyphicon-pencil"></span></a>
              </td> 
              <!--   
              <td>
                <a href="<?php echo base_url().'admin/view_purchase/'.$row['purchase_id']?>" class="btn btn-primary btn-small"><span class="glyphicon glyphicon-search"></span></a>
              </td> 
              -->
            </tr>
            <?php } } }else{?>
                      <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no purchases made.';?></span></p></tr>
            <?php }?>
          </tbody>
        </table>
      </form>
   </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
  });


function save_status(id)
{
  show_animation();
  $.post("<?php echo base_url().'admin/edit_purchase/'.$row['purchase_id']?>", { purchase_id: id,  supplier: $('#supplier_form_'+id).val()}, function(data){
    setTimeout('hide_animation()', 500);
  });
}

function show_animation()
{
  $('#saving_container').css('display', 'block');
  $('#saving').css('opacity', '.8');
}

function hide_animation()
{
  $('#saving_container').fadeOut();
}
</script>

<div id="saving_container" style="display:none;">
  <div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
  <img id="saving_animation" src="<?php echo base_url('img/saving.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
  <div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001"><?php echo 'Saving...';?></div>
</div>