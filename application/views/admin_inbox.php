<div class="container">
	
	<div class="page-header">
		<h2 align="center"><label class="label label-primary">Messages</label></h2>
	</div>
	
	<div class="col-md-5">
		<?php foreach($message as $r);?>

        <input type="hidden" name="message_id"/>
        
        <div class="form-group">
        	<label>From:</label>
        	<input class="form-control" id="usender" value="<?php echo $r->sender_id;?>" name="sender" type="text" disabled/>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" id="title" name="title" value="<?php echo $r->title;?>" type="text" disabled/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" id="message" name="message" rows="5" cols="250" disabled><?php echo $r->body;?></textarea>
        </div>
        
     
        <a href="<?php echo base_url('admin/trash/'.$r->message_id);?>" class="btn btn-danger">Trash</a>
        <a href="<?php echo base_url('admin/read/'.$r->message_id);?>" class="btn btn-success">Done</a>
        <a href="#" data-toggle="modal" data-target="#reply" class="btn btn-primary">Reply</a>
		
</div>

<div class="modal fade" id="reply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">New Message</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('admin/reply_message');?>
        
        <div class="form-group">
        	<label>To:</label>
        	<input class="form-control" type="text" value="<?php echo $r->sender_id;?>" name="receiver"/>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" type="text" value="RE: <?php echo $r->title;?>" name="title"/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" name="message" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>