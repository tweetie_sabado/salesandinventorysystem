<div class="container">
  <div class="bs-callout bs-cat-four">
	  <h1><span class="glyphicon glyphicon-list"> Categories</span></h1>
	</div>

  <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
  <div class="table-responsive">
    <form action="<?php echo base_url();?>admin/bulk_delete_cat" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return cat_form();" class="form-inline">
      <div style="text-align:right">
        <a class="btn btn-info" href="<?php echo base_url();?>admin/add_category"><span class="glyphicon glyphicon-plus"></span> Add New Category</a>
      </div>

      <table class="table table-striped table-hover">
      	<thead>
      		<tr>
            <th><input type="checkbox" id="checkAll"/>&nbsp;<button type="submit" class="btn btn-small btn-danger"><i class="glyphicon glyphicon-trash"></i></button></th>
    				<th>Category ID</th>
    				<th>Name</th>
            <th>Description</th>
    				<th>Status</th>
            <th></th>
      		</tr>
      	</thead>
        <tbody>
          <tr>
            <?php if(!empty($categories)){
                        if (is_array($categories)){                      
                          foreach ($categories as $row) {?>
             <td><input name="category[]" value="<?php echo $row['category_id']?>" type="checkbox" class="gc_check"/></td>
             <td><?php echo $row['category_id'];?></td>
             <td><?php echo $row['category_name'];?></td>
             <td><?php echo $row['description'];?></td>
             <td><?php $status = $row['status'];
             	if($status == 'Enabled'){ ?>
                      <h4><span class="label label-success"><?php echo $row['status'];?></span></h4>
                  <?php }elseif ($status = 'Disabled') { ?>
                      <h4><span class="label label-danger"><?php echo $row['status'];?></span></h4>
                  <?php } ?>
             </td>
             <td>
             <a href="<?php echo base_url().'admin/edit_category/'.$row['category_id']?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
             
             </td>   
          </tr> 
           <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no categories';?></span></p></tr>
                 <?php }?> 
        </tbody>
      </table>
    </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
 });
</script>