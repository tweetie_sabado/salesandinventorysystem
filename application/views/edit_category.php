<div class="container">
  <h1 class="text-center page-header">Edit Category</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<span class="glyphicon glyphicon-remove-sign"></span>
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
	  <?php $row = $categories[0];{?>  
		  <form action="<?php echo base_url().'admin/edit_cat/'.$row['category_id']?>" method="post" accept-charset="utf-8">    
		  	<div class="form-group">
		  		<label for="catName">Category Name</label>
		    	<input type="text" class="form-control" id="catName" name="catName" value="<?php echo $row['category_name'];?>">
		    </div>
		    <div class="form-group">
		    	<label for="catDesc">Description</label>
		    	<textarea class="form-control" rows="3" id="catDesc" name="catDesc"><?php echo $row['description'];?></textarea>
		    	<?php } ?>
		  	</div>
		  	
		  	<div class="form-group">
		  		<select name="status" class="form-control">
				  <option>Disabled</option>
				  <option>Enabled</option>
				</select>
		  	</div>
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/categories" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>