<div class="container">
  <h1 class="text-center page-header">Add Purchase</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
		  <form role="form" data-toggle="validator" action="<?php echo base_url();?>admin/add_pur" method="post" accept-charset="utf-8">
		  	<div class="form-group form-inline">
		    	<label for="supplier">Supplier</label>
		    	<select name="supplier" id="supplier" class="form-control">
		    		<?php if(!empty($suppliers)){
	                        if (is_array($suppliers)){                      
	                          foreach ($suppliers as $row) {?>?>
	                          	<option value="<?php echo $row['supplier_id'];?>"><?php echo $row['company_name']?></option>
	                <?php } } }else{?>
                      			<option value=""></option>
            		<?php }?>
		    	</select>
		    	<a href="<?php echo base_url();?>admin/add_supplier" class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add Supplier</a>
		    </div>
		    <div class="form-group form-inline">
		    	<label for="category">Category</label>
		    	<select name="category" id="category" class="form-control">
		    		<?php if(!empty($categories)){
	                        if (is_array($categories)){                      
	                          foreach ($categories as $row) {?>?>
	                          	<option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name']?></option>
	                <?php } } }else{?>
                      			<option value=""></option>
            		<?php }?>
		    	</select>
		    	<a href="<?php echo base_url();?>admin/add_category" class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Add Category</a>
		    </div>
		    <div class="form-group">
		    	<input type="text" class="form-control" name="prodName" placeholder="Product Name">
		    </div>
		 	<div class="form-group form-inline">
		 		<div class="form-group">
		    	<input type="number" class="form-control" name="quantity" placeholder="Quantity">
			    </div>
			    <div class="form-group">
				    <div class="input-group">
					      <div class="input-group-addon">₱</div>
					      <input type="text" class="form-control" name="regPrice" placeholder="Price">
				    </div>
				</div>
		 	</div>
		    <div class="form-group">
		  		 <select class="form-control" name="status">
                      <option>Pending</option>
                      <option>Delivered</option>
                    </select>
		  	</div>
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/purchases" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>