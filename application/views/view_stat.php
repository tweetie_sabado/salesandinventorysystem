 <div class="container">
  <h1 class="text-center page-header">Order Breakdown</h1>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?php foreach($orders as $row){?>


      <?php echo form_open('admin/update_stat/'.$row->customer_id.'/'.$row->order_number);?>
       
          <input type="text" name="order_id" class="hidden" id="order_id" value="<?php echo $row->order_id;?>" />

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><h3 class="text-center">Order Number: <?php echo $row->order_number ;?></h3></h3>
              </div>
              <div class="panel-body">
              	  <div class="row">
              	  	 <div class="col-md-9">
                    <strong>Customer Name:</strong><p><?php echo $row->bill_to;?></p>
                    <strong>Order Date:</strong><p><?php echo date('m/d/y h:i a', strtotime($row->order_date));?></p>
                  </div>
                  <strong>Status:</strong><p><?php echo $row->status;?></p>
              	  </div>
                 
                  
                  <hr>
           			<div class="row">
           				<div class="col-md-12">
           					<div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                      <!--<?php echo '<pre>';print_r($cart_contents);?>-->
                        <?php foreach ($cart_contents as $row1) {?>
                          <tr>
                             <td><?php echo $row1['product_name'];?></td>
                             <td><?php echo $row1['quantity'];?></td>
                             <td>₱ <?php $price = $row1['reg_price']; $formattedPrice = number_format($price, 2); echo $formattedPrice;?></td>
                             <td class="text-left">₱ <?php $qty = $row1['quantity']; $price = $row1['reg_price']; $mul = $qty*$price; $formattedAmount = number_format($mul, 2); echo $formattedAmount;?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td>Grand Total</td>
                          <td></td>
                          <td></td>
                          <td class="text-left">₱ <?php $amount = $row->total; $formattedAmount = number_format($amount, 2); echo $formattedAmount;?></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
           				</div>
           				
           				
           			</div>
                  
                  
              </div>
            </div>

            
                  <div class="form-group">
                  <label for="order_status">Edit Status</label>
                     <select class="form-control" name="order_status">
                      <option>Pending</option>
                      <option>Processing</option>
                      <option>On Hold</option>
                      <option>Cancelled</option>
                      <option>Delivered</option>
                    </select>
                  </div>
                  <input type="submit" name="Submit" value="Update"  class="btn btn-success">
                  <a href="<?php echo base_url();?>admin/orders" class="btn btn-danger" style="align">Cancel</a>
        <?php } ?>
        <?php form_close();?>
        </div>
        </div>
    </div>