<div class="container">
   <div class="bs-callout bs-cat-three">
	  <h1><span class="glyphicon glyphicon-list-alt"> Orders&nbsp;Archives</span></h1>
	</div>
   <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
 <div class="table-responsive">
      <form action="<?php echo base_url();?>admin/move_orders" method="post" accept-charset="utf-8" id="archive_form" onsubmit="return archive_form();" class="form-inline">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><input type="checkbox" id="checkAll" />&nbsp;<button type="submit" data-toggle="tooltip" title="Move back to ORDERS" class="btn btn-small btn-warning"><i class="glyphicon glyphicon-ok"></i></button></th>
              <th><a href="#">Order Number</a></th>
              <th><a href="#">Customer Name</a></th>
              <th><a href="#">Order Date</a></th>
              <th><a href="#">Status</a></th>
              <th><a href="#">Total</a></th>
            </tr>
          </thead>
          <tbody>
              <?php if(!empty($archives)){
                        if (is_array($archives)){                      
                          foreach ($archives as $row) {?>
            <tr>
              <td><input name="archive[]" value="<?php echo $row['order_id']?>" type="checkbox" class="gc_check"/></td>
              <td><?php echo $row['order_number'];?></td>
              <td><?php echo $row['bill_to'];?></td>
              <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['timestamp']));?></td>
              <td><?php 
              			$status = $row['status'];
              			if($status == 'Pending'){?>
              				<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Processing'){?>
              				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
              	    <?php }elseif($status == 'On Hold'){?>
				   			<h4><label class="label label-warning"><?php echo $row['status'];?></label></h4>
			   		<?php }elseif($status == 'Cancelled'){?>
			   				<h4><label class="label label-danger"><?php echo $row['status'];?></label></h4>
              		<?php }elseif($status == 'Delivered'){?>
			   				<h4><label class="label label-success"><?php echo $row['status'];?></label></h4>
			   		<?php } ?>
              </td>
              <td class="text-left">₱ <?php $amount = $row['total']; $formattedAmount = number_format($amount, 2); echo $formattedAmount;?></td>
              <td></td>
            </tr>
            <?php } } }else{?>
                      <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no archived orders';?></span></p></tr>
            <?php }?>
          </tbody>
        </table>
      </form>
   </div>
</div>
<script>
    $( document ).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>
