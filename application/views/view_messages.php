<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-cog"></span> Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account"><span class="fa fa-user"></span> My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages"><span class="fa fa-envelope-o"></span> My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>account/info"><i class="fa fa-info-circle"></i> About Us</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout"><span class="fa fa-power-off"></span> Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
	 <?php if($this->session->flashdata('sent')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('sent');?>
      </div>
  <?php }?>
	<div class="page-header">
		<h2 align="center"><label class="label label-primary">Messages</label></h2>
	</div>

	<div class="row">
		<div class="col-md-2">
			<div>
			<button class="btn btn-primary" data-toggle="modal" data-target="#new_message">New Message <i class="fa fa-plus-square"></i></button>
			</div>
			</br>
			<div>
			<ul class="nav nav-pills nav-stacked">
			  <li role="presentation" class="active"><a href="#inbox" data-toggle="tab">Inbox <span class="badge"><i class="fa fa-envelope"></i> <?php echo $ct_inbox;?></span></a></li>
			  <li role="presentation"><a href="#outbox" data-toggle="tab">Outbox <span class="badge"><i class="fa fa-share"></i> <?php echo $ct_outbox;?></span></a></li>
			  <li role="presentation"><a href="#trash" data-toggle="tab">Trash <span class="badge"><i class="fa fa-trash"></i> <?php echo $ct_trash;?></span></a></li>
			  
			</ul>
			</div>
		</div>
		
		<div class="col-md-10">
			
			<div class="tab-content">
		
				<div role="tabpanel" class="tab-pane active" id="inbox">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">Message ID</th>	
				          <th style="width:200px;">From</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				          <th style="width:100px;">Status</th>
				          <th style="width:100px;">Action</th>
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($inbox)): foreach($inbox as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->message_id;?></td>	
				      	  <td width="100" style="text-align:center;"> <?php echo $row->sender_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->status;?></td>
				          <td width="100" style="text-align:center;"> <a href="<?php echo base_url('account/inbox/'.$row->message_id);?>" class="btn btn-primary">View</a></td>
				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
				</div>
				
				<div role="tabpanel" class="tab-pane" id="outbox">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">To</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				 
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($outbox)): foreach($outbox as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->receiver_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>
			
				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
					
				</div>
				<div role="tabpanel" class="tab-pane" id="trash">
					
					<table class="table table-bordered table-striped">
		      
				      <thead>
				        <tr>
				          <th style="width:200px;">From</th>
				          <th style="width:200px;">Date</th>
				          <th style="width:200px;">Title</th>
				          <th style="width:400px;">Message</th>
				  
				        </tr>
				      </thead>
				
				      <tbody>
				        <tr>
				      <?php if(isset($trash)): foreach($trash as $row):?>
				      	  <td width="100" style="text-align:center;"> <?php echo $row->sender_id;?></td>
				          <td width="100" style="text-align:center;"> <?php echo date('F d,Y (D) / h:i A', strtotime($row->timestamp));?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->title;?></td>
				          <td width="100" style="text-align:center;"> <?php echo $row->body;?></td>
	
				
				        </tr><?php endforeach?>        
				      <?php else: ?>
				      <th><div>No records</div></th>
				      <?php endif;?>
				      </tbody>
				    </table>
					
				</div>
				
		</div>

		</div>
	</div>
</div>

<div class="modal fade" id="new_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">New Message</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('account/add_message');?>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" type="text" name="title"/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" name="message" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="read_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">New Message</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('account/edit_message');?>
        <input type="hidden" id="mid" name="umessage_id"/>
        
        <div class="form-group">
        	<label>From:</label>
        	<input class="form-control" id="usender" name="usender" type="text"/>
        </div>
        
        <div class="form-group">
        	<label>Title:</label>
        	<input class="form-control" id="title" name="utitle" type="text"/>
        </div>
        
        <div class="form-group">
        	<label>Message:</label>
        	<textarea class="form-control" id="message" name="umessage" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Send" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>


