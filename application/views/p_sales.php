<div class="container">
  <h1 class="text-center page-header">Sales</h1>
  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-warning" role="alert">
        <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
  <div><h3>TOTAL: ₱ <strong><?php echo $this->cart->format_number($tot->total);?></strong></h3></div>

<div align="right"> 
  <button onclick="myFunction()">Print</button>
</div>

<script>
  function myFunction() {
      window.print();
  }
</script>

  <div class="table-responsive">
      <form action="<?php echo base_url();?>admin/bulk_delete_order" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return submit_form();" class="form-inline">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><a href="#">Order Number</a></th>
              <th><a href="#">Customer Name</a></th>
              <th><a href="#">Ordered On</a></th>
              <th><a href="#">Status</a></th>
              <th><a href="#">Total</a></th>
            </tr>
          </thead>
          <tbody>
              <?php if(!empty($sales)){
                        if (is_array($sales)){                      
                          foreach ($sales as $row) {?>
            <tr>
              <td><?php echo $row['order_number'];?></td>
              <td><?php echo $row['bill_to'];?></td>
              <td><?php echo date('F d,Y (D) / h:i A', strtotime($row['order_date']));?></td>
              <td><?php echo $row['status'];?></td>
              <td>₱ <?php echo $this->cart->format_number($row['total']);?></td>
              <td></td>
            </tr>
            <?php } } }else{?>
                      <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no orders';?></span></p></tr>
            <?php }?>
          </tbody>
        </table>
      </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
  });
});

function save_status(id)
{
  show_animation();
  $.post("<?php echo base_url().'admin/edit_order/'.$row['order_id']?>", { order_id: id, status: $('#status_form_'+id).val()}, function(data){
    setTimeout('hide_animation()', 500);
  });
}

function show_animation()
{
  $('#saving_container').css('display', 'block');
  $('#saving').css('opacity', '.8');
}

function hide_animation()
{
  $('#saving_container').fadeOut();
}
</script>

<div id="saving_container" style="display:none;">
  <div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
  <img id="saving_animation" src="<?php echo base_url('img/saving.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
  <div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001"><?php echo 'Saving...';?></div>
</div>
