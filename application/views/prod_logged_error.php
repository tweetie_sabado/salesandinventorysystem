<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/cat_logged_in/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account">My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages">My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout">Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
<div class="container">
  <?php if($this->session->flashdata('ex')){?>
      <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('ex');?>
      </div>
  <?php }?>
</div>  
<div class="container">
          
           
            <div class="page-header">
              <?php    
              foreach ($products as $row) {
                 echo '<h2 style="font-weight:normal">'.$row->product_name.'';
              }
              ?></h2>
            
            </div>

          <div class="col-md-4">
             <div>
                <img class="img-responsive" src="<?php echo $row->image_url?>" alt=""/>
            </div>


        </div>
     <div class="col-md-8 container-fluid">
     <div class="row">
         <div class="col-md-8 col-sm-8 col-xs-8" style="margin-top:10px;">
          
            <?php foreach ($products as $row) ?>
                    
                    <h4 style="font-weight:normal">                                            
                    <span class="pull-left">
                    <medium>Price:</medium>
                    <span>₱ <?php echo $this->cart->format_number($row->reg_price);?></span>
                    <input type="text" class="hidden" name="reg_price" value="<?php echo $row->reg_price;?>">
                  </span>
                    </h4>
                </div>
              </div>  
              
       <div class="row">        
         <div class="col-md-8 col-sm-8 col-xs-8" style="margin-top:10px;">
            <h4 style="font-weight:normal">
            <span class="pull-left">
                    <medium>Available Stock:</medium>
                    <span><?php echo $count;?></span>
                    </span>              
            </h4>
        </div>
        
       </div> 
       
      <div class="row">
        <div class="col-md-5 col-sm-5">
     
          <div class="row" style="margin-top:10px;">
          <?php echo form_open('account/add_cart/'.$row->product_id.'/'.$count);?>
            <form class="form-inline navbar-left" style="margin-top:10px;">
              <div class="col-md-6 col-xs-6 col-sm-6">
                  <div class="input-group form-group has-error">
                    <div class="input-group-addon">QTY.</div>
                     <input name="qty" type="number" class="form-control" placeholder="">
                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                        <span id="inputError2Status" class="sr-only">(error)</span>
                  </div>
              </div>
               <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</button>
            </form>
          </div>
         
          <?php echo form_close();?>  
        </div>
      </div>
    </div>    
</div>

<div class="container">
<ul class="nav nav-pills">
  <li role="presentation" class="active"><a href="#desc" data-toggle="tab"><i class="fa fa-align-left"></i> Description</a></li>
  <li role="presentation"><a href="#reviews" data-toggle="tab"><i class="fa fa-eye"></i> Reviews</a></li>
  <li role="presentation"><a href="#howto" data-toggle="tab"><i class="fa fa-pencil-square-o"></i> How to Order</a></li>
</ul>

	<div class="tab-content">
		
		<div role="tabpanel" class="tab-pane active" id="desc">
			<h2>Description:</h2>
			<p><?php echo $row->product_description;?></p>
			
		</div>
		<div role="tabpanel" class="tab-pane" id="reviews">
			
			<h2>Reviews:</h2>
			<button class="btn btn-primary small" data-toggle="modal" data-target="#review">Add Review</button>
			<?php foreach($review as $row){;?>
			
			<h4><?php echo $row->fname.' '.$row->lname;?></h4>
			<input id="input-id" type="number" name="rating" glyphicon ="true" class="rating form-control" min="0" max="5" step="1" data-size="xs" readonly="true" value="<?php echo $row->rating;?>">
			<p><?php echo $row->review;?></p>
			</br>
			<?php };?>
		</div>
		<div role="tabpanel" class="tab-pane" id="howto">
			<h2>How to Order:</h2>
			<p>1. Create an account</p>
			<p>2. Confirm your account through your registered email.</p>
			<p>3. Login your account</p>
			<p>4. Pick a product category from the list.</p>
			<p>5. Choose your desired product from the list.</p>
			<p>6. Input desired quantity and click <span class="glyphicon glyphicon-shopping-cart"></span> Add to cart button.</p>
			<p>7. Click the cart icon <span class="glyphicon glyphicon-shopping-cart"></span> at the upper right of your dashboard when you are done placing your items.</p>
			<p>8. Fill up the necessary information needed.</p>
			<p>9. Finally submit your order.</p>
		</div>
	</div>
	
</div>

<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Cart</h2>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Product Name</th>
                  <th width="100"style="text-align:center;">Price</th>
                  <th width="100"style="text-align:center;">Quantity</th>
                </tr>
              </thead>
              <tbody>
              
     
                <tr>
                <?php if(isset($cart)) : foreach($cart as $row):?> 
                  <tr>
                  <td width="10" style="text-align:center;"> <?php echo $row->product_name;?></td>
                  <td width="100" style="text-align:center;">₱ <?php echo $this->cart->format_number($row->reg_price);?></td>
                  <td width="100" style="text-align:center;"> <?php echo $row->quantity;?></td>
                  <td width="100" style="text-align:center;"><a href="<?php echo base_url();?>account/cart_del/<?php echo $row->cart_id;?>" class="btn btn-danger">Delete</a></td>    
                  </tr><?php endforeach;?>
                  <?php else: ?>
                    <th><div></div></th>
                    <?php endif;?>
                </tr>

              </tbody>
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url();?>order" type="button" class="btn btn-success">Check out</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Add Product Review</h2>
      </div>
      <div class="modal-body">
        <?php echo form_open('account/add_review/'.$row->product_id);?>
        
        <div class="form-group">
        	<label>Add Rating:</label>
        	<input id="input-id" type="number" name="rating" glyphicon ="true" class="rating form-control" min="0" max="5" step="1" data-size="xs" data-rtl="false">
        </div>
        
        <div class="form-group">
        	<label>Review:</label>
        	<textarea class="form-control" name="review" rows="5" cols="250"></textarea>
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Submit" name="submit"/>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$("#input-id").rating();
</script>