<div class="container">
  <h1 class="text-center page-header">Edit Product</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
		<span class="glyphicon glyphicon-remove-sign"></span>
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if(!empty($this->upload->display_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo $this->upload->display_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		
  		<div class="alert alert-success" role="alert">
  			<span class="glyphicon glyphicon-ok-sign"></span>
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('success')){?>
  		
  		<div class="alert alert-success" role="alert">
  			<span class="glyphicon glyphicon-ok-sign"></span>
  			<?php echo $this->session->flashdata('success');?>
  		</div>
  <?php }?>
  <?php if($this->session->flashdata('error')){?>
  		<div class="alert alert-danger" role="alert">
  			<span class="glyphicon glyphicon-remove-sign"></span>
  			<?php echo $this->session->flashdata('error');?>
  		</div>
  <?php }?>
  <?php $row = $products[0];{?>  
  <form action="<?php echo base_url().'admin/edit_prod/'.$row['product_id']?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">    
  <div class="row">
  		<div class="col-md-6">
				<?php if(!empty($row['image_url'])){?>
		    	<img id="image_1" class="img-thumbnail" src="<?php echo $row['image_url'];?>" data-type="editable" height="450px" width="450px" />
				<?php }else{?>
				<img id="image_1" class="img-thumbnail" src="<?php echo base_url();?>img/product.jpg" data-type="editable" height="450px" width="450px" />	
				<?php } ?>
		    		<script type="text/javascript">
		    			function init() {
						    $("img[data-type=editable]").each(function (i, e) {
						        var _inputFile = $('<input/>')
						            .attr('type', 'file')
						            .attr('name', 'userfile')
						            .attr('id', 'userfile')
						            .attr('hidden', 'hidden')
						            .attr('onchange', 'readImage()')
						            .attr('data-image-placeholder', e.id);
						
						        $(e.parentElement).append(_inputFile);
						
						        $(e).on("click", _inputFile, triggerClick);
						    });
						}
						
						function triggerClick(e) {
						    e.data.click();
						}
						
						Element.prototype.readImage = function () {
						    var _inputFile = this;
						    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
						        var _fileReader = new FileReader();
						        _fileReader.onload = function (e) {
						            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
						            var _img = $("#" + _imagePlaceholder);
						            _img.attr("src", e.target.result);
						        };
						        _fileReader.readAsDataURL(_inputFile.files[0]);
						    }
						};
						
						// 
						// IIFE - Immediately Invoked Function Expression
						// https://stackoverflow.com/questions/18307078/jquery-best-practises-in-case-of-document-ready
						(
						
						function (yourcode) {
						    "use strict";
						    // The global jQuery object is passed as a parameter
						    yourcode(window.jQuery, window, document);
						}(
						
						function ($, window, document) {
						    "use strict";
						    // The $ is now locally scoped 
						    $(function () {
						        // The DOM is ready!
						        init();
						    });
						
						    // The rest of your code goes here!
						}));
		    		</script>

		    		<!--<div style="margin-top: 10px;">
		    			<span class="btn btn-success fileinput-button">
	                    <i class="glyphicon glyphicon-plus"></i>
	                    <span>Choose an image</span>
	                    <input type="file" name="userfile" />
                	</span>
               	 	</div>
               		 -->		    		    			 
		  		</div>
	  <div class="col-md-6">
		  	<div class="form-group">
		  		<label for="prodName">Product Name</label>
		    	<input type="text" class="form-control" id="prodName" name="prodName" value="<?php echo $row['product_name'];?>">
		    </div>
		    <div class="form-group">
		    	<label for="catDesc">Description</label>
		    	<textarea class="form-control" rows="3" id="prodDesc" name="prodDesc"><?php echo $row['product_description'];?></textarea>
		    	<?php } ?>
		  	</div>
		  	<div class="form-group">
		  	 	<label for="regPrice">Regular Price</label>
			    <div class="input-group"> 
			      <div class="input-group-addon">₱ </div>
			      <input type="text" class="form-control" name="regPrice" placeholder="Regular Price" value="<?php echo $row['reg_price'];?>">
			    </div>
			</div>
			<div class="form-group">
				<label for="salePrice">Sale Price</label>
				<label class="label label-danger" for="salePrice">Leave blank if product is NOT on sale.</label>
			    <div class="input-group">
			      <div class="input-group-addon">₱</div>
			      <input type="text" class="form-control" name="salePrice" id="salePrice" placeholder="Sale Price" value="<?php echo $row['sale_price'];?>">
			    </div>
			</div>
		  	<div class="form-group">
				<label for="quantity">Quantity</label>
		    	<input type="number" class="form-control" name="quantity" placeholder="Quantity" value="<?php echo $row['current_count'];?>">
		    </div>
		    <div class="form-group">
		  		<select name="show" class="form-control">
				  <option value="Disabled">Disabled</option>
				  <option value="Enabled">Enabled</option>
				</select>
		  	</div>
		    <div class="form-group">
		    	<label for="category">Category</label>
		    	<select name="category" id="category" class="form-control">
		    		<?php if(!empty($categories)){
	                        if (is_array($categories)){                      
	                          foreach ($categories as $row) {?>?>
	                          	<option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name']?></option>
	                <?php } } }else{?>
                      			<option value=""></option>
            		<?php }?>
		    	</select>
		    </div>
		    <div class="form-group">
		    	<label for="supplier">Supplier</label>
		    	<select name="supplier" id="supplier" class="form-control">
		    		<?php if(!empty($suppliers)){
	                        if (is_array($suppliers)){                      
	                          foreach ($suppliers as $row) {?>?>
	                          	<option value="<?php echo $row['supplier_id'];?>"><?php echo $row['company_name']?></option>
	                <?php } } }else{?>
                      			<option value=""></option>
            		<?php }?>
		    	</select>
		    </div>
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/products" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>