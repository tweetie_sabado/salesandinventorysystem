<div class="container">
  <h1 class="text-center page-header">Products</h1>
  <?php if(!empty(validation_errors())){?>
      <div class="alert alert-danger" role="alert">
      <?php echo validation_errors();?>
      </div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
      <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('message');?>
      </div>
  <?php }?>
 <div align="right"> 
<button onclick="myFunction()">Print</button>
</div>
  <div class="table-responsive">
    <form action="<?php echo base_url();?>admin/bulk_delete_prod" method="post" accept-charset="utf-8" id="delete_form" onsubmit="return prod_form();" class="form-inline">
      
      <table class="table table-striped">
      	<thead>
      		<tr>
    				<th>ID</th>
    				<th>Name</th>
    				<th>Description</th>
            <th>Stock</th>
            <th>Unit Price</th>
      		</tr>
      	</thead>
        <tbody>
               <?php if(!empty($products)){
                        if (is_array($products)){                      
                          foreach ($products as $row) {?> 
          <tr>
             <td><?php echo $row['product_id'];?></td>
             <td><?php echo $row['product_name'];?></td>
             <td><?php echo $row['product_description'];?></td>
             <td><?php echo $row['count'];?>
             </td>
             <td><?php echo $row['reg_price'];?></td> 
          </tr> 
           <?php } } }else{?>
                    <tr><p class="text-center"><span class="label label-warning"><?php echo 'There are currently no categories';?></span></p></tr>
                 <?php }?> 
        </tbody>
      </table>
    </form>
   </div>
</div>
<script type="text/javascript">
$("#checkAll").click(function () {
    $(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.gc_check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
 });
</script>

<script>
function myFunction() {
    window.print();
}
</script>