<div class="container">
  <h1 class="text-center page-header">Edit Supplier</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<span class="glyphicon glyphicon-remove-sign"></span>
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
	  <?php $row = $suppliers[0];{?>  
		  <form action="<?php echo base_url().'admin/edit_sup/'.$row['supplier_id']?>" method="post" accept-charset="utf-8">    
		  	<div class="form-group">
		  		<label for="compName">Company Name</label>
		    	<input type="text" class="form-control" id="compName" name="compName" value="<?php echo $row['company_name'];?>">
		    </div>
		   
		    
		     <div class="form-inline form-group">
		     	 <label for="supFirstName">First Name</label>
		    	<input type="text" class="form-control" name="supFirstName" value="<?php echo $row['sup_fname'];?>" required>
		    	<label for="supLastName">Last Name</label>
		    	<input type="text" class="form-control" name="supLastName" value="<?php echo $row['sup_lname'];?>" required>
		    	<div class="help-block with-errors"></div> 
		    </div>
		     <div class="form-group">
		     	<label for="cnum">Contact Number</label>
		    	<input type="text" data-minlength="11" class="form-control" name="cnum" value="<?php echo $row['cnum'];?>" required>
		    </div>
		    <div class="form-group">
		    	<label for="email">Email Address</label>
		    	<input type="email" class="form-control" name="email" value="<?php echo $row['email'];?>" required>
		    	<div class="help-block with-errors"></div> 
		    </div>
		    <div class="form-group">
		    	<textarea class="form-control" rows="3" name="add" placeholder="Address" required><?php echo $row['address'];?></textarea>
		  	</div>
		  	<div class="form-group">
		  		<select name="status" class="form-control">
		  			<option value="active">Active</option>
		  			<option value="inactive">Inactive</option>
		  		</select>
		  	</div>
	    	<?php } ?>
		 
		  	
		  
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/suppliers" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>