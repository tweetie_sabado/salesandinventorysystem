<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
              echo '<li><a href="'.base_url().'category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>';	
            }
            ?>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>home/info"><i class="fa fa-info-circle"></i> About Us</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
     <!-- <form class="form-inline navbar-left" role="search" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form> -->
        <li><a href="<?php echo base_url();?>account_login"><i class="fa fa-user"></i> Login</a></li>
        <li><a href="<?php echo base_url();?>home/signup"><i class="fa fa-pencil"></i> Sign Up</a></li>
     <li style="margin-top:2px"><script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
			<div id="SkypeButton_Call_pcfast.computer_1">
			  <script type="text/javascript">
			    Skype.ui({
			      "name": "call",
			      "element": "SkypeButton_Call_pcfast.computer_1",
			      "participants": ["pcfast.computer"],
			      "imageSize": 16
			    });
			  </script>
			</div>
		</li>
		<li><a href="https://facebook.com/PCFastComputer"><img src="<?php echo base_url();?>img/facebook-icon.png" /></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>	


<div class="container">

      <?php 
      if(validation_errors() == FALSE){
        echo validation_errors();
      }
      else{
      ?>  
        <div class="alert alert-danger" role="alert"><?php echo validation_errors();?></div>
      <?php 
      }
      ?>
      <?php if($this->session->flashdata('age')){?>
      <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('age');?>
      </div>
  		<?php }?>
   <div class="col-lg-4 col-lg-offset-4">
     <?php echo form_open('home/add_clients');?>
        	<p>Fields with (*) are required.</p>
              <div class="form-group">
                    <label>E-mail Address*</label>
                    <input type="email" name="email" class="form-control" id="email" required/>
                    </div>

                    <div class="form-group">
                    <label>Password*</label>
                    <input type="password" pattern=".{8,}"   required title="8 characters minimum" name="password" class="form-control" id="password" required/>
                    </div>

                    <div class="form-group">
                    <label>Confirm Password*</label>
                    <input type="password" name="conpass" class="form-control" id="confirm_password" required/>
                    </div>

                    <div class="form-group">
                    <label>Choose a Secret Question*</label>
                    <select name="quest" class="form-control" id="quest" required>
                    <option></option>
                    <option>-- Select a Secret Question --</option>
                    <option>What is your maternal grandmother's maiden name?</option>
                    <option>Name of the street where you grew up?</option>
                    <option>Name of your first pet?</option>
                    <option>Name of your favorite teacher in high school?</option>
                    <option>What is your favorite song?</option>
                    </select>
                    </div>

                    <div class="form-group">
                    <label>Answer*</label>
                    <input type="text" name="ans" class="form-control" id="ans" required/>
                    </div>
        			<hr>
                    <div class="form-group">
                    <label>First Name*</label>
                    <input type="text" name="fname" class="form-control" id="fname" required/>
                    </div>

                    <div class="form-group">
                    <label>Last Name*</label>
                    <input type="text" name="lname" class="form-control" id="lname" required/>
                    </div>
                
                    <div class="form-group">
                     <label>Birthdate*</label>
                     <input name="bdate" id="bdate" data-provide="datepicker" class="form-control" data-date-format="yyyy/mm/dd" required/>
                    </div>
                
                	<div class="form-group">
                      <label>City*</label>
                      <select id="city" name="city" class="form-control" required>
                      <option></option>
                      <option>-- Select a Region --</option>
                      <option>Caloocan City</option>
                      <option>City of Malabon</option>
                      <option>City of Manila</option>
                      <option>City of Navotas</option>
                      <option>Las Pinas City</option>
                      <option>Makati City</option>
                      <option>Mandaluyong City</option>
                      <option>Marikina City</option>
                      <option>Muntinlupa City</option>
                      <option>Paranaque City</option>
                      <option>Pasay City</option>
                      <option>Pasig City</option>
                      <option>Pateros</option>
                      <option>Quezon City</option>
                      <option>San Juan City</option>
                      <option>Taguig City</option>
                      <option>Valenzuela City</option> 
                      </select>
                  	</div>
                	
                   <div class="form-group">
                      <label>Address*</label>
                      <textarea  name="address" class="form-control" id="address" rows="4" cols="50" required></textarea>
                  </div>
                  
                  <div class="form-group">
                      <label>Village/Barangay</label>
                      <input type="text" name="brgy" class="form-control" id="brgy" />
                  </div>

                   <div class="form-group">
                      <label>Contact Number*</label>
                      <input type="text" name="cnum" class="form-control" id="cnum"/>
                  </div>

        <button type="submit" class="btn btn-primary">Sign up</button>
        <?php echo form_close();?> 
    </div>
</div> <!-- /container -->
</div>

<script>
	var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>