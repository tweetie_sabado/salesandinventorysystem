<div class="container">
  <h1 class="text-center page-header">Add Supplier</h1>
  <?php if(!empty(validation_errors())){?>
  		<div class="alert alert-danger" role="alert">
  		<?php echo validation_errors();?>
  		</div>
  <?php } ?>
  <?php if($this->session->flashdata('message')){?>
  		<div class="alert alert-success" role="alert">
  			<?php echo $this->session->flashdata('message');?>
  		</div>
  <?php }?>
  <div class="row">
	  <div class="col-md-6 col-md-offset-3">
		  <form role="form" data-toggle="validator" action="<?php echo base_url();?>admin/add_sup" method="post" accept-charset="utf-8">
		  	<div class="form-group">
		    	<input type="text" class="form-control" name="comp" placeholder="Company" required>
		    </div>
		    <div class="form-inline form-group">
		    	<input type="text" class="form-control" name="supFirstName" placeholder="First Name" required>
		    	<input type="text" class="form-control" name="supLastName" placeholder="Last Name" required>
		    	<div class="help-block with-errors"></div> 
		    </div>
		    <div class="form-inline form-group">
		    	<input type="text" data-minlength="11" class="form-control" name="cnum" placeholder="Contact Number" required>
		    	<input type="email" class="form-control" name="email" placeholder="Email Address" required>
		    	<div class="help-block with-errors"></div> 
		    </div>
		    <div class="form-group">
		    	<textarea class="form-control" rows="3" name="add" placeholder="Address" required></textarea>
		  	</div>
		  
		  	<div align="right">
			  	<button type="submit" class="btn btn-primary">Done</button>
			  	<a href="<?php echo base_url();?>admin/suppliers" class="btn btn-danger" style="align">Cancel</a>
		  	</div>
		  </form>
	  </div>
  </div>
</div>