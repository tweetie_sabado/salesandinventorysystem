<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>account"><img src="<?php echo base_url('img/logo.jpg');?>"  style="width:70px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-barcode"></span> Products <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php 

            foreach($q1 as $row)
            { 
              
             echo '<li><a href="'.base_url().'account/category/'.$row->category_id.'">'.$row->category_name.'</a></li>';
              echo '<li class="divider"></li>'; 
            }
            ?>
          </ul>
        </li>
        
        <li>
          <?php foreach($name as $row);?>

          <a class="navbar-brand" href="<?php echo base_url()?>account">Welcome <?php echo $row->fname;?> <?php echo $row->lname;?> !</a>

        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      <form class="form-inline navbar-left" role="search" method="post" action="<?php echo  base_url();?>account/search_product" style="margin-top:7px;">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
               <input type="text" name="keyword" class="form-control" placeholder="Search">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-cog"></span> Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          
            <li><a href="<?php echo base_url()?>account/view_account"><span class="fa fa-user"></span> My Account</a></li>
             <li class="divider"></li>
              <li><a href="<?php echo base_url()?>account/view_messages"><span class="fa fa-envelope-o"></span> My Messages</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>account/info"><i class="fa fa-info-circle"></i> About Us</a></li>
             <li class="divider"></li>
             <li><a href="<?php echo base_url()?>logout"><span class="fa fa-power-off"></span> Logout</a></li>
        
          </ul>
        </li>
        <li><a href="" data-toggle="modal" data-target="#cart"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge"><?php echo $num;?></span></a></li>
        <li style="margin-right:10px;"><a><span class="glyphicon glyphicon-envelope"></span> <span class="badge"><?php echo $unread;?></span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="container">
<div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Product Name</th>
                  <th width="100"style="text-align:center;">Price</th>
                  <th width="100"style="text-align:center;">Quantity</th>
                  <th width="100"style="text-align:center;">Total</th>
                </tr>
              </thead>
              <tbody>
              
     
                <tr>
                <?php if(isset($cart)) : foreach($cart as $row):?> 
                  <tr>
                  <td width="10" style="text-align:center;"> <?php echo $row->product_name;?></td>
                  <td width="100" style="text-align:center;">₱ <?php echo $row->reg_price;?></td>
                  <td width="100" style="text-align:center;"> <?php echo $row->quantity;?></td>
                  <td width="100" style="text-align:center;">₱ <?php $qty = $row->quantity; $price = $row->reg_price; $mul = $qty*$price;echo $this->cart->format_number($mul);?></td>    
                </tr><?php endforeach;?>
                  <?php else: ?>
                    <th><div>No records</div></th>
                    <?php endif;?>
                <tr>  
                  <td width="10" style="text-align:center;"></td>
                  <td width="100" style="text-align:center;"></td>
                  <td width="100" style="text-align:center;">Grand Total:</td>
                  <td width="100" style="text-align:center;">₱ <?php $s = $sum->total; echo $this->cart->format_number($s);?></td>

                </tr>
              </tbody>
            </table>
          </div>
          
</div>

<div class="container">
	      <?php 
      if(validation_errors() == FALSE){
        echo validation_errors();
      }
      else{
      ?>  
        <div class="alert alert-danger" role="alert"><?php echo validation_errors();?></div>
      <?php 
      }
      ?>
</div>

<div class="container">
<div class="col-md-6" style="margin-top:10px;">
  <div class="panel panel-primary">
    <div class="panel-heading">Shipping Information</div>
      <div class="panel-body">
        <div class="wrap">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>order/submit_order">

          			<div class="row">
          				<div class="col-md-6">
	                      <label><small>City:</small></label>
		                      <select id="ship_city" name="ship_city" class="form-control">
			                      <option></option>
			                      <option>-- Select a City --</option>
			                      <option>Caloocan City</option>
			                      <option>City of Malabon</option>
			                      <option>City of Manila</option>
			                      <option>City of Navotas</option>
			                      <option>Las Pinas City</option>
			                      <option>Makati City</option>
			                      <option>Mandaluyong City</option>
			                      <option>Marikina City</option>
			                      <option>Muntinlupa City</option>
			                      <option>Paranaque City</option>
			                      <option>Pasay City</option>
			                      <option>Pasig City</option>
			                      <option>Pateros</option>
			                      <option>Quezon City</option>
			                      <option>San Juan City</option>
			                      <option>Taguig City</option>
			                      <option>Valenzuela City</option> 
		                      </select>
                      	</div>
                  	</div>
          				
                        <div class="row">
                          <div class="col-md-6">
                          <label><small>Street:</small></label>
                          <textarea name="ship_street" class="form-control" id="ship_street"></textarea>
                          </div>
                          
                          <div class="col-md-6">
                          <label><small>Village/Barangay:</small></label>
                          <input type="text" name="ship_brgy" class="form-control" id="ship_brgy"/>
                          </div>
						</div>
						
					
		                  <div class="checkbox">
							<label>
								<input name="shipping_add" id="shipping_add" type="checkbox" onclick="shipping_address()"> Use Account Address
							</label>
						  </div>
						  
						  <hr>
						  
						  <div class="row">
                          <div class="col-md-6">
                          <label><small>Contact Number:</small></label>
                          <input type="text" name="ship_cnum" class="form-control" id="ship_cnum"/>
                          </div>
                          
                          <div class="col-md-6">
                          <label><small>Email Address:</small></label>
                          <input type="text" name="ship_email" class="form-control" id="ship_email"/>
                          </div>
						</div>
						
						  <div class="checkbox">
							<label>
								<input name="shipping_cont" id="shipping_cont" type="checkbox" onclick="shipping_contact()"> Use Account Contact
							</label>
						  </div>
						  
                        </div>           
      	</div>
    </div>
   
  </div>
  
  <div class="col-md-6" style="margin-top:10px;">
  <div class="panel panel-primary">
    <div class="panel-heading">Billing Information</div>
      <div class="panel-body">
        <div class="wrap">
            <form class="form-horizontal" method="post">
          	
          				
          			<div class="row">
          				<div class="col-md-6">
	                      <label><small>City:</small></label>
		                      <select id="bill_city" name="bill_city" class="form-control">
			                      <option></option>
			                      <option>-- Select a City --</option>
			                      <option>Caloocan City</option>
			                      <option>City of Malabon</option>
			                      <option>City of Manila</option>
			                      <option>City of Navotas</option>
			                      <option>Las Pinas City</option>
			                      <option>Makati City</option>
			                      <option>Mandaluyong City</option>
			                      <option>Marikina City</option>
			                      <option>Muntinlupa City</option>
			                      <option>Paranaque City</option>
			                      <option>Pasay City</option>
			                      <option>Pasig City</option>
			                      <option>Pateros</option>
			                      <option>Quezon City</option>
			                      <option>San Juan City</option>
			                      <option>Taguig City</option>
			                      <option>Valenzuela City</option> 
		                      </select>
                      	</div>
                  	</div>
          				
                        <div class="row">
                          <div class="col-md-6">
                          <label><small>Street:</small></label>
                          <textarea name="bill_street" class="form-control" id="bill_street"></textarea>
                          </div>
                          
                          <div class="col-md-6">
                          <label><small>Village/Barangay:</small></label>
                          <input type="text" name="bill_brgy" class="form-control" id="bill_brgy"/>
                          </div>
						</div>
						
					
		                  <div class="checkbox">
							<label>
								<input name="billing_add" id="billing_add" type="checkbox" onclick="billing_address()"> Use Account Address
							</label>
						  </div>
						  
						  <hr>
						  
						  <div class="row">
                          <div class="col-md-6">
                          <label><small>Contact Number:</small></label>
                          <input type="text" name="bill_cnum" class="form-control" id="bill_cnum"/>
                          </div>
                          
                          <div class="col-md-6">
                          <label><small>Email Address:</small></label>
                          <input type="text" name="bill_email" class="form-control" id="bill_email"/>
                          </div>
						</div>
						
						  <div class="checkbox">
							<label>
								<input name="billing_cont" id="billing_cont" type="checkbox" onclick="billing_contact()"> Use Account Contact
							</label>
						  </div>
						  
                        </div>           
      	</div>
    </div>
    <span class="pull-right"><input type="submit" class="btn btn-primary" name="submit" value="Submit"/></span>
    </form>
  </div>
  
</div>



<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 align="center">Cart</h2>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Product Name</th>
                  <th width="100"style="text-align:center;">Price</th>
                  <th width="100"style="text-align:center;">Quantity</th>
                </tr>
              </thead>
              <tbody>
              
     
                <tr>
                <?php if(isset($cart)) : foreach($cart as $row):?> 
                  <tr>
                  <td width="10" style="text-align:center;"> <?php echo $row->product_name;?></td>
                  <td width="100" style="text-align:center;">₱ <?php echo $this->cart->format_number($row->reg_price);?></td>
                  <td width="100" style="text-align:center;"> <?php echo $row->quantity;?></td>
                  <td width="100" style="text-align:center;"><a href="<?php echo base_url();?>account/cart_del/<?php echo $row->cart_id;?>" class="btn btn-danger">Delete</a></td>    
                  </tr><?php endforeach;?>
                  <?php else: ?>
                    <th><div></div></th>
                    <?php endif;?>
                </tr>

              </tbody>
            </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="<?php echo base_url();?>order" type="button" class="btn btn-success">Check out</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function shipping_address(){
	
	if( document.getElementById("shipping_add").checked){
		
		street = "<?php foreach($name as $r); echo $r->address;?>";
		city = "<?php foreach($name as $r); echo $r->city;?>";
		brgy = "<?php foreach($name as $r); echo $r->brgy;?>";
		
		document.getElementById("ship_street").value = street;
		document.getElementById("ship_city").value = city;
		document.getElementById("ship_brgy").value = brgy;
		
		
	}else if(!document.getElementById("shipping_add").checked){
		
		document.getElementById("ship_street").value = "";
		document.getElementById("ship_city").value = "";
		document.getElementById("ship_brgy").value = "";
	}
	
}

function shipping_name(){
	
	if( document.getElementById("shipto").checked){
		
		fname = "<?php foreach($name as $r); echo $r->fname;?>";
		lname = "<?php foreach($name as $r); echo $r->lname;?>";
		
		document.getElementById("ship_fname").value = fname;
		document.getElementById("ship_lname").value = lname;
		
		
	}else if(!document.getElementById("shipto").checked){
		
		document.getElementById("ship_fname").value = "";
		document.getElementById("ship_lname").value = "";
	}
	
}

function shipping_contact(){
	
	if( document.getElementById("shipping_cont").checked){
		
		cnum = "<?php foreach($name as $r); echo $r->cnum;?>";
		email = "<?php foreach($name as $r); echo $r->email;?>";
		
		document.getElementById("ship_cnum").value = cnum;
		document.getElementById("ship_email").value = email;
		
		
	}else if(!document.getElementById("shipping_cont").checked){
		
		document.getElementById("ship_cnum").value = "";
		document.getElementById("ship_email").value = "";
	}
	
}	

function billing_address(){
	
	if( document.getElementById("billing_add").checked){
		
		street = "<?php foreach($name as $r); echo $r->address;?>";
		city = "<?php foreach($name as $r); echo $r->city;?>";
		brgy = "<?php foreach($name as $r); echo $r->brgy;?>";
		
		document.getElementById("bill_street").value = street;
		document.getElementById("bill_city").value = city;
		document.getElementById("bill_brgy").value = brgy;
		
		
	}else if(!document.getElementById("billing_add").checked){
		
		document.getElementById("bill_street").value = "";
		document.getElementById("bill_city").value = "";
		document.getElementById("bill_brgy").value = "";
	}
	
}

function billing_name(){
	
	if( document.getElementById("billto").checked){
		
		fname = "<?php foreach($name as $r); echo $r->fname;?>";
		lname = "<?php foreach($name as $r); echo $r->lname;?>";
		
		document.getElementById("bill_fname").value = fname;
		document.getElementById("bill_lname").value = lname;
		
		
	}else if(!document.getElementById("billto").checked){
		
		document.getElementById("bill_fname").value = "";
		document.getElementById("bill_lname").value = "";
	}
	
}

function billing_contact(){
	
	if( document.getElementById("billing_cont").checked){
		
		cnum = "<?php foreach($name as $r); echo $r->cnum;?>";
		email = "<?php foreach($name as $r); echo $r->email;?>";
		
		document.getElementById("bill_cnum").value = cnum;
		document.getElementById("bill_email").value = email;
		
		
	}else if(!document.getElementById("billing_cont").checked){
		
		document.getElementById("bill_cnum").value = "";
		document.getElementById("bill_email").value = "";
	}
	
}

</script>
