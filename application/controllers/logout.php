<?php

 class Logout extends CI_Controller{
	
	public function __construct()
		{
        
        parent::__construct();
		$this->load->library('session'); 
		$this->load->model('logout_m');
		$id = $this->session->userdata('client_id');
		$this->logout_m->logout($id);
		$this->session->sess_destroy();
		redirect('home');
		}
	
	
	}
