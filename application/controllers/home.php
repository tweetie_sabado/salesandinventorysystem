<?php

class Home extends CI_Controller{
	
	
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
        $this->load->model('home_model');
        $this->load->library('cart');
        $this->load->library('form_validation');
            
	}

	function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true){
            echo 'Sorry, you don\'t have permission to access this page.';
            echo anchor('login', 'Login');
            die();  
        }
        else{
            return true;
        }   
    }
	
	function feedback(){
		
		$this->home_model->feedback();
		$this->session->set_flashdata('feedback','Thank you for sending us your feedback.');
		redirect('home/info');
		
		
		
	}
	
	function info(){
		
		$data['q1'] = $this->home_model->getCatalogs();
        $data['q2'] = $this->home_model->getLatest();
		$data['main_content'] = 'aboutus';
		$this->load->view('includes/template',$data);
		
		
	}
	
    function index()
	{
        $data['q1'] = $this->home_model->getCatalogs();
        $data['q2'] = $this->home_model->getLatest();
		$data['q3'] = $this->home_model->getTop();
		$data['main_content'] = 'home';
		$this->load->view('includes/template',$data);
	}

      

    function signup(){

        $data['q1'] = $this->home_model->getCatalogs();
        $data['q2'] = $this->home_model->getLatest();
        $data['main_content'] = 'signup';
        $this->load->view('includes/template',$data);
    }
	

    function validate_account()
    {
    	$this->load->library('form_validation');	
        $this->form_validation->set_rules('myusername', 'Username', 'trim|numeric|required|callback_check_account|xss_clean');
        $this->form_validation->set_rules('mypassword', 'Password','required');
            if($this->form_validation->run() == FALSE){
                $this->login_error();								
            }
            else{
                $data = array(
                    'email' => $this->input->post('myusername'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('account');  	
             	
            }
    }

    function check_account()
    {
    	 $this->load->model('home_model');
            $query = $this->home_model->check_account_db();
            if($query){
        		return true;
            }
            else{
        		return false;
            }
    }

    function logout()
    {
        $this->session->sess_destroy();
        $this->index();
    }

    function add_clients()
    {
    	$bdate = ($this->input->post('bdate'));
		$date = date('Y-m-d');
		$age = abs(strtotime($date) - strtotime($bdate));
		$years = floor($age / (365*60*60*24));
		
    	$activation_code = $this->_random_string(10);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[client.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('ans', 'Secret Question Answer', 'required');
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('bdate', 'Birthdate', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('cnum', 'Contact Number', 'required|numeric');
        if($this->form_validation->run() == FALSE)
        {
           $this->signup();                 
        }
        else{
        	
			if($years < 18){
			
			$this->session->set_flashdata('age','Age Restriction.');
			redirect('home/signup'); 
			
			}else{
        
            $this->home_model->add_clients($activation_code);
            $name = $this->input->post('fname').' '.$this->input->post('lname');
			$this->load->library('email');
			$this->email->from('pcfast.ph@gmail.com','PC FAST');
			$this->email->to($this->input->post('email'));
			$this->email->subject('Registration Confirmation');
			$this->email->message('<h1>Good day '.$name.' !</h1></br>Please click the link to confirm your registration </br></br>' . anchor('http://localhost/pcfast/home/register_confirm/' . $activation_code,'Confirm Registration'));
			
			$this->email->send();
            $this->session->set_flashdata('message', 'Sign up complete! please check your email address for confirmation.');
            redirect('home'); 
			}
        }
		
		
		
    	

    }
	
	
	function register_confirm(){
		
		$registration_code = $this->uri->segment(3);
		
		if($registration_code == ''){
			echo 'Error no registration code in URL';
			exit();
		}
		$registration_confirmed = $this->home_model->confirm_registration($registration_code);
		
		if($registration_confirmed){
			
			$this->session->set_flashdata('message', 'Email confirmed! You may now login your account.');
            redirect('home'); 
			
		}else{
			
			$this->session->set_flashdata('warning', 'Please confirm your email address.');
            redirect('home'); 
			
		}
		
	}

    function product_look_up(){

        $product_id = $this->uri->segment(2);
        
		$data['review']= $this->home_model->getProductReview($product_id);
        $data['products'] = $this->home_model->getProduct($product_id);
        $data['q1'] = $this->home_model->getCatalogs();
        $data['main_content'] = 'product';
        $this->load->view('includes/template',$data);

    }

    function category_look_up(){

         $category_id = $this->uri->segment(2);
        

        $data['category'] = $this->home_model->getCategory($category_id);
        $data['cn'] = $this->home_model->getCatName($category_id);
        $data['q1'] = $this->home_model->getCatalogs();
        $data['main_content'] = 'category';
        $this->load->view('includes/template',$data);


    }
	
	function _random_string($length){
		
		$len = $length;
		$base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
		$max = strlen($base)-1;
		$activatecode='';
		mt_srand((double)microtime()*1000000);
		while (strlen($activatecode)< $len+1)
			$activatecode.=$base{mt_rand(0,$max)};
		return $activatecode;
		
	}


}

