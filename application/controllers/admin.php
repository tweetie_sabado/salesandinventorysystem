<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();	
		$this->load->model('admin_m');
		$this->lang->load('order');
		$this->load->library('cart');
		 //$this->load->library('HighChart_lib.php');

	}

	public function index()
	{
		$this->is_logged_in();
		$data['recent_orders'] = $this->admin_m->get_recent_orders();
		$data['recent_customers'] = $this->admin_m->get_recent_customers();
		$data['order_count'] = $this->admin_m->get_order_count();
		$data['pending'] = $this->admin_m->get_order_pending();
		$data['processing'] = $this->admin_m->get_order_processing();
		$data['delivered'] = $this->admin_m->get_order_delivered();
		$data['cancelled'] = $this->admin_m->get_order_cancelled();
		$data['on_hold'] = $this->admin_m->get_order_on_hold();
		$data['archived_orders'] = $this->admin_m->get_archived_orders_count();
		$data['active_clients'] =	$this->admin_m->get_active_clients();
		$data['inactive_clients'] =	$this->admin_m->get_inactive_clients();
		$data['main_content'] = 'dashboard';
		$this->load->view('includes/admin_template',$data);
	}

	function is_logged_in()
	{
		$baseurl = base_url();
		$is_logged_in = $this->session->userdata('is_logged_in');
		
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			//echo 'You don\'t have permission to access this page. <a href="'.$baseurl.'admin_login">Login</a>';
			//die();
			redirect('admin_login');	
		}
		else
		{
			return true;
		}	
	}
	
	function upload_images()
	{
		$config['upload_path'] = "./uploads/";
		$config['allowed_types'] = "jpg|jpeg|gif|png";
		$this->upload->initialize($config);
		$image = $this->input->post('image');
		if(!$this->upload->do_upload($image)){
			$error = array('error' => $this->upload->display_errors());
			$this->add_product_details($error);
		}else
		{
			// $image_data = $this->upload->data();
			// $data['image_url'] = base_url().'/uploads/'.$image_data['file_name'];
			$data = $this->upload->data();
			$image_url = base_url().'/uploads/'.$data['file_name'];
			$filename = $data['file_name'];
            $file_id = $this->admin_m->insert_product_details($image_url, $filename);
            if($file_id)
            {
            	//$this->admin_m->insert_product_details($image_url, $filename);
            	$this->session->set_flashdata('success', 'Product have been succesfully added!');
				redirect('admin/products');         	
            }
            else
            {
                $this->session->set_flashdata('error', 'Something went wrong when saving the image, please try again.');
				redirect('admin/add_product_details');    
            }
		}
	}

	function active_clients()
	{
		$data['customers'] = $this->admin_m->get_active_customers();
		$data['main_content'] = 'active_clients';
		$this->load->view('includes/admin_template', $data);
	}
	
	
	function inactive_clients()
	{
		$data['customers'] = $this->admin_m->get_inactive_customers();
		$data['main_content'] = 'inactive_clients';
		$this->load->view('includes/admin_template', $data);
	}
	
	function pending_orders()
	{
		$data['orders'] = $this->admin_m->get_pending_orders();
		$data['main_content'] = 'pending_orders';
		$this->load->view('includes/admin_template',$data);
	}
	
	function processing_orders()
	{
		$data['orders'] = $this->admin_m->get_processing_orders();
		$data['main_content'] = 'processing_orders';
		$this->load->view('includes/admin_template',$data);
	}
	
	function onhold_orders()
	{
		$data['orders'] = $this->admin_m->get_onhold_orders();
		$data['main_content'] = 'onhold_orders';
		$this->load->view('includes/admin_template',$data);
	}
	
	function del_orders()
	{
		$data['orders'] = $this->admin_m->get_delivered_orders();
		$data['main_content'] = 'delivered_orders';
		$this->load->view('includes/admin_template',$data);
	}
	
	function can_orders()
	{
		$data['orders'] = $this->admin_m->get_cancelled_orders();
		$data['main_content'] = 'cancelled_orders';
		$this->load->view('includes/admin_template',$data);
	}
	
	function view_product()
	{
		$data['main_content'] = 'view_product';
		$this->load->view('includes/admin_template', $data);
	}
	
	function add_prod_details()
	{
		$file_element_name = 'userfile';
		$prod_name = $this->input->post('prodName');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('prodName', 'Product Name', 'required|is_unique[products.product_name]');
		$this->form_validation->set_rules('prodDesc', 'Product Description', 'required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'trim|numeric|greater_than[0]|required');
        $this->form_validation->set_rules('regPrice', 'Price', 'trim|required|numeric|greater_than[0]');
        if($this->form_validation->run() == FALSE)
        {
           $this->add_product_details();                     
        }
        else{
        	$config['upload_path'] = "./uploads/";
			$config['allowed_types'] = "jpg|jpeg|gif|png";
			$this->upload->initialize($config);
			//$image = $this->input->post('image');
			if(!$this->upload->do_upload($file_element_name)){
				$error = array('error' => $this->upload->display_errors());
				$this->add_product_details($error);
			}else
			{
				// $image_data = $this->upload->data();
				// $data['image_url'] = base_url().'/uploads/'.$image_data['file_name'];
				$data = $this->upload->data();
				$image_url = base_url().'uploads/'.$data['file_name'];
				$filename = $data['file_name'];
	            if($this->admin_m->insert_product_details($image_url, $filename))
	            {
	            	$this->session->set_flashdata('success', 'Product '.$prod_name.' have been succesfully added!');
					redirect('admin/products');           	
	            }
	            else
	            {
	                $this->session->set_flashdata('error', 'Something went wrong when saving the image, please try again.');
					redirect('admin/add_product_details');    
	            }
			}
        }
	}
	
	
	function edit_prod()
	{
		$file_element_name = 'userfile';
		$prod_name = $this->input->post('prodName');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('prodName', 'Product Name', 'required');
		$this->form_validation->set_rules('prodDesc', 'Product Description', 'required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'trim|numeric|greater_than[0]|required');
        $this->form_validation->set_rules('regPrice', 'Price', 'trim|required|numeric|greater_than[0]');
        if($this->form_validation->run() == FALSE)
        {
           $this->edit_product();                     
        }
        else{
        	$config['upload_path'] = "./uploads/";
			$config['allowed_types'] = "jpg|jpeg|gif|png";
			$this->upload->initialize($config);
			//$image = $this->input->post('image');
			if(!$this->upload->do_upload($file_element_name)){
				$error = array('error' => $this->upload->display_errors());
				$this->edit_product($error);
			}else
			{
				// $image_data = $this->upload->data();
				// $data['image_url'] = base_url().'/uploads/'.$image_data['file_name'];
				$data = $this->upload->data();
				$image_url = base_url().'uploads/'.$data['file_name'];
				$filename = $data['file_name'];
				$id = $this->uri->segment(3);
				
	            if($this->admin_m->update_prod($id, $image_url, $filename))
	            {
	            	$this->session->set_flashdata('success', 'Product '.$prod_name.' have been succesfully updated!');
					redirect('admin/products');           	
	            }
	            else
	            {
	                $this->session->set_flashdata('error', 'Something went wrong when saving the image, please try again.');
					redirect('admin/edit_product/'.$id);    
	            }
			}
        }
	}

	function edit_product()
	{
		$product_id = $this->uri->segment(3);
		$data['products'] = $this->admin_m->get_specific_product($product_id);
		$data['categories'] = $this->admin_m->get_categories();
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['main_content'] = 'edit_product';
		$this->load->view('includes/admin_template', $data);
	}
	
	function statistics()
	{
		// Load the library
		$this->load->library('googlemaps');
		
		$config['center'] = '14.5653015, 121.0246344';
		$config['zoom'] = 'auto';
		// Initialize our map. Here you can also pass in additional parameters for customising the map (see below)
		
		$client_address = $this->admin_m->get_clients_address();
		foreach($client_address as $row)
		{
			$address = $row->address;
			$fname = $row->fname;
			$lname = $row->lname;
			$full_name = $fname.' '.$lname;
			$coords = $this->getCoordinates($address);
    		$lat = $coords[0];
			$lng = $coords[1];
			$pos = $lat.', '.$lng;
			//echo $pos;
			$this->googlemaps->initialize($config);
			$marker = array();
			$marker['position'] = $pos;
			$marker['animation'] = 'DROP';
			$marker['infowindow_content'] = $full_name.' - '.$address;
			$this->googlemaps->add_marker($marker);
		}
		// $this->googlemaps->initialize($config);
		// $marker = array();
		// $marker['position'] = '14.449693, 120.982609';
		// $marker['infowindow_content'] = '1 - Hello World!';
		// $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
		// $this->googlemaps->add_marker($marker);
// 		
		// $marker = array();
		// $marker['position'] = '14.553892, 121.028097';
		// $marker['draggable'] = TRUE;
		// $marker['animation'] = 'DROP';
		// $this->googlemaps->add_marker($marker);
// 		
		// $marker = array();
		// $marker['position'] = '14.578862, 121.036879';
		// $marker['onclick'] = 'alert("You just clicked me!!")';
		// $this->googlemaps->add_marker($marker);
		
		// Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
		// placed where we want the map to appear.
		$data['map'] = $this->googlemaps->create_map();
		// Load our view, passing the map data that has just been created
		//$data['year_graph'] = $this->admin_m->get_data();
		//$data['charts'] = $this->getChart($studentName);
      
		$data['main_content'] = 'statistics';
		
        $this->load->view('includes/admin_map_template', $data);
	}	

	function change_pass()
	{
		$this->validate_account();
	}
	
	function validate_account()
	{ 
        $this->form_validation->set_rules('newPass', 'Password','required|matches[confPass]|callback_check_profile');
        $this->form_validation->set_rules('confPass', 'Password','required|callback_check_account');
            if($this->form_validation->run() == FALSE){
               $this->profile();                     
            }
            else{
            	$admin_id = $this->session->userdata('clientID');
                $this->admin_m->update_pass($admin_id);
				$this->session->set_flashdata('success', 'Password successfully changed!');
				redirect('admin/profile');         
            }
    }

    function check_profile()
    {
            $query = $this->admin_m->check_profile_db();
            if($query){
                return true;
            }
            else{
                return false;
            }
    }
 	// private function getChart($stuName) {
// 
        // $this->highcharts->set_title('Name of Student :' . $stuName);
        // $this->highcharts->set_dimensions(740, 300); 
        // $this->highcharts->set_axis_titles('Date', 'Age');
        // $credits->href = base_url();
        // $credits->text = "Code 2 Learn : HighCharts";
        // $this->highcharts->set_credits($credits);
        // $this->highcharts->render_to("content_top");
// 
        // $result = $this->admin_m->getStudentDetails($stuName);
// 
            // if ($myrow = mysql_fetch_array($result)) {
                // do {
                    // $value[] = intval($myrow["age"]);
                    // $date[] = ($myrow["date"]);
                // } while ($myrow = mysql_fetch_array($result));
            // }
// 
            // $this->highcharts->push_xcategorie($date);
// 
            // $serie['data'] = $value;
            // $this->highcharts->export_file("Code 2 Learn Chart".date('d M Y')); 
            // $this->highcharts->set_serie($serie, "Age");
// 
            // return $this->highcharts->render();
// 
    // }
	
	function getCoordinates($address)
	{
    $address = urlencode($address);
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=" . $address;
    $response = file_get_contents($url);
    $json = json_decode($response,true);
 
    $lat = $json['results'][0]['geometry']['location']['lat'];
    $lng = $json['results'][0]['geometry']['location']['lng'];
 
    return array($lat, $lng);
	} 
 	
	
	function add_product()
	{
		$data['categories'] = $this->admin_m->get_categories();
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['main_content'] = 'add_product';
        $data['error'] = array('error' => '');
        $this->load->view('includes/upload_template', $data); 
	}
	
	function add_product_details()
	{
		$data['categories'] = $this->admin_m->get_categories();
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['main_content'] = 'add_product_details';
        $data['error'] = array('error' => '');
        $this->load->view('includes/upload_template', $data);
	}
	
	
	
	function add_purchase()
	{
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['categories'] = $this->admin_m->get_categories();
		$data['main_content'] = 'add_purchase';
		$this->load->view('includes/admin_template', $data);
	}

	function add_pur()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_rules('prodName', 'Product Name', 'required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'trim|numeric|greater_than[0]|required');
        $this->form_validation->set_rules('regPrice', 'Price', 'trim|required|numeric|greater_than[0]');
        if($this->form_validation->run() == FALSE)
        {
           $this->add_purchase();                     
        }
        else{
        	//echo $this->input->post('bday');
        	//$this->load->view('test', $admin_info);
            $this->admin_m->insert_purchase();
            //$this->admin_m->insert_purchase_to_products();
            $this->session->set_flashdata('success', 'New purchase made!');
			redirect('admin/purchases'); 
        }
	}
	

	function files()
	{
	    $files = $this->admin_m->get_files();
	    $this->load->view('files', array('files' => $files));
	}
	
	//DASHBOARD
	function dashboard()
	{
		$data['recent_orders'] = $this->admin_m->get_recent_orders();
		$data['recent_customers'] = $this->admin_m->get_recent_customers();
		$data['order_count'] = $this->admin_m->get_order_count();
		$data['pending'] = $this->admin_m->get_order_pending();
		$data['processing'] = $this->admin_m->get_order_processing();
		$data['delivered'] = $this->admin_m->get_order_delivered();
		$data['cancelled'] = $this->admin_m->get_order_cancelled();
		$data['on_hold'] = $this->admin_m->get_order_on_hold();
		$data['archived_orders'] = $this->admin_m->get_archived_orders_count();
		$data['active_clients'] =	$this->admin_m->get_active_clients();
		$data['inactive_clients'] =	$this->admin_m->get_inactive_clients();
		$data['main_content'] = 'dashboard';
		$this->load->view('includes/admin_template',$data);
	}

	//END DASHBOARD

	//SALES
	function sales()
	{
		$data['sales'] = $this->admin_m->get_sales();
		$data['main_content'] = 'sales';
		$this->load->view('includes/admin_template',$data);
	}
	
	function sales_archives()
	{
		$data['archives'] = $this->admin_m->get_sales_archives();
		$data['main_content'] = 'sales_archives';
		$this->load->view('includes/admin_template', $data);
	}
	//END OF SALES
	
	function products_archives()
	{
		$data['archives'] = $this->admin_m->get_products_archives();
		$data['main_content'] = 'products_archives';
		$this->load->view('includes/admin_template', $data);
	}
	
	//ORDERS
	function orders()
	{
		$data['orders'] = $this->admin_m->get_orders();
		$data['main_content'] = 'orders';
		$this->load->view('includes/admin_template',$data);
	}

	function bulk_delete_order()
	{
		$orders	= $this->input->post('order');
    	
		if($orders)
		{
			foreach($orders as $order)
	   		{
	   			$this->admin_m->delete_order($order);
	   		}
			$this->session->set_flashdata('success', 'Selected order/s successfully moved to archives.');
			redirect('admin/orders');
		}
	}

	//END ORDERS


	//CUSTOMERS
	function customers()
	{
		$data['customers'] = $this->admin_m->get_customers();
		$data['main_content'] = 'customers';
		$this->load->view('includes/admin_template', $data);
	}

	function bulk_delete_customers()
	{
		$customers	= $this->input->post('customer');
    	
		if($customers)
		{
			foreach($customers as $customer)
	   		{
	   			$this->admin_m->delete_cust($customer);
	   		}
			$this->session->set_flashdata('message', 'Selected customer/s successfully deactivated.');
			redirect('admin/customers');	
		}
	}

	function activate(){
		
		$id = $this->uri->segment(3);
		$stat = $this->uri->segment(4);
		
		if($stat == 'inactive'){
			
			$stat = 'active';
			$this->admin_m->activate($stat,$id);
			redirect('admin/customers');
			$this->session->set_flashdata('message', 'Selected customer/s successfully activated.');
		}
		elseif($stat == 'active'){
			
			$stat = 'inactive';
			$this->admin_m->activate($stat,$id);
			echo $stat;
			redirect('admin/customers');
			$this->session->set_flashdata('message', 'Selected customer/s successfully deactivated.');
		}
		
		
		
	}
	//END CUSTOMERS

	//CATEGORIES

	//DISPLAY CATEGORIES
	function categories()
	{
		$data['categories'] = $this->admin_m->get_categories();
		$data['main_content'] = 'categories';
		$this->load->view('includes/admin_template', $data);
	}

	//DISPLAY ADD CATEGORY FORM
	function add_category()
	{
		$data['main_content'] = 'add_category';
		$this->load->view('includes/admin_template', $data);
	}

	//INSERT CATEGORIES
	function add_cat()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('catName', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('catDesc', 'Description', 'trim');
        if($this->form_validation->run() == FALSE)
        {
           $this->add_category();                     
        }
        else{
        	$name = $this->input->post('catName');
        	$desc = $this->input->post('catDesc');
        	$stat = $this->input->post('status');
            $this->admin_m->insert_cat($name, $desc, $stat);
            $this->session->set_flashdata('success', 'New category inserted!');
			redirect('admin/categories'); 
        }
	}

	//EDIT CATEGORIES
	function edit_category()
	{
		$id = $this->uri->segment(3);
		$data['categories'] = $this->admin_m->get_specific_category($id);
		$data['main_content'] = 'edit_category';
		$this->load->view('includes/admin_template', $data);
	}
	
	function edit_supplier()
	{
		$id = $this->uri->segment(3);
		$data['suppliers'] = $this->admin_m->get_specific_supplier($id);
		$data['main_content'] = 'edit_supplier';
		$this->load->view('includes/admin_template', $data);
	}

	function edit_sup()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('compName', 'Company Name', 'required');
        $this->form_validation->set_rules('supFirstName', 'First Name', 'trim|required');
		$this->form_validation->set_rules('supLastName', 'Last Name', 'trim|required');
		 $this->form_validation->set_rules('cnum', 'Contact Number', 'trim|numeric|required');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email|required');
        $this->form_validation->set_rules('add', 'Address', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
           $this->edit_supplier();                     
        }
        else{
        	
        	$sup_id = $this->uri->segment(3);
            $this->admin_m->update_sup($sup_id);
            $this->session->set_flashdata('success', 'Supplier #'.$sup_id.' successfully edited!');
			redirect('admin/suppliers'); 
        }
	}
	
	
	
	function edit_cat()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('catName', 'Category Name', 'required');
        $this->form_validation->set_rules('catDesc', 'Description', 'trim');
        if($this->form_validation->run() == FALSE)
        {
           $this->edit_category();                     
        }
        else{
        	$name = $this->input->post('catName');
        	$desc = $this->input->post('catDesc');
        	$stat = $this->input->post('status');
        	$id = $this->uri->segment(3);
            $this->admin_m->update_cat($name, $desc, $stat, $id);
            $this->session->set_flashdata('message', 'Category ID #'.$id.' edited!');
			redirect('admin/categories'); 
        }
	}

	function bulk_delete_cat()
	{
		$categories	= $this->input->post('category');
    	
		if($categories)
		{
			foreach($categories as $category)
	   		{
	   			$this->admin_m->delete_cat($category);
	   		}
			$this->session->set_flashdata('message', 'Category/ies successfully deleted.');
			redirect('admin/categories');
		}
	}
	//END CATEGORIES

	//PRODUCTS
	function products()
	{
		$data['products'] = $this->admin_m->get_products();
		$data['main_content'] = 'products';
		$this->load->view('includes/admin_template', $data);
	}

	//END PRODUCTS
	function logout()
	{
		$this->session->sess_destroy();
		redirect('admin_login');
	}

	//ADD PRODUCT
	
	

	
	function check_discount()
	{
		$regPrice = $this->input->post('regPrice');
		$salePrice = $this->input->post('salePrice');
		if($regPrice < $salePrice){
			$this->form_validation->set_message('check_discount', 'The %s field can not be greater than the REGULAR PRICE');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	
	function bulk_delete_prod()
	{
		$products = $this->input->post('product');
    	
		if($products)
		{
			foreach($products as $product)
	   		{
	   			$this->admin_m->delete_product($product);
	   		}
			$this->session->set_flashdata('message', 'Selected product/s successfully deactivated.');
			redirect('admin/products');	
		}
	}


	//ARCHIVES
	function archives()
	{
		$data['archives'] = $this->admin_m->get_order_archives();
		$data['main_content'] = 'archives';
		$this->load->view('includes/admin_template', $data);
	}


	function move_orders()
	{
		$archives= $this->input->post('archive');
    	
		if($archives)
		{
			foreach($archives as $archive)
	   		{
	   			$this->admin_m->move_order($archive);
	   		}
			$this->session->set_flashdata('message', 'Archived order/s successfully moved back to Orders');
			redirect('admin/archives');
		}
	}

	//END ARCHIVES

	//SUPPLIERS
	function suppliers()
	{
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['main_content'] = 'suppliers';
		$this->load->view('includes/admin_template', $data);
	}


	function bulk_delete_suppliers()
	{
		$suppliers	= $this->input->post('supplier');
    	
		if($suppliers)
		{
			foreach($suppliers as $supplier)
	   		{
	   			$this->admin_m->delete_supplier($supplier);
	   		}
			$this->session->set_flashdata('message', 'Selected supplier/s successfully deactivated.');
			redirect('admin/suppliers');	
		}
	}

	function add_supplier()
	{
		$data['main_content'] = 'add_supplier';
		$this->load->view('includes/admin_template', $data);
	}

	function add_sup()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('comp', 'Company', 'trim|required|is_unique[suppliers.company_name]');
        $this->form_validation->set_rules('supFirstName', 'First Name', 'trim|alpha|required');
        $this->form_validation->set_rules('supFirstName', 'Last Name', 'trim|alpha|required');
        $this->form_validation->set_rules('cnum', 'Contact Number', 'trim|numeric|required');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email|required');
        $this->form_validation->set_rules('add', 'Address', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
           $this->add_supplier();                     
        }
        else{
        	$sup_info =  array(
        		'company_name' 	=> $this->input->post('comp'),
        		'sup_fname' 	=> $this->input->post('supFirstName'),
        		'sup_lname' 	=> $this->input->post('supLastName'),
        		'cnum' 			=> $this->input->post('cnum'),
        		'address' 		=> $this->input->post('add'),
        		'email'			=> $this->input->post('email'),
				'status'		=> 'active'
        		); 
        	
            $this->admin_m->insert_sup($sup_info);
            $this->session->set_flashdata('success', 'New supplier inserted!');
			redirect('admin/suppliers'); 
        }
	}
	//END SUPPLIERS

	//ADMINISTRATORS
	function administrators()
	{
		$data['administrators'] = $this->admin_m->get_administrators();
		$data['main_content'] = 'administrators';
		$this->load->view('includes/admin_template', $data);
	}

	function add_admin()
	{
		$data['main_content'] = 'add_administrator';
		$this->load->view('includes/admin_template', $data);
	}

	function add_administrator()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_rules('adminFName', 'First Name', 'alpha');
        $this->form_validation->set_rules('adminLName', 'Last Name', 'alpha');
        $this->form_validation->set_rules('bday', 'Birthdate', 'required');
        $this->form_validation->set_rules('cnum', 'Contact Number', 'trim|numeric|greater_than[0]|exact_length[11]|required');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email|required');
        $this->form_validation->set_rules('add', 'Address', 'trim|required');
        $this->form_validation->set_rules('pass1', 'Password', 'trim|matches[pass2]|required');
        $this->form_validation->set_rules('pass2', 'Confirm Password', 'trim|matches[pass1]|required');
        if($this->form_validation->run() == FALSE)
        {
           $this->add_admin();                     
        }
        else{
        	
            $this->admin_m->insert_admin($admin_info);
            $this->session->set_flashdata('success', 'New admin created!');
			redirect('admin/administrators'); 
        }
	}

	function bulk_delete_administrators()
	{
		$admins	= $this->input->post('admin');
    	
		if($admins)
		{
			foreach($admins as $admin)
	   		{
	   			$this->admin_m->delete_administrator($admin);
	   		}
			$this->session->set_flashdata('message', 'Selected administrator/s successfully deactivated.');
			redirect('admin/administrators');	
		}
	}
	//END ADMINISTRATORS

	function purchases()
	{
		$data['purchases'] = $this->admin_m->get_purchases();
		$data['main_content'] = 'purchases';
		$this->load->view('includes/admin_template', $data);
	}

	function bulk_delete_purchases()
	{
		$purchases	= $this->input->post('purchase');
    	
		if($purchases)
		{
			foreach($purchases as $purchase)
	   		{
	   			$this->admin_m->delete_purchases($purchase);
	   		}
			$this->session->set_flashdata('message', 'Selected purchase/s successfully deactivated.');
			redirect('admin/purchases');	
		}
	}

	

	function edit_purchase()
	{
		$id = $this->uri->segment(3);
		$data['suppliers'] = $this->admin_m->get_suppliers();
		$data['categories'] = $this->admin_m->get_categories();
		$data['purchases'] = $this->admin_m->get_specific_purchase($id);
		$data['main_content'] = 'edit_purchase';
		$this->load->view('includes/admin_template', $data);
	}

	function edit_pur()
	{
        	$purchase_id = $this->uri->segment(3);
		    $this->admin_m->edit_purchase($purchase_id);
			
			if($this->input->post('purchase_status') == 'Delivered'){
				$this->admin_m->update_purchase();
			}
			$this->session->set_flashdata('success', 'Purchase #'.$purchase_id.' updated!');
			redirect('admin/purchases'); 
        

	}

	//CURRENT FNCTION
	function update_stat(){
			$cust_id 	= $this->uri->segment(3);
			$id 		= $this->input->post('order_id');
			$stat 		= $this->input->post('order_status');
			
			
			if($stat == 'Cancelled'){
				
				$this->admin_m->order_cancel($id,$cust_id);
				$this->session->set_flashdata('message', 'Order ID: '.$id.' status successfully updated! Status set to Status: '.$stat.'');
				redirect('admin/orders');
				
			}
			
			if(strcmp("Delivered",$stat) == 0)
			{
				$order_number = $this->uri->segment(4);

				$data['cart_contents'] = $this->admin_m->get_order_products($order_number,$cust_id);
				// echo '<pre>';
				// print_r($data['cart_contents']);

				foreach ($data['cart_contents'] as $row) {
					$prod_id = $row['product_id'];
					$quantity = $row['quantity'];
	    			$data['current_count'] = $this->admin_m->get_current_count($prod_id);
	    			$cur_count = $data['current_count'][0]['current_count'];
	    			$this->admin_m->update_quantity($prod_id,$quantity,$cur_count);
	    			if($cur_count <= 0 )
		    			{
						    $status = 'On Hold';
							$this->admin_m->update_stat_hold($id, $cust_id);
						    $this->session->set_flashdata('warning', 'Your product ID: '.$prod_id.' is out of stock! Status will be set to: On Hold');
		    				redirect('admin/orders');
		    			}
		    			else
		    			{
		    				$que = $this->admin_m->update_quantity($prod_id,$quantity,$cur_count);
		    				if($que == FALSE)
		    				{	
							   	$status = 'On Hold';
								$this->admin_m->update_stat_hold($id, $cust_id);
							    $this->session->set_flashdata('warning', 'Your orders QUANTITY exceeds the current quantity count in the inventory. Please update quantity first in PRODUCTS menu. Status will be set to: On Hold');
			    				redirect('admin/orders');
		    				}
							
		    			}
				}

			}
			$this->admin_m->update_stat($id,$cust_id);
			$this->session->set_flashdata('message', 'Order ID: '.$id.' status successfully updated! Status set to Status: '.$stat.'');
			redirect('admin/orders');
	}

	function edit_order()
	{
		$row['order_id'] = $this->input->post('order_id');
    	$row['status']	 = $this->input->post('status');
    	$order_number = $this->input->post('order_number');
    	$stat	= $this->input->post('status');
    	$this->admin_m->edit_order($row);
    	if(strcmp("Delivered",$stat) == 0){
    		$data['cart_contents'] = $this->admin_m->get_order_products($order_number);
    		foreach ($data['cart_contents'] as $row) {
			 	$prod_id = $row['product_id'];
				$quantity = $row['quantity'];
    			$data['current_count'] = $this->admin_m->get_current_count($prod_id);
    			$cur_count = $data['current_count'][0]['current_count'];
    			if($cur_count <= 0 )
    			{
				    $row2['order_id'] = $this->input->post('order_id');
    				$row2['status']	 = 'Cancelled';
				    $this->admin_m->edit_order($row2);
				    $this->session->set_flashdata('warning', 'Your product ID: '.$prod_id.' is out of stock! Delivered status in ORDERS form will be set to Cancelled.');
    				redirect('admin/products', 'location', 301);
    			}
    			else
    			{
    				$this->admin_m->update_quantity($prod_id,$quantity,$cur_count);
    			}
       		}

    		// foreach ($data['products'] as $row) {
    		// 	$prod_id = $row['product_id'];
    		// 	$quantity = $row['quantity'];
    		// 	$data['current_count'] = $this->admin_m->get_current_count($prod_id);
    		// 	$cur_count = $data['current_count'][0]['current_count'];
    		// 	$this->admin_m->update_quantity($prod_id,$quantity,$cur_count);
    		// }
    		//$this->admin_m->update_order_quantity($order_number);
    	}
    	
	}

	function view_stat()
	{		
			$order_id = $this->uri->segment(3);
			$id = $this->uri->segment(4);
			$cust_id 	= $this->uri->segment(4);
			$order_number =  $this->uri->segment(5);

		 	//$data['sum'] = $this->admin_m->get_order_total($id,$order_number);
			$data['orders'] = $this->admin_m->get_order_id($order_id);
        	//$data['order_total'] = $this->admin_m->get_order_total($order_number,$cust_id);
			$data['cart_contents'] = $this->admin_m->get_products_cart($order_number,$cust_id);
			$data['main_content'] = 'view_stat';
        	$this->load->view('includes/admin_template',$data);
	}

	function view_sales()
	{
		$order_id = $this->uri->segment(3);
		$cust_id = $this->uri->segment(4);
		$order_number = $this->uri->segment(5);
		$data['order_details'] = $this->admin_m->get_sales_details($order_id,$cust_id);
		$data['cart_contents'] = $this->admin_m->get_products_cart($order_number,$cust_id);
		$data['main_content'] = 'view_sales';
    	$this->load->view('includes/admin_template',$data);
	}
	
	function profile()
	{
		$data['admin_details'] = $this->admin_m->get_admin_details();
		$data['main_content'] = 'profile';
    	$this->load->view('includes/admin_template',$data);
	}

	function image_upload(){
		$config = array();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '300000';
		$this->load->library('upload');
		$this->upload->initialize($config);
		$this->upload->do_multi_upload("myfile");
		$files_upload = $this->upload->get_multi_upload_data();
		$err_msg = $this->upload->display_errors();
		
		if (!empty($files_upload) && is_array($files_upload)) {
		
		  foreach ($files_upload as $file_data) {
		
		    // Your files here :)
		    echo '<pre>';
        	print_r($this->upload->get_multi_upload_data());
			echo '</pre>';
		  }
		}else{
			echo $err_msg;
		}
	}
		
		// if($this->input->post('submit')){
			    // $path = './uploads';
			    // $this->upload->initialize(array(
			        // "upload_path"		=>	$path,
			        // "allowed_types"		=>	"gif|jpg|png|jpeg",
			        // "max_size"			=>  "256000"
			    // ));
			    // $files_upload = $this->upload->get_multi_upload_data();
				// $err_msg = $this->upload->display_errors();
// 			 	
// 				
				// if (!empty($files_upload) && is_array($files_upload)) {
// 				
				  // foreach ($files_upload as $file_data) {
// 				
				    // // Your files here :)
				    // echo '<pre>';
			        // print_r($this->upload->get_multi_upload_data());
			        // echo '</pre>';
				  // }
				// }
	    // }else{
		    // $this->add_product();
		// }
	
	
	// function do_upload()
	// {
	    // $files = $_FILES;
	    // $cpt = count($_FILES['userfile']['name']);
	    // for($i=0; $i<$cpt; $i++)
	    // {
	        // $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        // $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        // $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        // $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        // $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
		    // $this->upload->initialize($this->set_upload_options());
		    // $this->upload->do_upload();
	    // }
// 	
	// }
	// private function set_upload_options()
	// {   
	// //  upload an image options
	    // $config = array();
	    // $config['upload_path'] = './uploads/"';
	    // $config['allowed_types'] = 'gif|jpg|png|jpeg';
	    // $config['max_size']      = '5000';
	    // $config['overwrite']     = FALSE;
// 	
// 	
	    // return $config;
	// }
		
	// function uploadImage()
    // {
//  
       // $config['upload_path']   =   "./uploads/";
//  
       // $config['allowed_types'] =   "gif|jpg|jpeg|png"; 
//  
       // $config['max_size']      =   "5000";
//  
       // $config['max_width']     =   "1907";
//  
       // $config['max_height']    =   "1280";
//  
       // $this->upload->initialize($config);
//  
       // if(!$this->upload->do_upload())
//  
       // {
//  
           // echo $this->upload->display_errors();
//  
       // }
//  
       // else
//  
       // {
//  
           // $finfo=$this->upload->data();
//  
           // $this->_createThumbnail($finfo['file_name']);
//  
           // $data['uploadInfo'] = $finfo;
//  
           // $data['thumbnail_name'] = $finfo['raw_name']. '_thumb' .$finfo['file_ext']; 
// 		   
		   // $this->add_product($data);
//  
          // // $this->load->view('upload_success',$data);
//  
           // // You can view content of the $finfo with the code block below
//  
           // /*echo '<pre>';
//  
           // print_r($finfo);
//  
           // echo '</pre>';*/
//  
       // }
//  
    // }
// 	
	// //Create Thumbnail function
//  
    // function _createThumbnail($filename)
//  
    // {
//  
        // $config['image_library']    = "gd2";      
//  
        // $config['source_image']     = "./uploads/" .$filename;      
//  
        // $config['create_thumb']     = TRUE;      
//  
        // $config['maintain_ratio']   = TRUE;      
//  
        // $config['width'] = "80";      
//  
        // $config['height'] = "80";
//  
        // $this->load->library('image_lib',$config);
//  
        // if(!$this->image_lib->resize())
//  
        // {
//  
            // echo $this->image_lib->display_errors();
//  
        // }      
//  
    // }
 
 function order_search(){
		$data['orders'] = $this->admin_m->order_search();
		

		if(($this->input->post('search'))){
		$data['start'] = $this->input->post('sdate');
		$data['end'] = $this->input->post('edate');
		$data['main_content'] = 'orders_search';
		$this->load->view('includes/admin_template',$data);

		}

		if(($this->input->post('orders_p'))){
		$data['orders'] = $this->admin_m->get_orders();
		$data['main_content'] = 'p_orders';
		$this->load->view('includes/admin_template',$data);

		}

		if(($this->input->post('export'))){
		$data['start'] = $this->input->post('sdate');
		$data['end'] = $this->input->post('edate');
		$data['tot'] = $this->admin_m->tot_orders();
		$data['main_content'] = 'orders_print';
		$this->load->view('includes/admin_template',$data);

		}
	}

		function sales_report(){
		$data['sales'] = $this->admin_m->get_sales();

		if(($this->input->post('export'))){
		$data['tot'] = $this->admin_m->total_sales();
		$data['main_content'] = 'p_sales';
		$this->load->view('includes/admin_template',$data);

		}

		if(($this->input->post('search'))){
		$data['sales'] = $this->admin_m->print_sales();	
		$data['start'] = $this->input->post('sdate');
		$data['end'] = $this->input->post('edate');
		$data['tot'] = $this->admin_m->tot_sales();
		$data['main_content'] = 'sales_print';
		$this->load->view('includes/admin_template',$data);

		}

	}

	function purchases_report(){
		$data['purchases'] = $this->admin_m->get_purchases();

		if(($this->input->post('export'))){
		$data['tot'] = $this->admin_m->total_purchase();
		$data['main_content'] = 'p_purchases';
		$this->load->view('includes/admin_template',$data);

		}

		if(($this->input->post('search'))){
		$data['purchases'] = $this->admin_m->print_purchases();	
		$data['start'] = $this->input->post('sdate');
		$data['end'] = $this->input->post('edate');
		$data['tot'] = $this->admin_m->tot_purchase();
		$data['main_content'] = 'purchases_print';
		$this->load->view('includes/admin_template',$data);

		}

	}
	
	// Messages
	
	function add_message()
		{
			
			$this->admin_m->add_message();
			$this->session->set_flashdata('sent', 'Message Sent!');
			redirect('admin/view_messages');
		}
		
		
	function reply_message()
		{
			
			$this->admin_m->add_message();
			$this->session->set_flashdata('sent', 'Reply Sent!');
			redirect('admin/view_messages');
		}	
		
		 function view_messages()
        {
        	$email = 'admin@pcfast.com';          
			$data['em'] = $this->admin_m->get_emails();
			$data['inbox'] = $this->admin_m->get_inbox($email);
			$data['ct_inbox'] = $this->admin_m->count_inbox($email);
			$data['outbox'] = $this->admin_m->get_outbox($email);
			$data['ct_outbox'] = $this->admin_m->count_outbox($email);
			$data['trash'] = $this->admin_m->get_trash($email);
			$data['ct_trash'] = $this->admin_m->count_trash($email);
			$data['unread'] = $this->admin_m->count_unread($email);
            $data['main_content'] = 'admin_messages';
            $this->load->view('includes/admin_template',$data);
        }

		function inbox(){
			$email = 'admin@pcfast.com'; 
			$mid = $this->uri->segment(3);
			$data['message'] = $this->admin_m->get_mid($mid);
			$data['unread'] = $this->admin_m->count_unread($email);
            $data['main_content'] = 'admin_inbox';
            $this->load->view('includes/admin_template',$data);
    }

	function read(){
	
	$mid = $this->uri->segment(3);
	$this->admin_m->read($mid);
	redirect('admin/view_messages');

	}
	
	function trash(){
	
	$mid = $this->uri->segment(3);
	$this->admin_m->trash($mid);
	redirect('admin/view_messages');

	}

	function order_history(){
	
	$id = $this->uri->segment(3);
	$data['order'] = $this->admin_m->order_history($id);
	$data['main_content'] = 'order_history';
    $this->load->view('includes/admin_template',$data);	
		
		
		
	}


	function order_items(){
	
	$id = $this->uri->segment(3);
	$data['items'] = $this->admin_m->order_items($id);
	$data['main_content'] = 'order_items';
    $this->load->view('includes/admin_template',$data);	
		
		
		
	}
	
 
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */