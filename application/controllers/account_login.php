<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('account_login_m');
	}

	function index()
	{
		$data['main_content'] = 'account_login';
		$this->load->view('includes/template',$data);
	}

	function validate_account()
	{ 
        $this->form_validation->set_rules('inputEmail', 'Email Address', 'required|callback_check_account');
        $this->form_validation->set_rules('inputPassword', 'Password','required');
            if($this->form_validation->run() == FALSE){
               $this->index();                     
            }
            else{
                $data = array(
                    'emailAdd' => $this->input->post('inputEmail'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('account');            
            }
    }

    function check_account()
    {
            $query = $this->account_login_m->check_account_db();
            if($query){
                return true;
            }
            else{
                return false;
            }
    }

    function pass_rec(){
        $email = $this->session->userdata('email');
        $ans = $this->input->post('ans');
        $data['answer'] = $this->account_login_m->get_pass($email);
        
        if($ans ==  $data['answer']){
        	
			$this->new_pass();
			
        }
       
		else{
			
			$this->forgot_pass();
			
		}

    }

    function forgot_pass(){
    	$this->session->set_flashdata('warning', 'Your answer is incorrect,please try again!');
        $email = $this->input->post('email');
         $data = array(
                    'email' => $email,            
                );  
        $this->session->set_userdata($data);
        $data['quest'] = $this->account_login_m->get_quest($email);
        $data['main_content'] = 'pass_recovery';
        $this->load->view('includes/template',$data);

    }
	
	function new_pass(){
		
		$this->session->set_flashdata('warning', 'Your answer is incorrect,please try again!');
        
        $data['main_content'] = 'new_pass';
        $this->load->view('includes/template',$data);
		
		
	}

	function change_pass(){
		$email = $this->session->userdata('email');
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'Password Confirmation', 'required');
  
        if($this->form_validation->run() == FALSE)
        {
           $this->new_pass();                     
        }
        else{
        	$this->session->set_flashdata('password', 'You have successfuly changed your password!');
            $this->account_login_m->change_pass($email);
            redirect ('home');
        }
		
		
		
	}

}