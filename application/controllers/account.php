<?php

class Account extends CI_Controller{
	
	
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
        $this->load->model('account_model');
        $this->load->library('cart');
         //$this->load->library('../controllers/studprofile');
	}

	
    function index()
	{
		
		date_default_timezone_set('Asia/Manila');
		$email = $this->session->userdata('email');
		$bday = date('m/d', strtotime($this->session->userdata('bday')));
		$date = date('m/d');
        $rndm = $this->session->userdata('random');
        $id = $this->session->userdata('client_id');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name');
		$data['main_content'] = 'account_home';
        $data['latest'] = $this->account_model->getLatest();
		$data['top'] = $this->account_model->getTop();
        $data['q1'] = $this->account_model->getCatalogs();
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['order'] = $this->account_model->getOrder($id);
		if($bday == $date){
			 $this->session->set_flashdata('bday', 'Happy Birthday! PC FAST is giving you a special promo, click here and check your email.');
		}
		$this->load->view('includes/account_template',$data);
		//echo date_default_timezone_get();
	}

	function feedback(){
		
		$this->account_model->feedback();
		$this->session->set_flashdata('feedback','Thank you for sending us your feedback.');
		redirect('account/info');
		
	}
	
	
	function info(){
		
		$email = $this->session->userdata('email');
        $id = $this->session->userdata('client_id');
        $rndm = $this->session->userdata('random');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name');
        $product_id = $this->uri->segment(3);
        $data['count'] = $this->uri->segment(4);
		$data['review']= $this->account_model->getProductReview($product_id);
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['products'] = $this->account_model->getProduct($product_id);
        $data['q1'] = $this->account_model->getCatalogs();
        $data['quantity'] = $this->account_model->getQty($product_id);
		$data['main_content'] = 'info';
		$this->load->view('includes/template',$data);
		
		
	}

    function view_account(){
    	$pass = $this->session->userdata('pass');
		$email = $this->session->userdata('email');
        $id = $this->session->userdata('client_id');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name'); 
        $data['main_content'] = 'account';
        $data['q1'] = $this->account_model->getCatalogs();
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['order'] = $this->account_model->getOrder($id);
        $this->load->view('includes/account_template',$data);


    }

	function bday_promo(){
		$this->load->helper('string');
		$rand = random_string('alnum', 10);
		$id = $this->session->userdata('client_id');
		$bday = date('m/d', strtotime($this->session->userdata('bday')));
		$date = date('m/d');
		
		//echo '<p><h2>Happy birthday '.$h.' !</h2></p><p>Because today is your birthday, we are giving you a SPECIAL PROMO</p><p>You will get a free 4 GB SanDisk USB for every Php 10,000 and up worth of purchase.</p><p>Just present this code below in our store to avail this promo.</p><p><h1>'.$rand.'</h1><p>Valid until this day only!</p>';
		
		if($bday == $date){
			$this->load->library('email');
			$this->email->from('pcfast.ph@gmail.com','PC FAST');
			$this->email->to($this->session->userdata('email'));
			$this->email->subject('Happy Birthday!');
			$this->email->message('
									<body>
									<div><p><h2>Happy birthday our valued customer !</h2></p></div>
									<div><p>Because today is your birthday, we are giving you a SPECIAL PROMO</p></div>
									<div><p>You will get a free 4 GB SanDisk USB for every Php 10,000 and up worth of purchase.</p></div>
									<div><p>Just present this code below in our store to avail this promo.</p></div>
									<div><h2>Valid until this day only!</h2></div>
									<div style="background-color: lightgrey;width: 300px;padding: 25px;margin: 25px;"><p><h1 style="text-align:center;">'.$rand.'</h1></div>
									</body>');
			$this->email->send();
			
		}
		 $this->session->set_flashdata('code', 'Birthday promo code redeemed.');
		redirect('account');
	}

    function product_look_up(){
    	$email = $this->session->userdata('email');
        $id = $this->session->userdata('client_id');
        $rndm = $this->session->userdata('random');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name');
        $product_id = $this->uri->segment(3);
        $data['count'] = $this->uri->segment(4);
		$data['review']= $this->account_model->getProductReview($product_id);
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['products'] = $this->account_model->getProduct($product_id);
        $data['q1'] = $this->account_model->getCatalogs();
        $data['quantity'] = $this->account_model->getQty($product_id);
        $data['main_content'] = 'prod_logged_in';
        $this->load->view('includes/account_template',$data);

    }

    function category_look_up(){
    	$email = $this->session->userdata('email');
    	$this->session->set_flashdata('ex', 'Desired item quantity exceeded existing stock.');
        $id = $this->session->userdata('client_id');
        $rndm = $this->session->userdata('random');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name');
        $category_id = $this->uri->segment(3);
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['category'] = $this->account_model->getCategory($category_id);
        $data['cn'] = $this->account_model->getCatName($category_id);
        $data['q1'] = $this->account_model->getCatalogs();
        $data['main_content'] = 'cat_logged_in';
        $this->load->view('includes/account_template',$data);


    }

    function del_cart(){

        $id = $this->uri->segment(3);
        

        $data['id'] = $this->account_model->delete_row($id);

        redirect ('account');


    }

    function add_cart()
    {
    	
    	$this->session->set_flashdata('message', 'Item added to cart!');   
        $c = $this->uri->segment(4);
        $qty = $this->input->post('qty');
        $price = $this->input->post('reg_price');
        $total = $price * $qty; 
     

        if(($qty < $c) || ( $qty == $c)){
        $id = $this->session->userdata('client_id');
        $product_id = $this->uri->segment(3);
        $rndm = $this->session->userdata('random');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
        $h = $this->session->userdata('name');
        $this->account_model->addCart($id,$product_id,$rndm,$h,$total);
		$this->account_model->add_hits($product_id);    
        redirect ('account');

        }else{
		$email = $this->session->userdata('email');
        $id = $this->session->userdata('client_id');
        $rndm = $this->session->userdata('random');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
		$data['unread'] = $this->account_model->count_unread($email);
        $data['h'] = $this->session->userdata('name');
        $product_id = $this->uri->segment(3);
        $data['count'] = $this->uri->segment(4);
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
		$data['review']= $this->account_model->getProductReview($product_id);
        $data['products'] = $this->account_model->getProduct($product_id);
        $data['q1'] = $this->account_model->getCatalogs();
        $data['quantity'] = $this->account_model->getQty($product_id);
        $data['main_content'] = 'prod_logged_error';
        $this->load->view('includes/account_template',$data);

        } 
    }

     function edit_client()
    {

         $id = $this->session->userdata('client_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('cnum', 'Contact Number', 'required|numeric');
        if($this->form_validation->run() == FALSE)
        {
           $this->view_account();                     
        }
        else{
        
            $this->account_model->edit_client($id);
            
            $this->session->set_flashdata('message', 'User info updated!');
			
            redirect('account/view_account'); 
        }

    }

    function edit_password(){

        $id = $this->session->userdata('client_id');
        $this->load->library('form_validation');
		$this->form_validation->set_rules('oldpass', 'Old Password', 'required');
		$this->form_validation->set_rules('recpass', 'Password', 'required|matches[oldpass]');
        $this->form_validation->set_rules('password', 'New Password', 'required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'Password Confirmation', 'required');
        if($this->form_validation->run() == FALSE)
        {
           $this->view_account();                     
        }
        else{
        
            $this->account_model->edit_password($id);
            
            $this->session->set_flashdata('message', 'Password updated!');
			
			$this->load->library('email');
			$this->email->from('pcfast.ph@gmail.com','PC FAST');
			$this->email->to($this->session->userdata('email'));
			$this->email->subject('Password Changed');
			$this->email->message('<p>Your password has been changed.</p>');
			$this->email->send();
            redirect('account/view_account'); 
        }


    }

    function edit_secret(){
	
        $id = $this->session->userdata('client_id');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ans', 'Secret Answer', 'required');
        if($this->form_validation->run() == FALSE)
        {
           $this->view_account();                     
        }
        else{
        
            $this->account_model->edit_secret($id);
            
            $this->session->set_flashdata('message', 'Secret answer updated!');
			$this->load->library('email');
			$this->email->from('pcfast.ph@gmail.com','PC FAST');
			$this->email->to($this->session->userdata('email'));
			$this->email->subject('Secret Question Changed');
			$this->email->message('<p>Your secret question and answer have been changed.</p>');
			$this->email->send();
            redirect('account/view_account'); 
        }


    }
	
	function add_review(){
		  $id = $this->session->userdata('client_id');
		  $product_id = $this->uri->segment(3);
		
		  $this->account_model->add_review($id,$product_id);
		  $this->session->set_flashdata('review', 'Thank you for submitting a product review!');
		  redirect('account');
		
	}

     function search_product()
        {
			$email = $this->session->userdata('email');
            $id = $this->session->userdata('client_id');
            $rndm = $this->session->userdata('random');
            $fn = $this->session->userdata('fname');
            $ln = $this->session->userdata('lname');
			$data['unread'] = $this->account_model->count_unread($email);
            $data['h'] = $this->session->userdata('name');
            $category_id = $this->uri->segment(3);
            $data['name'] = $this->account_model->getName($id);
            $data['cart'] = $this->account_model->getCart($id);
            $data['num'] = $this->account_model->countCart($id);
            $data['category'] = $this->account_model->getCategory($category_id);
            $data['cn'] = $this->account_model->getCatName($category_id);
            $data['q1'] = $this->account_model->getCatalogs();
            $keyword    =   $this->input->post('keyword');
            $data['results']    =   $this->account_model->search($keyword);
            
            $data['main_content'] = 'search_prod';
            $this->load->view('includes/account_template',$data);
        }


        function view_messages()
        {

            $id = $this->session->userdata('client_id');
			$email = $this->session->userdata('email');
            $rndm = $this->session->userdata('random');
            $fn = $this->session->userdata('fname');
            $ln = $this->session->userdata('lname');
            $data['h'] = $this->session->userdata('name');
            $category_id = $this->uri->segment(3);
			$data['inbox'] = $this->account_model->get_inbox($email);
			$data['ct_inbox'] = $this->account_model->count_inbox($email);
			$data['outbox'] = $this->account_model->get_outbox($email);
			$data['ct_outbox'] = $this->account_model->count_outbox($email);
			$data['trash'] = $this->account_model->get_trash($email);
			$data['ct_trash'] = $this->account_model->count_trash($email);
            $data['name'] = $this->account_model->getName($id);
            $data['cart'] = $this->account_model->getCart($id);
            $data['num'] = $this->account_model->countCart($id);
            $data['category'] = $this->account_model->getCategory($category_id);
            $data['cn'] = $this->account_model->getCatName($category_id);
            $data['q1'] = $this->account_model->getCatalogs();
			$data['unread'] = $this->account_model->count_unread($email);
            $data['main_content'] = 'view_messages';
            $this->load->view('includes/account_template',$data);
        }


		function add_message()
		{
			$email = $this->session->userdata('email');
			$this->account_model->add_message($email);
			$this->session->set_flashdata('sent', 'Message Sent!');
			redirect('account/view_messages');
		}
		
		function reply_message()
		{
			
			$email = $this->session->userdata('email');
			$this->account_model->add_message($email);	
			$this->session->set_flashdata('sent', 'Reply Sent!');
			redirect('account/view_messages');
		}	

     function view_order(){
		$email = $this->session->userdata('email');
        $id = $this->session->userdata('client_id');
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
        $order_number = $this->uri->segment(3);
        $data['h'] = $this->session->userdata('name');
        $data['main_content'] = 'view_order';
        $data['q1'] = $this->account_model->getCatalogs();
		$data['unread'] = $this->account_model->count_unread($email);
        $data['name'] = $this->account_model->getName($id);
        $data['cart'] = $this->account_model->getCart($id);
        $data['num'] = $this->account_model->countCart($id);
        $data['order'] = $this->account_model->view_order($id,$order_number);
        $data['stat'] = $this->account_model->order_stat($id,$order_number);
        $this->load->view('includes/account_template',$data);

    }
	 
	function inbox(){
			$mid = $this->uri->segment(3);
        	$id = $this->session->userdata('client_id');
			$email = $this->session->userdata('email');
            $rndm = $this->session->userdata('random');
            $fn = $this->session->userdata('fname');
            $ln = $this->session->userdata('lname');
            $data['h'] = $this->session->userdata('name');
			$data['message'] = $this->account_model->get_mid($mid);
			$data['ct_inbox'] = $this->account_model->count_inbox($email);
			$data['outbox'] = $this->account_model->get_outbox($email);
			$data['ct_outbox'] = $this->account_model->count_outbox($email);
			$data['trash'] = $this->account_model->get_trash($email);
			$data['ct_trash'] = $this->account_model->count_trash($email);
            $data['name'] = $this->account_model->getName($id);
            $data['cart'] = $this->account_model->getCart($id);
            $data['num'] = $this->account_model->countCart($id);
            $data['q1'] = $this->account_model->getCatalogs();
			$data['unread'] = $this->account_model->count_unread($email);
            $data['main_content'] = 'inbox';
            $this->load->view('includes/account_template',$data);
    }

	function read(){
	
	$mid = $this->uri->segment(3);
	$this->account_model->read($mid);
	redirect('account/view_messages');

	}
	
	function trash(){
	
	$mid = $this->uri->segment(3);
	$this->account_model->trash($mid);
	redirect('account/view_messages');

	}
	
	function update_cart(){
			
		$this->account_model->update_cart($this->input->post('cart_id'));
		redirect('account');
		
	}
	
	function get_cart_id(){
		
		$data = $this->account_model->get_cart_id($this->input->post('cart_id'));
        echo json_encode($data);
		
		
	}
	
}

