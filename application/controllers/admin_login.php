<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_m');
	}

	function index()
	{
		$data['main_content'] = 'login';
		$this->load->view('includes/login_template',$data);
	}

	function validate_account()
	{ 
        $this->form_validation->set_rules('inputEmail', 'Email Address', 'required|callback_check_account');
        $this->form_validation->set_rules('inputPassword', 'Password','required');
            if($this->form_validation->run() == FALSE){
               $this->index();                     
            }
            else{
                $data = array(
                    'emailAdd' => $this->input->post('inputEmail'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('admin');            
            }
    }

    function check_account()
    {
            $query = $this->admin_m->check_account_db();
            if($query){
                return true;
            }
            else{
                return false;
            }
    }

}