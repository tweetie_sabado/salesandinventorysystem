<?php

class Order extends CI_Controller{
    
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
        $this->load->model('order_m');
		$this->load->library('cart');
         //$this->load->library('../controllers/studprofile');
    }

    
    function index()
    {
    	$email = $this->session->userdata('email');
    	$this->load->model('account_model');
    	$data['unread'] = $this->account_model->count_unread($email);
        $id = $this->session->userdata('client_id');
        // $total = $this->input->post('total');
        // $this->order_m->insert_total_per_product($total);
        $data['main_content'] = 'order';
		$fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
        $h = $this->session->userdata('name');
        $data['q1'] = $this->order_m->getCatalogs();
        $data['name'] = $this->order_m->getName($id);
        $data['cart'] = $this->order_m->getCart($id);
        $data['num'] = $this->order_m->countCart($id);
        $data['sum'] = $this->order_m->getSum($id);
        
        $this->load->view('includes/template',$data);
    }

    function submit_order()
    {
        $id = $this->session->userdata('client_id');
        $sum = $this->order_m->getSum($id);
        $rndm = $this->session->userdata('random');
        $s = $sum->total;
        $fn = $this->session->userdata('fname');
        $ln = $this->session->userdata('lname');
        $h = $this->session->userdata('name');
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('ship_email', 'Shipping Email', 'required');
		$this->form_validation->set_rules('ship_city', 'Shipping City', 'required');
        $this->form_validation->set_rules('ship_street', 'Shipping Street', 'required');
        $this->form_validation->set_rules('ship_cnum', 'Shipping Contact Number', 'required|numeric');
		$this->form_validation->set_rules('bill_email', 'Billing Email', 'required');
		$this->form_validation->set_rules('bill_city', 'Billing City', 'required');
        $this->form_validation->set_rules('bill_street', 'Billing Street', 'required');
        $this->form_validation->set_rules('bill_cnum', 'Billing Contact Number', 'required|numeric');
        if($this->form_validation->run() == FALSE)
        {
           $this->index();                     
        }
        else{
        
            $this->order_m->submitOrder($id,$s,$h,$rndm);
            redirect ('order/update_cart');
        }
		
        
        

    }

    function update_cart()

    {
        $id = $this->session->userdata('client_id');
        $rndm = $this->session->userdata('random');
        $this->order_m->update_cart($id,$rndm);
		$this->session->set_flashdata('message', 'Order submitted!');
        redirect ('account');
        
    }

    function del_cart()

    {
        $id = $this->session->userdata('client_id');
        $this->order_m->del_cart($id);
        redirect ('account');

    }

}

