<?php

Class Account_login_m Extends CI_Model
{
    function get_recent_orders(){
        $this->db->order_by("order_date", "desc"); 
        $q = $this->db->get('orders');
        $data = $q->result_array();
        return $data;   
    }

    function get_recent_customers(){
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('client', 'orders.customer_id = client.client_id');
        $this->db->order_by("order_date", "desc");
        $q = $this->db->get();
        $data = $q->result_array();
        return $data;
    }

    function get_orders(){
        $this->db->order_by("order_date", "desc"); 
        $q = $this->db->get('orders');
        $data = $q->result_array();
        return $data;
        //print_r($data);
    }

    function get_customers(){ 
        $q = $this->db->get('client');
        $data = $q->result_array();
        return $data;   
    }

    function check_account_db(){
        $num = rand();
        $year = date('Y');
		$this->db->where('status','active');
        $this->db->where('email', $this->input->post('inputEmail'));
        $this->db->where('password',$this->input->post('inputPassword'));
        $query = $this->db->get('client');
        if($query->num_rows == 1)
        {
                $row = $query->row();
                $data = array(
                'client_id' => $row->client_id,
                'email' => $row->email,
                'bday' => $row->bdate,
                'name' => $row->fname.''. $row->lname,
                'is_logged_in' => true,
                'random' => $year.'-'.$num,
               	'pass' => $row->password
                );
                $this->session->set_userdata($data);
				$this->session->set_flashdata('welcome', 'Welcome to PC FAST Online Store!');
                return true;
        }
        else{
                return false;   
        }      
    }

    function delete($id)
    {
        $this->db->where('order_id', $id);
        $this->db->delete('orders');
        
        //now delete the order items
        $this->db->where('order_id', $id);
        $this->db->delete('order_items');
    }

    function get_quest($email){

        $this->db->where('email',$email);
        $query = $this->db->get('client');

        return $query->result();


    }

	function change_pass($email){
		
		 $data = array(
            'password' => $this->input->post('password'),
            );

        $this->db->where('email',$email);
        $q = $this->db->update('client',$data);
		
	}

    function get_pass($email){
    	$this->db->select('ans');
        $this->db->where('email',$email);
        $query = $this->db->get('client');
        
		foreach($query->result() as $row ){
			
			$data = $row->ans;
		}
		return $data;



    }
}