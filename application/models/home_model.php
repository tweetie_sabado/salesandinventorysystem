<?php

class Home_model extends CI_Model{

	function check_account_db()
    {
        $this->db->where('email', $this->input->post('myusername'));
        $this->db->where('password',$this->input->post('mypassword'));
        
        $query = $this->db->get('client');
        if($query->num_rows == 1)
        {
                $row = $query->row();
                $data = array(
                'client_id' => $row->client_id,
                'email' => $row->email,
                'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
        }
        else{
                return false;   
        }      
    }   
	

	function add_clients($activation_code)
	
		{
			
		$add = 	$this->input->post('address').', '.$this->input->post('city');
          $data = array(
            'password' => $this->input->post('password'),
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'bdate' => $this->input->post('bdate'),
            'city' => $this->input->post('city'),
            'address' => $add,
            'brgy' => $this->input->post('brgy'),
            'cnum' => $this->input->post('cnum'),
            'email' => $this->input->post('email'),
            'question' => $this->input->post('quest'),
            'ans' => $this->input->post('ans'),
            'status' => 'inactive',
            'activation_code' => $activation_code
	       );

		$this->db->insert('client', $data);
		return;
	
		}
		
	function feedback(){
		
		$data = array(
		
		'sender_id' => $this->input->post('name'),
		'receiver_id' => 'admin@pcfast.com',
		'title' => 'Feedback',
		'body' => $this->input->post('message')
		);
		
		$q = $this->db->insert('messages',$data);
		return $q;
		
		
	}	
		
	function confirm_registration($registration_code){
		$q = $this->db->query('select client_id from client where activation_code = "'.$registration_code.'"');
		
		if($q->num_rows() == 1){
			
		$this->db->query('update client set status = "active" where activation_code = "'.$registration_code.'"');
		
		return true;	
		}else{
			
			return false;
			
		}
		
		
	}	
	
	

	function getCatalogs()
	
    	{
		$this->db->where('status','Enabled');
        $query = $this->db->get('category');


        return $query->result();

    	}

	function getProductReview($product_id){
		
		$this->db->where('product_id',$product_id);
		$this->db->join('client c','c.client_id = r.client_id','right');
        $query=$this->db->get('prod_review r'); 


        return $query->result();
		
	}

	function getLatest(){
		$this->db->where('current_count !=',0);
		$this->db->where('show','Enabled');
        $query=$this->db->get('products',3); 
        return $query->result();
		}
	
	function getTop(){
		$this->db->where('current_count !=',0);
		$this->db->where('show','Enabled');
		$this->db->join('products p', 'p.product_id = t.product_id','left');
		$this->db->order_by('hits','desc');
        $query=$this->db->get('top_selling t',3); 
        return $query->result();
}

	function getProduct($product_id)
    {

        $this->db->where('product_id',$product_id);
        $query=$this->db->get('products'); 


        return $query->result();

    }

	function getCategory($category_id)
    {
        $this->db->join('category c', 'c.category_id = p.category_id', 'right');
        $this->db->where('c.category_id',$category_id);
        $query=$this->db->get('products p'); 


        return $query->result();

    }

	function getCatName($category_id)
    {
        $this->db->where('category_id',$category_id);
        $query = $this->db->get('category');

        return $query->result();
    }        



}
