<?php

Class Admin_m Extends CI_Model
{
	var $gallery_path;
	function Admin_m()
	{
		parent::__construct();

		$this->gallery_path = realpath(APPPATH . '../images');
	}
	
	
	
	function getStudentDetails($stuName) {
        $query = "PUT YOUR QUERY";
        $result = mysql_query($query);
        return $result;
    }
	 
	function insert_file($image_url, $filename)
    {
        $data = array(
            'filename'      => $filename,
            'image_url'         => $image_url
        );
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }
	
	public function get_files()
	{
	    return $this->db->select()
	            ->from('products')
	            ->get()
	            ->result();
	}
	
	function get_pending_orders()
	{
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 0);
		$this->db->where('status', 'Pending');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function get_processing_orders()
	{
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 0);
		$this->db->where('status', 'Processing');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function get_onhold_orders()
	{
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 0);
		$this->db->where('status', 'On Hold');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function get_delivered_orders()
	{
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 0);
		$this->db->where('status', 'Delivered');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function get_cancelled_orders()
	{
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 1);
		$this->db->where('status', 'Cancelled');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function delete_file($file_id)
	{
	    $file = $this->get_file($file_id);
	    if (!$this->db->where('id', $file_id)->delete('products'))
	    {
	        return FALSE;
	    }
	    unlink('./uploads/' . $file->filename);    
	    return TRUE;
	}
 
	
	function get_recent_orders()
	{
		$this->db->order_by("order_date", "desc"); 
        $this->db->limit('5');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;	
	}

	function get_recent_customers(){
		$this->db->join('client', 'orders.customer_id = client.client_id');
		$this->db->order_by("order_date", "desc");
        $this->db->limit('5');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
	}
	
	function get_active_clients()
	{
		
		$this->db->where('status','active');
		$this->db->from('client');
		$data = $this->db->count_all_results();
		return $data;
	}
	
	function get_inactive_clients()
	{
		$this->db->where('status','inactive');
		$this->db->from('client');
		$data = $this->db->count_all_results();
		return $data;
	}
	
	function get_emails(){
		$this->db->select('email');
		$q = $this->db->get('client');
		return $q->result();
		
		
	}
	
	function get_clients_address()
	{
		$query = $this->db->get('client');
		return $query->result();
	}

	function get_orders(){
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('archived', 0);
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
		//print_r($data);
	}
	
	function get_sales(){
		$this->db->order_by("timestamp", "desc"); 
		$this->db->where('status', 'Delivered');
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;
		//print_r($data);
	}

	// public function insert_file($filename, $title)
    // {
        // $data = array(
            // 'filename'      => $filename,
            // 'title'         => $title
        // );
        // $this->db->insert('files', $data);
        // return $this->db->insert_id();
    // }
// 
    // public function get_files()
	// {
	    // return $this->db->select()
	            // ->from('files')
	            // ->get()
	            // ->result();
	// }
// 
	// public function delete_file($file_id)
// {
    // $file = $this->get_file($file_id);
    // if (!$this->db->where('id', $file_id)->delete('files'))
    // {
        // return FALSE;
    // }
    // unlink('./files/' . $file->filename);    
    // return TRUE;
// }
//  
// public function get_file($file_id)
// {
    // return $this->db->select()
            // ->from('files')
            // ->where('id', $file_id)
            // ->get()
            // ->row();
// }

	function get_customers()
	{ 
		$q= $this->db->get('client');
		$data = $q->result_array();
		return $data;	
	}

	function get_active_customers()
	{ 
		$q= $this->db->get_where('client', array('status' => 'active'));
		$data = $q->result_array();
		return $data;	
	}
	
	function get_inactive_customers()
	{ 
		$q= $this->db->get_where('client', array('status' => 'inactive'));
		$data = $q->result_array();
		return $data;	
	}
	
	
	function check_account_db(){
        $this->db->where('email', $this->input->post('inputEmail'));
        $this->db->where('password',$this->input->post('inputPassword'));
        
        $query = $this->db->get('administrators');
		if($query->num_rows == 1)
		{
                $row = $query->row();
                $data = array(
                'clientID' => $row->admin_id,
                'fname' => $row->fname,
                'lname' => $row->lname,
                'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
		}
		else{
                return false;	
		}	   
    }
	
	function check_profile_db(){
        $this->db->where('password',$this->input->post('curPass'));
        
        $query = $this->db->get('administrators');
		if($query->num_rows == 1)
		{
              return true;
		}
		else{
               return false;	
		}	   
    }
	
	function update_pass($admin_id)
	{
		echo 'hello';
		$data = array(
           'password' => $this->input->post('confPass')
        );

		$this->db->where('admin_id', $admin_id);
		$this->db->update('administrators', $data); 
	}
	
    function delete_order($id)
	{
		// $this->db->where('order_id', $id);
		// $this->db->delete('orders');
		$data = array(
		   'archived' => 1 ,
		);
		$this->db->where('order_id', $id);
		$q = $this->db->update('orders', $data); 
		return $q;
		//now delete the order items
		// $this->db->where('order_id', $id);
		// $this->db->delete('order_items');
	}

	function delete_cat($id)
	{
		$this->db->where('category_id', $id);
		$this->db->delete('category');
	}

	function delete_cust($id)
	{
		//no delete just deactivate
		$data = array(
               'status' => 'inactive',
            );

		$this->db->where('client_id', $id);
		$this->db->update('client', $data); 
	}
	
	function activate($stat,$id){
		
		$data = array(
		'status' => $stat
		);
		$this->db->where('client_id',$id);
		$q = $this->db->update('client',$data);

		return $q;
		
	}

	function get_categories()
	{
		$this->db->order_by("category_id", "asc"); 
		$q = $this->db->get('category');
		$data = $q->result_array();
		return $data;
	}

	function insert_cat($name, $desc, $stat)
	{
		$data = array(
		   'category_name' => $name ,
		   'description' => $desc ,
		   'status' => $stat
		);
		$q = $this->db->insert('category', $data); 
		return $q;
	}

	function get_products()
	{
		$this->db->select('*');
        $this->db->from('products'); 
        $this->db->join('suppliers', 'suppliers.supplier_id=products.supplier_id','left');
        $this->db->join('category', 'products.category_id=category.category_id','left');
        $this->db->where('products.status', 'active');
		$this->db->order_by("products.product_id", "asc"); 
		$query = $this->db->get(); 
        $data = $query->result_array();
        return $data;
	}

	function get_specific_category($id)
	{
		$q= $this->db->get_where('category', array('category_id' => $id));
		$data = $q->result_array();
		return $data;	
	}
	
	function get_specific_supplier($id)
	{
		$q= $this->db->get_where('suppliers', array('supplier_id' => $id));
		$data = $q->result_array();
		return $data;	
	}

	function update_cat($name, $desc, $stat, $id)
	{
		$data = array(
		   'category_name' => $name ,
		   'description' => $desc ,
		   'status' => $stat
		);
		$this->db->where('category_id', $id);
		$q = $this->db->update('category', $data); 
		return $q;
	}
	
	function update_sup($sup_id)
	{
		$sup_info =  array(
        		'company_name' 	=> $this->input->post('compName'),
        		'sup_fname' 	=> $this->input->post('supFirstName'),
        		'sup_lname' 	=> $this->input->post('supLastName'),
        		'cnum' 			=> $this->input->post('cnum'),
        		'email'			=> $this->input->post('email'),
        		'address' 		=> $this->input->post('add'),		
        		'status'		=> $this->input->post('status')
        		); 
		$this->db->where('supplier_id', $sup_id);
		$q = $this->db->update('suppliers', $sup_info); 
		return $q;
	}

	

	

	function get_current_count($prod_id)
	{
		$this->db->where('product_id', $prod_id);
		$q = $this->db->get('products'); 
		$data = $q->result_array();
		return $data;	
	}


	function edit_order($data)
	{
		//print_r($data);
		if (isset($data['order_id']))
		{
			$this->db->where('order_id', $data['order_id']);
			$this->db->update('orders', $data);
		}
	}


	function update_prod($id, $image_url, $filename)
	{
		$query = $this->db->get_where('products', array('product_id' => $id));
		
		foreach($query->result() as $row)
		{
			$initial_count = $row->count;
			
			$current_count = $this->input->post('quantity');
			
			if($current_count > $initial_count){
				$data = array(
	               'count' => $current_count,
	               'current_count' => $current_count
            	);

				$this->db->where('product_id', $id);
				$this->db->update('products', $data); 
			}
			else
			{
				$current_count = $this->input->post('quantity');
			}
		}
			
		$datestring = "%Y/%m/%d %h:%i:%s";
		$fname = $this->session->userdata('fname');
		$lname = $this->session->userdata('lname');
		$purchased_by = $fname . ' ' . $lname; 
		$update_product = array(
				'supplier_id' 			=> $this->input->post('supplier'),
				'product_name' 			=> $this->input->post('prodName'),
				'product_description' 	=> $this->input->post('prodDesc'),
            	'reg_price' 			=> $this->input->post('regPrice'),
            	'sale_price' 			=> $this->input->post('salePrice'),
            	'category_id' 			=> $this->input->post('category'),
            	'show'					=> $this->input->post('show'),
            	'current_count'			=> $current_count,
        		'inserted_by'			=> $purchased_by,
        		'date_received'			=> mdate($datestring),
        		'filename'      		=> $filename,
            	'image_url'         	=> $image_url
        );  
		$this->db->where('product_id', $id);
        $update = $this->db->update('products', $update_product); 
		return $update;
		//echo $this->db->last_query();
	}

	function get_initial_count($id)
	{
		$q = $this->db->get_where('products',array('product_id' => $id));
		return $q;
	}
	

	function get_specific_product($product_id)
	{
		$q= $this->db->get_where('products', array('product_id' => $product_id));
		$data = $q->result_array();
		return $data;	
	}

	function insert_image($data)
	{
         $insert_product = array(
            'product_name'        	    => $product_info['prod_name'],
            'product_description'       => $product_info['prod_desc'],
            'reg_price'        	    	=> $product_info['reg_price'],
            'sale_price'        	    => $product_info['sale_price'],
            'count'        	   			=> $product_info['quantity'],
            'status'        	    	=> $product_info['status'],
            'category_id'        	    => $product_info['category']
        );  
        $insert = $this->db->insert('products', $insert_product);
        return $insert;
	}

	function insert_product($product_info)
	{
		$insert_product = array(
            'product_name'        	    => $product_info['prod_name'],
            'product_description'       => $product_info['prod_desc'],
            'reg_price'        	    	=> $product_info['reg_price'],
            'sale_price'        	    => $product_info['sale_price'],
            'count'        	   			=> $product_info['quantity'],
            'max_count'        	   		=> $product_info['max_quantity'],
            'min_count'        	   		=> $product_info['min_quantity'],
            'show'        	    	=> $product_info['status'],
            'category_id'        	    => $product_info['category']
        );  
        $insert = $this->db->insert('products', $insert_product);
        return $insert;
	}

	function get_order_archives()
	{
		$q= $this->db->get_where('orders', array('archived' => 1));
		$data = $q->result_array();
		return $data;	
	}
	
	function get_sales_archives()
	{
		$q= $this->db->get_where('orders', array('archived' => 1, 'status' => 'Delivered'));
		$data = $q->result_array();
		return $data;	
	}

	function get_products_archives()
	{
		$this->db->select('*');
        $this->db->from('products'); 
        $this->db->join('suppliers', 'suppliers.supplier_id=products.supplier_id','left');
        $this->db->join('category', 'products.category_id=category.category_id','left');
        $this->db->where('products.status', 'inactive');
		$this->db->order_by("products.product_id", "asc"); 
		$query = $this->db->get(); 
        $data = $query->result_array();
        return $data;
	}

	function move_order($id)
	{
		$data = array(
		   'archived' => 0 ,
		);
		$this->db->where('order_id', $id);
		$q = $this->db->update('orders', $data); 
		return $q;
	}

	function get_suppliers()
	{
		$q= $this->db->get('suppliers');
		$data = $q->result_array();
		return $data;
	}

	function delete_supplier($id)
	{
		//no delete just deactivate
		$data = array(
               'status' => 'inactive',
            );

		$this->db->where('supplier_id', $id);
		$this->db->update('suppliers', $data); 
	}

	function insert_sup($sup_info)
	{
		$insert_supplier = array(
            	'company_name' 		=> $sup_info['company_name'],
        		'sup_fname' 		=> $sup_info['sup_fname'],
        		'sup_lname' 		=> $sup_info['sup_lname'],
        		'cnum' 				=> $sup_info['cnum'],
        		'address' 			=> $sup_info['address'],
        		'email'				=> $sup_info['email'],
				'status'			=> $sup_info['status']
        );  
        $insert = $this->db->insert('suppliers', $insert_supplier);
        return $insert;
	}

	function get_administrators()
	{
		// $this->db->join('administrators', 'orders.customer_id = administrators.client_id');
		// $q = $this->db->get('orders');
		// $data = $q->result_array();
		// return $data;
		$this->db->order_by("admin_id", "desc"); 
		$q= $this->db->get('administrators');
		$data = $q->result_array();
		return $data;
	}

	function insert_admin()
	{
		//print_r($admin_info);
		//echo '<pre>';print_r($insert_administrator);
		$fname = $this->session->userdata('fname');
		$lname = $this->session->userdata('lname');
		$created_by = $fname . ' ' . $lname; 
		$insert_administrator = array(
            	'fname' 			=> $this->input->post('fname'),
        		'lname' 			=> $this->input->post('lname'),
        		'bdate'				=> $this->input->post('bday'),
        		'cnum' 				=> $this->input->post('cnum'),
        		'email' 			=> $this->input->post('email'),
        		'address' 			=> $this->input->post('add'),
        		'password'			=> $this->input->post('pass1'),
        		'status'			=> 'active',
        		'created_by'		=> $created_by
        );  
        $insert = $this->db->insert('administrators', $insert_administrator);
	}

	function delete_administrator($id)
	{
		//no delete just deactivate
		$data = array(
               'status' => 'inactive',
            );
		$this->db->where('admin_id', $id);
		$data = $this->db->update('administrators', $data); 
		//print_r($data);
	}

	function insert_image_details($file_data)
	{
		$data = array(
            'file_name'     => $upload_data['file_name'],
            'file_type'     => $upload_data['file_type'],
            'file_size'		=>	$upload_data['file_size']
        );
        $this->db->insert('images', $data);
	}

	// public function insert_file($filename, $title)
 //    {
 //        $data = array(
 //            'file_name'      => $filename,
 //            'file_type'         => $title
 //        );
 //        $this->db->insert('images', $data);
 //        return $this->db->insert_id();
 //    }

    function do_upload()
    {
    	$config = array(
    			'allowed_types' 	=> 'jpg|jpeg|png|gif',
    			'upload_path' 		=> $this->gallery_path,
    			'max_size' 			=> 30000 
    	);

    	$this->load->library('upload', $config);
    	$this->upload->do_upload();
    	$image_data = $this->upload->data();

    	$config = array(
    		'source_image' 		=> $image_data['full_path'],
    		'new_image'			=> $this->gallery_path . '/thumbs',
    		'maintain_ration' 	=> true,
    		'width' 			=> 150,
    		'height' 			=> 100 
    	);

    	$this->load->library('image_lib', $config);

    	$this->image_lib->resize();
    }


    function get_purchases()
    {
    	$this->db->select('product_name,category_name,company_name,timestamp,count,reg_price,total,purchases.status,purchase_id');
        $this->db->from('purchases'); 
        $this->db->join('suppliers', 'suppliers.supplier_id=purchases.supplier_id','left');
        $this->db->join('category', 'purchases.category_id=category.category_id','left');
        $this->db->order_by('purchases.timestamp','desc');         
        $query = $this->db->get(); 
        $data = $query->result_array();
        return $data;
  //   	$this->db->join('suppliers', 'purchases.supplier_id = suppliers.supplier_id','left');
  //   	$this->db->join('category', 'purchases.category_id = category.category_id','left');
  //   	$this->db->order_by("timestamp", "desc"); 
		// $this->db->where('purchases.status', 'active');
		// $q = $this->db->get('purchases');
		// $data = $q->result_array();
		// //return $data;
		// echo '<pre>';print_r($data);

		// $this->db->join('client', 'orders.customer_id = client.client_id');
		// $this->db->order_by("order_date", "desc");
  //       $this->db->limit('5');
		// $q = $this->db->get('orders');
		// $data = $q->result_array();
		// return $data;
    }

    function delete_purchases($id)
    {
    	$data = array(
               'status' => 'inactive',
            );
		$this->db->where('purchase_id', $id);
		$data = $this->db->update('purchases', $data); 
    }

    function insert_purchase()
    {
    	$fname = $this->session->userdata('fname');
		$lname = $this->session->userdata('lname');
		$quantity = (int)$this->input->post('quantity');
		$price = (double)$this->input->post('regPrice');
		$total =  $quantity*$price;
		$purchased_by = $fname . ' ' . $lname; 
		$insert_pur = array(
				'supplier_id' 			=> $this->input->post('supplier'),
				'product_name' 			=> $this->input->post('prodName'),
				'count' 				=> $this->input->post('quantity'),
				'current_count' 		=> $this->input->post('quantity'),
            	'reg_price' 			=> $this->input->post('regPrice'),
            	'category_id' 			=> $this->input->post('category'),
        		'status'				=> $this->input->post('status'),
        		'purchased_by'			=> $purchased_by,
        		'total'					=> $total
        );  
        $insert = $this->db->insert('purchases', $insert_pur);
    }

    function edit_purchase($purchase_id)
    {
			$data = array(
			   'status' 	=> $this->input->post('purchase_status') ,
			);
			$this->db->where('purchase_id', $purchase_id);
			$this->db->update('purchases', $data);
    }
	
	function update_purchase(){
		$pcount = (int)$this->input->post('prod_count');
		$prod_name = $this->input->post('product_name');
		$cat_id = $this->input->post('category_id');
		$pd = $this->input->post('purchase_date');
		
		$this->db->where('category_id',$this->input->post('category_id'));
		$this->db->where('product_name',$this->input->post('product_name'));
		$q = $this->db->get('products');
		if( $q->num_rows == 1){
			
			$r = $q->row();
			$ccount = $r->current_count;
			$final = $pcount + $ccount;
			$data = array(
			'current_count' => $final,
			'date_received' => $pd
			);
			$this->db->where('category_id',$cat_id);
			$this->db->where('product_name',$prod_name);
			$query = $this->db->update('products',$data);
			
		}else{
			
			$data = array(
			'product_name' => $this->input->post('product_name'),
			'supplier_id' => $this->input->post('supplier_id'),
			'category_id' => $this->input->post('category_id'),
			'reg_price' => $this->input->post('reg_price'),
			'current_count' => $this->input->post('prod_count'),
			'count' => $this->input->post('prod_count'),
			'date_received' => $this->input->post('purchase_date'),
			'show' => 'Enabled',
			'status' => 'active'
			
			);
			
			$q = $this->db->insert('products',$data);
			return $q;
			
			
		}
			
			
			
			
		
		
		
		
	}

    function get_specific_purchase($id)
    {
    	$this->db->select('purchases.supplier_id,purchases.category_id,product_name,category_name,company_name,timestamp,count,reg_price,total,purchases.status,purchase_id');
    	$this->db->join('suppliers', 'purchases.supplier_id = suppliers.supplier_id');
    	$this->db->join('category', 'purchases.category_id = category.category_id');
    	$this->db->order_by("timestamp", "desc"); 
		$this->db->where('purchases.purchase_id', $id);
		$q = $this->db->get('purchases');
		return $q->result();
		
    }

    function insert_purchase_to_products()
    {
    	$datestring = "%Y/%m/%d %h:%i:%s";
		
    	$fname = $this->session->userdata('fname');
		$lname = $this->session->userdata('lname');
		$purchased_by = $fname . ' ' . $lname; 
		$insert_pur = array(
				'supplier_id' 			=> $this->input->post('supplier'),
				'product_name' 			=> $this->input->post('prodName'),
				'count' 				=> $this->input->post('quantity'),
				'current_count' 		=> $this->input->post('quantity'),
            	'reg_price' 			=> $this->input->post('regPrice'),
            	'category_id' 			=> $this->input->post('category'),
        		'status'				=> 'active',
        		'show'					=> $this->input->post('status'),
        		'inserted_by'			=> $purchased_by,
        		'date_received'			=> mdate($datestring)
        );  
        $insert = $this->db->insert('products', $insert_pur);
    }

    function delete_product($id)
    {
    	//no delete just deactivate
		$data = array(
               'status' => 'inactive',
            );

		$this->db->where('product_id', $id);
		$this->db->update('products', $data); 
    }

     function get_order_id($id)
	
	{
		$this->db->where('order_id',$id);
		$query = $this->db->get('orders');
		return $query->result();
	}
	
	function get_sales_details($order_id,$cust_id)
	{
		$this->db->join('client', 'orders.customer_id=client.client_id');
		$this->db->where('orders.order_id',$order_id);
		$this->db->where('client.client_id',$cust_id);
		$query = $this->db->get('orders');
		return $query->result();
	}


	function update_stat($id,$customer_id)
	{
		$data = array(
			'status' => $this->input->post('order_status') , 
		);
		$this->db->where('order_id', $id);
		$this->db->where('customer_id', $customer_id);
		$q = $this->db->update('orders',$data); 
		return $q;	
		//echo $this->db->last_query();
	}
	
	function order_cancel($id,$cust_id){
		
		$data = array(
			'status' => $this->input->post('order_status') , 
			'archived' => '1' , 
		);
		$this->db->where('order_id', $id);
		$this->db->where('customer_id', $cust_id);
		$q = $this->db->update('orders',$data); 
		return $q;	
	}

	function update_stat_hold($id,$cust_id)
	{
		$data = array(
			'status' => 'On Hold', 
		);
		$this->db->where('order_id', $id);
		$this->db->where('customer_id', $cust_id);
		$q = $this->db->update('orders',$data); 
		return $q;	
	}

	function get_products_cart($order_number,$cust_id)
	{
		$this->db->select('*');
        $this->db->from('cart'); 
        $this->db->join('orders', 'orders.order_number=cart.cart_number','left');
		$this->db->join('products', 'products.product_id=cart.product_id','left');
		$this->db->where('cart.status', 'co');
        $this->db->where('cart.cart_number', $order_number);
        $this->db->where('cart.client_id', $cust_id);
  		$query = $this->db->get(); 
        $data = $query->result_array();
        return $data;
        //echo $this->db->last_query();
	}


	function get_order_products($order_number,$cust_id)
	{
		$q= $this->db->get_where('cart', array('cart_number' => $order_number, 'client_id' => $cust_id));
		$data = $q->result_array();
		return $data;
		//echo $this->db->last_query();
	}

	function update_quantity($prod_id,$quantity,$cur_count)
	{
		//echo 'hello';
		$total = $cur_count - $quantity;
		if($total < 0)
		{
			return false;
		}else{
			$update_product = array(
			'current_count' 	=> $total,  
			);
			$this->db->where('product_id', $prod_id);
			$q = $this->db->update('products', $update_product); 
			return $q;	
		}
	}

	function get_order_total($id, $order_number){
		$this->db->select_sum("quantity*reg_price","total");
        $this->db->from('cart'); 
        $this->db->join('products', 'cart.product_id=products.product_id');
        $this->db->where('cart.client_id', $id);
        $this->db->where('cart.status', 'oc');
		$this->db->where("cart.cart_number", $order_number); 
		$query = $this->db->get(); 
        $data = $query->result_array();
        // return $data;


        //  $sum = $this->db
        //  ->query('select sum(quantity*reg_price) as total from cart c right join products p 
        //             on c.product_id = p.product_id where client_id = "'.$id.'" and c.status = "oc" and cart_number = "'.$order_number.'"');
        // //return $sum->row();
		echo $this->db->last_query();
	}
	
	function get_order_count()
	{
		 $data = $this->db->count_all_results('orders');
		return $data;
	}
	
	function get_archived_orders_count()
	{
		 $this->db->where('archived',1);
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_order_pending()
	{
		 $this->db->where('status','Pending');
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_order_processing()
	{
		 $this->db->where('status','Processing');
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_order_delivered()
	{
		 $this->db->where('status','Delivered');
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_order_on_hold()
	{
		 $this->db->where('status','On Hold');
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_order_cancelled()
	{
		 $this->db->where('status','Cancelled');
		 $this->db->from('orders');	 	
		 $data = $this->db->count_all_results();
		 return $data;
	}
	
	function get_admin_details()
	{
		$admin_id = $this->session->userdata('clientID');
		$this->db->where('admin_id', $admin_id);
		$q= $this->db->get('administrators');
		$data = $q->result();
		return $data;
	}
	
	function insert_product_details($image_url,$filename)
	{
		$datestring = "%Y/%m/%d %h:%i:%s";
		$fname = $this->session->userdata('fname');
		$lname = $this->session->userdata('lname');
		$purchased_by = $fname . ' ' . $lname; 
		$insert_prod = array(
				'supplier_id' 			=> $this->input->post('supplier'),
				'product_name' 			=> $this->input->post('prodName'),
				'product_description' 	=> $this->input->post('prodDesc'),
            	'reg_price' 			=> $this->input->post('regPrice'),
            	'sale_price' 			=> $this->input->post('salePrice'),
            	'category_id' 			=> $this->input->post('category'),
            	'show'					=> $this->input->post('show'),
            	'count'					=> $this->input->post('quantity'),
            	'current_count'			=> $this->input->post('quantity'),
        		'status'				=> 'active',
        		'inserted_by'			=> $purchased_by,
        		'date_received'			=> mdate($datestring),
        		'filename'      		=> $filename,
            	'image_url'         	=> $image_url
        );  
        $insert = $this->db->insert('products', $insert_prod);
		return $insert;
	}
	
	
	function tot_orders(){
    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');
    	$tot = $this->db
    	->query('select sum(total) as total from orders where order_date >= "'.$start.'" and order_date <= "'.$end.'"');
    	return $tot->row();

    }

     function tot_sales(){
    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');
    	$tot = $this->db
    	->query('select sum(total) as total from orders where order_date >= "'.$start.'" and order_date <= "'.$end.'" and status="Delivered"');
    	return $tot->row();

    }

    function total_sales(){

    	$tot = $this->db
    	->query('select sum(total) as total from orders where status="Delivered"');
    	return $tot->row();

    }

    function tot_purchase(){
    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');
    	$tot = $this->db
    	->query('select sum(count*reg_price) as total from purchases where timestamp >= "'.$start.'" and timestamp <= "'.$end.'"');
    	return $tot->row();

    }

    function total_purchase(){

    	$tot = $this->db
    	->query('select sum(count*reg_price) as total from purchases');
    	return $tot->row();

    }

    function print_purchases(){

    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');

    	$this->db->where('timestamp >=', $start);
        $this->db->where('timestamp <=', $end); 	
    	$this->db->select('*');
        $this->db->from('purchases'); 
        $this->db->join('suppliers', 'suppliers.supplier_id=purchases.supplier_id','left');
        $this->db->join('category', 'purchases.category_id=category.category_id','left');
        $this->db->where('purchases.status','Delivered');
        $this->db->order_by('purchases.timestamp','desc');         
        $query = $this->db->get(); 
        $data = $query->result_array();
        return $data;

    }

    function print_sales(){

    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');

    	$this->db->where('status','Delivered');
    	$this->db->where('order_date >=', $start);
        $this->db->where('order_date <=', $end); 	
    	$this->db->order_by('order_date', 'desc'); 
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;

    }


    function order_search(){
    	$start = $this->input->post('sdate');
    	$end = $this->input->post('edate');
    	$this->db->where('order_date >=', $start);
        $this->db->where('order_date <=', $end); 	
    	$this->db->order_by('order_date', 'desc'); 
		$q = $this->db->get('orders');
		$data = $q->result_array();
		return $data;

    }
	
	//Messages
	
	function get_inbox($email)
	{
		$this->db->where('receiver_id',$email);	
		$this->db->where('status !=','trash');
		$q = $this->db->get('messages');
		
		return $q->result();			
	}
	
function count_inbox($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status !=','trash')
        ->count_all_results();

        return $num;
	}
	
function count_unread($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status','unread')
        ->count_all_results();

        return $num;
	}	

function get_trash($email)
	{
		$this->db->where('receiver_id',$email);	
		$this->db->where('status','trash');
		$q = $this->db->get('messages');
		
		return $q->result();
		
	}
	
	function count_trash($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status','trash')
        ->count_all_results();

        return $num;
	}
	
function get_outbox($email)
	{
		
		$this->db->where('sender_id',$email);	
		$q = $this->db->get('messages');
		
		return $q->result();
		
		
	}
	
function count_outbox($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('sender_id', $email)
        ->count_all_results();

        return $num;
	}	
	
function add_message()
	{
		$data = array(
		
		'sender_id' => 'admin@pcfast.com',
		'receiver_id' => $this->input->post('receiver'),
		'title' => $this->input->post('title'),
		'body' => $this->input->post('message'),
		);
		
		$q = $this->db->insert('messages',$data);
		return $q;
		
	}	
	
function get_mid($mid)
	
	{
		$this->db->where('message_id',$mid);
		$query = $this->db->get('messages');
		return $query->result();
		
	
	}
	
function read($mid){
	
	$data = array(
	'status' => 'read'
	);
	$this->db->where('message_id',$mid);
	$q = $this->db->update('messages',$data);
	
	}	

function trash($mid){
	
	$data = array(
	'status' => 'trash'
	);
	$this->db->where('message_id',$mid);
	$q = $this->db->update('messages',$data);
	
	}

function order_history($id){
	
	$this->db->where('customer_id',$id);
	$q = $this->db->get('orders');
	return $q->result_array();
	
}

function order_items($id){
	$this->db->where('cart_number',$id);
	$this->db->join('products p','p.product_id = c.product_id','left');
	$q = $this->db->get('cart c');
	return $q->result_array();
	
}
	
	
	// function get_specific_sale($cust_id,$order_no)
	// {
		// $this->db->select('*');
        // $this->db->from('products'); 
        // $this->db->join('cart', 'products.product_id=cart.product_id');
        // $this->db->where('cart_number', $order_no);
        // $this->db->where('client_id', $cust_id);
  		// $query = $this->db->get(); 
        // $data = $query->result_array();
        // return $data;
	// }
}