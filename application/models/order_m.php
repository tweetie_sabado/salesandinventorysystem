<?php

class Order_m extends CI_Model{


function add_clients($data)
    
    {
    
        $this->db->insert('client', $data);
        return;
    
    }

function getCatalogs()
    {

        $query = $this->db->query('SELECT category_name,category_id FROM category');


        return $query->result();

    }

function getLatest(){

        $this->db->join('inventory i', 'i.product_name = p.product_name', 'right');
        $query=$this->db->get('products p'); 
        return $query->result();
}

function getName($id){

        $this->db->where('client_id',$id);
        $query = $this->db->get('client');

        return $query->result();
}

function getCart($id){
        $this->db->where('c.status','oc');
        $this->db->where('client_id',$id);
        $this->db->join('products p', 'p.product_id = c.product_id', 'right');
        $query = $this->db->get('cart c');

        return $query->result();
}

function getSum($id){

         $sum = $this->db
         ->query('select sum(quantity*reg_price) as total from cart c right join products p 
                    on c.product_id = p.product_id where client_id = "'.$id.'" and c.status = "oc"');


        return $sum->row();
}

function countCart($id){

        $num = $this->db
        ->from('cart')
        ->where('client_id', $id)
        ->where('status','oc')
        ->count_all_results();

        return $num;
}

function addCart($id, $product_id,$rndm,$h,$price){

        $qty = $this->input->post('qty');
        $mul = $qty*$price;
        
        
        $data = array(  
                'client_id' => $id,
                'product_id' => $product_id,
                'quantity' => $this->input->post('qty'),
                'status' => 'oc',
                'name' => $h,
                'total' => $mul
                 );

        $q = $this->db->insert('cart', $data);
        return $q;
        //echo $this->db->last_query();
}


function getProduct($product_id)
    {

        $this->db->where('inventory_id',$product_id);
        $this->db->join('inventory i', 'i.product_name = p.product_name', 'right');
        $query=$this->db->get('products p'); 


        return $query->result();

    }

function delete_row($id)
    {
    $this->db->where('cart_id',$id);
    $query = $this->db->delete('cart');

    
    }

function getCategory($category_id)
    {
        $this->db->join('category c', 'c.category_id = p.category_id', 'right');
        $this->db->join('inventory i', 'i.product_name = p.product_name', 'right');
        $this->db->where('c.category_id',$category_id);
        $query=$this->db->get('products p'); 


        return $query->result();

    }

function getCatName($category_id)
    {
        $this->db->where('category_id',$category_id);
        $query = $this->db->get('category');

        return $query->result();
    }        

function submitOrder($id,$s,$h,$rndm)
    {
       date_default_timezone_set('Asia/Manila');	
       $s_name = $this->input->post('ship_fname').' '.$this->input->post('ship_lname');
	   $b_name = $this->input->post('bill_fname').' '.$this->input->post('bill_lname');
       $date = date("Y-m-d H:i:s");
       $data = array(  
                'customer_id' => $id,
                'status' => 'Pending',
                'total' => $s,
                'order_number' => $rndm,
                'bill_to' => $h,
                'ship_to' => $h,
                'ship_city' => $this->input->post('ship_city'),
                'ship_street' => $this->input->post('ship_street'),
                'ship_brgy' => $this->input->post('ship_brgy'),
                'ship_email' => $this->input->post('ship_email'),
                'ship_cnum' => $this->input->post('ship_cnum'),
                'bill_city' => $this->input->post('bill_city'),
                'bill_street' => $this->input->post('bill_street'),
                'bill_brgy' => $this->input->post('bill_brgy'),
                'bill_email' => $this->input->post('bill_email'),
                'bill_cnum' => $this->input->post('bill_cnum'),
                'order_date' => $date,
                 );

        $q = $this->db->insert('orders', $data);
        return $q;
    }

function update_cart($id,$rndm)

{
     $data = array(
            'status' => 'co',
            'total' => $this->input->post('total')
            );

    $this->db->where('client_id',$id);
    $this->db->where('cart_number',$rndm);
    $this->db->where('status','oc'); 
    $q = $this->db->update('cart',$data);
    //echo $this->db->last_query();
}

 function del_cart($id)
 
    {

        $this->db->where('client_id',$id);
        $this->db->where('status','oc');
        $query = $this->db->delete('cart');

    }   

    function update_prod_quantity($id)
    {
        $data = array(
           'quantity' => 0 ,
        );
        $this->db->where('cart.status','oc');
        $this->db->where('client_id',$id);
        $this->db->join('cart', 'products.product_id = cart.product_id');
        $query = $this->db->update('products',$data);
        echo $this->db->last_query();
        return $query->result();
    }


}
