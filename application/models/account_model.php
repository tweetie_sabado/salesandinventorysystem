<?php

class Account_model extends CI_Model{


function add_clients($data)
	
	{
	
		$this->db->insert('client', $data);
		return;
	
	}

function getCatalogs()
    {

        $query = $this->db->query('SELECT category_name,category_id FROM category where status = "Enabled"');


        return $query->result();

    }
	
function add_hits($product_id){
	$this->db->where('product_id',$product_id);
	$q = $this->db->get('top_selling');
	
	if($q->num_rows == 1){
		$r = $q->row();
		$h = $r->hits;
		$hits = $h + 1;
		
		$data = array(
		
		'hits' => $hits
		
		);
		$this->db->where('product_id',$product_id);
		$q = $this->db->update('top_selling',$data);
		return $q;
	}else{
		
		$data = array(
		'product_id' => $product_id,
		'hits' => 1
		
		);
		$q = $this->db->insert('top_selling',$data);
		return $q;
		
		
	}
	
	
	
	
}	

function get_inbox($email)
	{
		$this->db->where('receiver_id',$email);	
		$this->db->where('status !=','trash');
		$q = $this->db->get('messages');
		
		return $q->result();			
	}
	
function feedback(){
		
		$data = array(
		
		'sender_id' => $this->input->post('name'),
		'receiver_id' => 'admin@pcfast.com',
		'title' => 'Feedback',
		'body' => $this->input->post('message')
		);
		
		$q = $this->db->insert('messages',$data);
		return $q;
		
		
	}		
	
function count_inbox($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status !=','trash')
        ->count_all_results();

        return $num;
	}
	
function count_unread($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status','unread')
        ->count_all_results();

        return $num;
	}	

function get_trash($email)
	{
		$this->db->where('receiver_id',$email);	
		$this->db->where('status','trash');
		$q = $this->db->get('messages');
		
		return $q->result();
		
	}
	
	function count_trash($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('receiver_id', $email)
        ->where('status','trash')
        ->count_all_results();

        return $num;
	}
	
function get_outbox($email)
	{
		
		$this->db->where('sender_id',$email);	
		$q = $this->db->get('messages');
		
		return $q->result();
		
		
	}
	
function count_outbox($email)
	{
		$num = $this->db
        ->from('messages')
        ->where('sender_id', $email)
        ->count_all_results();

        return $num;
	}	
	
function add_message($email)
	{
		$data = array(
		
		'sender_id' => $email,
		'receiver_id' => 'admin@pcfast.com',
		'title' => $this->input->post('title'),
		'body' => $this->input->post('message'),
		);
		
		$q = $this->db->insert('messages',$data);
		return $q;
		
	}	
	
function get_mid($mid)
	
	{
		$this->db->where('message_id',$mid);
		$query = $this->db->get('messages');
		return $query->result();
		
	
	}
	
function read($mid){
	
	$data = array(
	'status' => 'read'
	);
	$this->db->where('message_id',$mid);
	$q = $this->db->update('messages',$data);
	
	}	

function trash($mid){
	
	$data = array(
	'status' => 'trash'
	);
	$this->db->where('message_id',$mid);
	$q = $this->db->update('messages',$data);
	
	}	


function getLatest(){
		$this->db->where('current_count !=',0);
		$this->db->where('show','Enabled');
        $query=$this->db->get('products',3); 
        return $query->result();
}

function getTop(){
		$this->db->where('current_count !=',0);
		$this->db->where('show','Enabled');
		$this->db->join('products p', 'p.product_id = t.product_id','left');
		$this->db->order_by('hits','desc');
        $query=$this->db->get('top_selling t',3); 
        return $query->result();
}

function getName($id){

        $this->db->where('client_id',$id);
        $query = $this->db->get('client');

        return $query->result();
}

function getCart($id){
        $this->db->where('c.status','oc');
        $this->db->where('client_id',$id);
        $this->db->join('products p', 'p.product_id = c.product_id', 'right');
        $query = $this->db->get('cart c');

        return $query->result();
}

function get_cart_id($cart_id){
		$this->db->where('cart_id',$cart_id);
		$this->db->join('products p', 'p.product_id = c.product_id', 'left');
		$q = $this->db->get('cart c');
		
			
			$r = $q->row();
			$this->session->set_flashdata('pc',$r->current_count);
		
		return $q->row();
	
}

function countCart($id){

        $num = $this->db
        ->from('cart')
        ->where('client_id', $id)
        ->where('status','oc')
        ->count_all_results();

        return $num;
}

function addCart($id, $product_id,$rndm,$h,$total){
    

        $data = array(  
                'client_id' => $id,
                'product_id' => $product_id,
                'quantity' => $this->input->post('qty'),
                'status' => 'oc',
                'cart_number' => $rndm,
                'name' => $h,
                 'total' => $total   
                 );

        $q = $this->db->insert('cart', $data);
        return $q;

    
}



function edit_client($id){
        $data = array(
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'cnum' => $this->input->post('cnum'),
            'city' => $this->input->post('city'),
            'brgy' => $this->input->post('brgy'),
            'address' => $this->input->post('address'),
            );

        $this->db->where('client_id',$id);
        $q = $this->db->update('client',$data);

}

function edit_password($id){

        $data = array(
            'password' => $this->input->post('password'),
            );

        $this->db->where('client_id',$id);
        $q = $this->db->update('client',$data);


}

function edit_secret($id){

        $data = array(
            'question' => $this->input->post('quest'),
            'ans' => $this->input->post('ans'),
            );

        $this->db->where('client_id',$id);
        $q = $this->db->update('client',$data);


}

	function getProduct($product_id)
    {

        $this->db->where('product_id',$product_id);
        $query=$this->db->get('products'); 


        return $query->result();

    }
	
	function getProductReview($product_id){
		
		$this->db->where('product_id',$product_id);
		$this->db->join('client c','c.client_id = r.client_id','right');
        $query=$this->db->get('prod_review r'); 


        return $query->result();
		
	}
	
	function add_review($id,$product_id){
		
		$data = array(
		'client_id' => $id,
		'product_id' => $product_id,
		'review' => $this->input->post('review'),
		'rating' => $this->input->post('rating')
		);
		
		$q = $this->db->insert('prod_review',$data);
		return $q;
		
	}

    function getQty($product_id)
    {
        $qty =
        $this->db->query('select current_count as qty from products where product_id ="'.$product_id.'"'); 


        return $qty->row();

    }

function getOrder($id)
    {
        $this->db->where('customer_id',$id);
        $this->db->order_by('order_date', 'desc'); 
        $query=$this->db->get('orders'); 
        return $query->result();

    }    

function delete_row($id)
    {
    $this->db->where('cart_id',$id);
    $query = $this->db->delete('cart');

    
    }

function update_cart($cart_id){
	
	$data = array(
	'quantity' => $this->input->post('cart_qty')
	);
	$this->db->where('cart_id',$cart_id);
	$q = $this->db->update('cart',$data);
	return $q;
	
	
	}

function getCategory($category_id)
    {	$this->db->where('current_count !=',0);
    	$this->db->where('show','Enabled');
        $this->db->join('category c', 'c.category_id = p.category_id', 'right');
        $this->db->where('c.category_id',$category_id);
        $query=$this->db->get('products p'); 


        return $query->result();

    }

function getCatName($category_id)
    {
        $this->db->where('category_id',$category_id);
        $query = $this->db->get('category');

        return $query->result();
    }

function search($keyword)
    {
    	$this->db->where('current_count !=',0);
		$this->db->where('show','Enabled');
        $this->db->like('product_name', $keyword); 
        $this->db->or_like('category_name', $keyword);       
        $this->db->join('category c', 'c.category_id = p.category_id', 'left');
        $query=$this->db->get('products p');
        return $query->result();
    }

 function view_order($id,$order_number)
    {
        $this->db->where('client_id',$id);
        $this->db->where('cart_number',$order_number);
        $this->db->from('cart c');
        $this->db->join('orders o','o.order_number = c.cart_number','o.customer_id = c.client_id','right');
        $this->db->join('products p','p.product_id = c.product_id','right');
        $query = $this->db->get();
        //echo $this->db->Last_query();
        return $query->result();

    }           

 function order_stat($id,$order_number)
    {

        $this->db->where('customer_id',$id);
        $this->db->where('order_number',$order_number);
        $q = $this->db->get('orders');

        return $q->result();

    }   



}
