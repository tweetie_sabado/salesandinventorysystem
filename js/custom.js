function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to move the selected orders to archives?');
	}
	else
	{
		alert('You did not select any orders to move');
		return false;
	}
}

function customer_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to deactivate the selected customer/s?');
	}
	else
	{
		alert('You did not select any customer/s to deactivate');
		return false;
	}
}

function cat_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to delete the selected category/ies?');
	}
	else
	{
		alert('You did not select any category to delete.');
		return false;
	}
}

function prod_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to delete the selected product/s?');
	}
	else
	{
		alert('You did not select any product to delete.');
		return false;
	}
}

function archive_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to move the selected archived order/s back to Orders?');
	}
	else
	{
		alert('You did not select any archived order/s to move.');
		return false;
	}
}

function supplier_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to deactivate the selected supplier/s?');
	}
	else
	{
		alert('You did not select any supplier/s to deactivate.');
		return false;
	}
}

function admin_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to deactivate the selected admin/s?');
	}
	else
	{
		alert('You did not select any admin/s to deactivate.');
		return false;
	}
}

function purchase_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to deactivate the selected purchase/s?');
	}
	else
	{
		alert('You did not select any purchase/s to deactivate.');
		return false;
	}
}

function sale_form()
{
	if($(".gc_check:checked").length > 0)
	{
		return confirm('Are you sure you want to deactivate the selected sale/s?');
	}
	else
	{
		alert('You did not select any sale/s to deactivate.');
		return false;
	}
}

function sales_archive_form()
{
	if($(".gc_check:checked").length > 1)
	{
		return confirm('Are you sure you want to move the selected sales back to SALES?');
	}
	else if($(".gc_check:checked").length == 1)
	{
		return confirm('Are you sure you want to move the selected sale back to SALES?');
	}
	else
	{
		alert('You did not select any sales to move.');
		return false;
	}
}

function prod_archives_form()
{
	if($(".gc_check:checked").length > 1)
	{
		return confirm('Are you sure you want to move the selected products back to SALES?');
	}
	else if($(".gc_check:checked").length == 1)
	{
		return confirm('Are you sure you want to move the selected product back to PRODUCTS?');
	}
	else
	{
		alert('You did not select any product to move.');
		return false;
	}
	
}

$(function grade() {
		
		$("a.edit-cart").on("click", function(event){
			event.preventDefault();
            $('#edit_cart').modal('show');


		$.ajax({
		url:'account/get_cart_id',
			  type: 'post',
			  data: {'cart_id': $(this).data('cart_id')},
			  dataType: "json",
			  success: function(data, status) {
			  	  $('#cart_prod_name').val(data.product_name);
			  	  $('#prod_count').val(data.current_count);
			  	  $('#cart_id').val(data.cart_id);
				  $('#cart_qty').val(data.quantity);
				
			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});


